/*
 * lxc: linux Container library
 *
 * (C) Copyright IBM Corp. 2007, 2008
 *
 * Authors:
 * Daniel Lezcano <daniel.lezcano at free.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1
#endif
#include <stdint.h>
#include <string.h>

#include "config.h"
#include "log.h"
#include "storage.h"
#include "storage_utils.h"
#include "utils.h"

lxc_log_define(blk, lxc);

int blk_destroy(struct lxc_storage *orig)
{
	return 0;
}

bool blk_detect(const char *path)
{
	struct stat statbuf;
	int ret;

	if (!strncmp(path, "blk:", 4))
		return true;

	ret = stat(path, &statbuf);
	if (ret == -1 && errno == EPERM) {
		SYSERROR("blk_detect: failed to look at \"%s\"", path);
		return false;
	}

	if (ret == 0 && S_ISBLK(statbuf.st_mode))
		return true;

	return false;
}

int blk_mount(struct lxc_storage *bdev)
{
	const char *src;
	if (strcmp(bdev->type, "blk"))
		return -22;

	if (!bdev->src || !bdev->dest)
		return -22;

	src = lxc_storage_get_path(bdev->src, bdev->type);

	return mount_unknown_fs(src, bdev->dest, bdev->mntopts);
}

int blk_umount(struct lxc_storage *bdev)
{
	if (strcmp(bdev->type, "blk"))
		return -22;

	if (!bdev->src || !bdev->dest)
		return -22;

	return umount(bdev->dest);
}
