/* SPDX-License-Identifier: LGPL-2.1+ */
/******************************************************************************
 * Copyright (c) Huawei Technologies Co., Ltd. 2020. Allrights reserved
 * Description: isulad utils
 * Author: lifeng
 * Create: 2020-04-11
******************************************************************************/
#ifndef __iSULAD_UTILS_H
#define __iSULAD_UTILS_H

#include <stdio.h>

extern int lxc_mem_realloc(void **newptr, size_t newsize, void *oldptr, size_t oldsize);
extern void *lxc_common_calloc_s(size_t size);
extern char *safe_strdup(const char *src);

extern int lxc_open(const char *filename, int flags, mode_t mode);
extern FILE *lxc_fopen(const char *filename, const char *mode);

#endif
