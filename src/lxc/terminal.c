/* SPDX-License-Identifier: LGPL-2.1+ */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1
#endif
#include <errno.h>
#include <fcntl.h>
#include <lxc/lxccontainer.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>

#include "af_unix.h"
#include "caps.h"
#include "commands.h"
#include "conf.h"
#include "config.h"
#include "log.h"
#include "lxclock.h"
#include "mainloop.h"
#include "memory_utils.h"
#include "start.h"
#include "syscall_wrappers.h"
#include "terminal.h"
#include "utils.h"
#ifdef HAVE_ISULAD
#include "logger_json_file.h"
#include "include/strlcpy.h"
#endif

#if HAVE_PTY_H
#include <pty.h>
#else
#include <../include/openpty.h>
#endif

#define LXC_TERMINAL_BUFFER_SIZE 1024

lxc_log_define(terminal, lxc);

void lxc_terminal_winsz(int srcfd, int dstfd)
{
	int ret;
	struct winsize wsz;

	if (!isatty(srcfd))
		return;

	ret = ioctl(srcfd, TIOCGWINSZ, &wsz);
	if (ret < 0) {
		WARN("Failed to get window size");
		return;
	}

	ret = ioctl(dstfd, TIOCSWINSZ, &wsz);
	if (ret < 0)
		WARN("Failed to set window size");
	else
		DEBUG("Set window size to %d columns and %d rows", wsz.ws_col,
		      wsz.ws_row);

	return;
}

static void lxc_terminal_winch(struct lxc_terminal_state *ts)
{
	lxc_terminal_winsz(ts->stdinfd, ts->masterfd);
}

int lxc_terminal_signalfd_cb(int fd, uint32_t events, void *cbdata,
			     struct lxc_epoll_descr *descr)
{
	ssize_t ret;
	struct signalfd_siginfo siginfo;
	struct lxc_terminal_state *ts = cbdata;

	ret = lxc_read_nointr(fd, &siginfo, sizeof(siginfo));
	if (ret < 0 || (size_t)ret < sizeof(siginfo)) {
		ERROR("Failed to read signal info");
		return LXC_MAINLOOP_ERROR;
	}

	if (siginfo.ssi_signo == SIGTERM) {
		DEBUG("Received SIGTERM. Detaching from the terminal");
		return LXC_MAINLOOP_CLOSE;
	}

	if (siginfo.ssi_signo == SIGWINCH)
		lxc_terminal_winch(ts);

	return LXC_MAINLOOP_CONTINUE;
}

struct lxc_terminal_state *lxc_terminal_signal_init(int srcfd, int dstfd)
{
	__do_close int signal_fd = -EBADF;
	__do_free struct lxc_terminal_state *ts = NULL;
	int ret;
	sigset_t mask;

	ts = malloc(sizeof(*ts));
	if (!ts)
		return NULL;

	memset(ts, 0, sizeof(*ts));
	ts->stdinfd = srcfd;
	ts->masterfd = dstfd;
	ts->sigfd = -1;

	ret = sigemptyset(&mask);
	if (ret < 0) {
		SYSERROR("Failed to initialize an empty signal set");
		return NULL;
	}

	if (isatty(srcfd)) {
		ret = sigaddset(&mask, SIGWINCH);
		if (ret < 0)
			SYSNOTICE("Failed to add SIGWINCH to signal set");
	} else {
		INFO("fd %d does not refer to a tty device", srcfd);
	}

	/* Exit the mainloop cleanly on SIGTERM. */
	ret = sigaddset(&mask, SIGTERM);
	if (ret < 0) {
		SYSERROR("Failed to add SIGWINCH to signal set");
		return NULL;
	}

	ret = pthread_sigmask(SIG_BLOCK, &mask, &ts->oldmask);
	if (ret < 0) {
		WARN("Failed to block signals");
		return NULL;
	}

	signal_fd = signalfd(-1, &mask, SFD_CLOEXEC);
	if (signal_fd < 0) {
		WARN("Failed to create signal fd");
		(void)pthread_sigmask(SIG_SETMASK, &ts->oldmask, NULL);
		return NULL;
	}
	ts->sigfd = move_fd(signal_fd);
	TRACE("Created signal fd %d", ts->sigfd);

	return move_ptr(ts);
}

/**
 * lxc_terminal_signal_fini: uninstall signal handler
 *
 * @terminal: terminal instance
 *
 * Restore the saved signal handler that was in effect at the time
 * lxc_terminal_signal_init() was called.
 */
static void lxc_terminal_signal_fini(struct lxc_terminal *terminal)
{
	struct lxc_terminal_state *state = terminal->tty_state;

	if (!terminal->tty_state)
		return;

	state = terminal->tty_state;
	if (state->sigfd >= 0) {
		close(state->sigfd);

		if (pthread_sigmask(SIG_SETMASK, &state->oldmask, NULL) < 0)
			SYSWARN("Failed to restore signal mask");
	}

	free(terminal->tty_state);
	terminal->tty_state = NULL;
}

static int lxc_terminal_truncate_log_file(struct lxc_terminal *terminal)
{
	/* be very certain things are kosher */
	if (!terminal->log_path || terminal->log_fd < 0)
		return -EBADF;

	return lxc_unpriv(ftruncate(terminal->log_fd, 0));
}

#ifdef HAVE_ISULAD

int lxc_set_terminal_winsz(struct lxc_terminal *terminal, unsigned int height, unsigned int width)
{
	int ret = 0;
	struct winsize wsz;

	if (terminal->master < 0) {
		return 0;
	}

	ret = ioctl(terminal->master, TIOCGWINSZ, &wsz);
	if (ret < 0) {
		WARN("Failed to get window size");
		return -1;
	}
	wsz.ws_col = width;
	wsz.ws_row = height;

	ret = ioctl(terminal->master, TIOCSWINSZ, &wsz);
	if (ret < 0)
		WARN("Failed to set window size");
	else
		DEBUG("Set window size to %d columns and %d rows", wsz.ws_col,
		      wsz.ws_row);
	return ret;
}

/*
 * isulad: support mult-logfiles
 * */
static int lxc_terminal_rename_old_log_file(struct lxc_terminal *terminal)
{
	int ret;
	unsigned int i;
	char tmp[PATH_MAX] = {0};
	char *rename_fname = NULL;

	for (i = terminal->log_rotate - 1; i > 1; i--) {
		ret = snprintf(tmp, PATH_MAX, "%s.%u", terminal->log_path, i);
		if (ret < 0 || ret >= PATH_MAX) {
			free(rename_fname);
			return -EFBIG;
		}
		free(rename_fname);
		rename_fname = safe_strdup(tmp);
		ret = snprintf(tmp, PATH_MAX, "%s.%u", terminal->log_path, (i - 1));
		if (ret < 0 || ret >= PATH_MAX) {
			free(rename_fname);
			return -EFBIG;
		}
		ret = lxc_unpriv(rename(tmp, rename_fname));
		if (ret < 0 && errno != ENOENT) {
			free(rename_fname);
			return ret;
		}
	}

	free(rename_fname);
	return 0;
}
#endif

static int lxc_terminal_rotate_log_file(struct lxc_terminal *terminal)
{
	__do_free char *tmp = NULL;
	int ret;
	size_t len;

	if (!terminal->log_path || terminal->log_rotate == 0)
		return -EOPNOTSUPP;

	/* be very certain things are kosher */
	if (terminal->log_fd < 0)
		return -EBADF;

#ifdef HAVE_ISULAD
	/* isuald: rotate old log file first */
	ret = lxc_terminal_rename_old_log_file(terminal);
	if(ret != 0) {
		ERROR("Rename old log file failed");
		return ret;
	}
#endif

	len = strlen(terminal->log_path) + sizeof(".1");
	tmp = must_realloc(NULL, len);

	ret = snprintf(tmp, len, "%s.1", terminal->log_path);
	if (ret < 0 || (size_t)ret >= len)
		return -EFBIG;

	close(terminal->log_fd);
	terminal->log_fd = -1;
	ret = lxc_unpriv(rename(terminal->log_path, tmp));
	if (ret < 0)
		return ret;

	return lxc_terminal_create_log_file(terminal);
}

#ifndef HAVE_ISULAD
static int lxc_terminal_write_log_file(struct lxc_terminal *terminal, char *buf,
				       int bytes_read)
{
	int ret;
	struct stat st;
	int64_t space_left = -1;

	if (terminal->log_fd < 0)
		return 0;

	/* A log size <= 0 means that there's no limit on the size of the log
         * file at which point we simply ignore whether the log is supposed to
	 * be rotated or not.
	 */
	if (terminal->log_size <= 0)
		return lxc_write_nointr(terminal->log_fd, buf, bytes_read);

	/* Get current size of the log file. */
	ret = fstat(terminal->log_fd, &st);
	if (ret < 0) {
		SYSERROR("Failed to stat the terminal log file descriptor");
		return -1;
	}

	/* handle non-regular files */
	if ((st.st_mode & S_IFMT) != S_IFREG) {
		/* This isn't a regular file. so rotating the file seems a
		 * dangerous thing to do, size limits are also very
		 * questionable. Let's not risk anything and tell the user that
		 * he's requesting us to do weird stuff.
		 */
		if (terminal->log_rotate > 0 || terminal->log_size > 0)
			return -EINVAL;

		/* I mean, sure log wherever you want to. */
		return lxc_write_nointr(terminal->log_fd, buf, bytes_read);
	}

	space_left = terminal->log_size - st.st_size;

	/* User doesn't want to rotate the log file and there's no more space
	 * left so simply truncate it.
	 */
	if (space_left <= 0 && terminal->log_rotate <= 0) {
		ret = lxc_terminal_truncate_log_file(terminal);
		if (ret < 0)
			return ret;

		if (bytes_read <= terminal->log_size)
			return lxc_write_nointr(terminal->log_fd, buf, bytes_read);

		/* Write as much as we can into the buffer and loose the rest. */
		return lxc_write_nointr(terminal->log_fd, buf, terminal->log_size);
	}

	/* There's enough space left. */
	if (bytes_read <= space_left)
		return lxc_write_nointr(terminal->log_fd, buf, bytes_read);

	/* There's not enough space left but at least write as much as we can
	 * into the old log file.
	 */
	ret = lxc_write_nointr(terminal->log_fd, buf, space_left);
	if (ret < 0)
		return -1;

	/* Calculate how many bytes we still need to write. */
	bytes_read -= space_left;

	/* There'd be more to write but we aren't instructed to rotate the log
	 * file so simply return. There's no error on our side here.
	 */
	if (terminal->log_rotate > 0)
		ret = lxc_terminal_rotate_log_file(terminal);
	else
		ret = lxc_terminal_truncate_log_file(terminal);
	if (ret < 0)
		return ret;

	if (terminal->log_size < bytes_read) {
		/* Well, this is unfortunate because it means that there is more
		 * to write than the user has granted us space. There are
		 * multiple ways to handle this but let's use the simplest one:
		 * write as much as we can, tell the user that there was more
		 * stuff to write and move on.
		 * Note that this scenario shouldn't actually happen with the
		 * standard pty-based terminal that LXC allocates since it will
		 * be switched into raw mode. In raw mode only 1 byte at a time
		 * should be read and written.
		 */
		WARN("Size of terminal log file is smaller than the bytes to write");
		ret = lxc_write_nointr(terminal->log_fd, buf, terminal->log_size);
		if (ret < 0)
			return -1;
		bytes_read -= ret;
		return bytes_read;
	}

	/* Yay, we made it. */
	ret = lxc_write_nointr(terminal->log_fd, buf, bytes_read);
	if (ret < 0)
		return -1;
	bytes_read -= ret;
	return bytes_read;
}
#endif

#ifdef HAVE_ISULAD
/* get time buffer */
static bool get_time_buffer(struct timespec *timestamp, char *timebuffer,
                            size_t maxsize)
{
	struct tm tm_utc = { 0 };
	int32_t nanos = 0;
	time_t seconds;
	size_t len = 0;
	int ret = 0;

	if (!timebuffer || !maxsize) {
		return false;
	}

	seconds = (time_t)timestamp->tv_sec;
	gmtime_r(&seconds, &tm_utc);
	strftime(timebuffer, maxsize, "%Y-%m-%dT%H:%M:%S", &tm_utc);

	nanos = (int32_t)timestamp->tv_nsec;
	len = strlen(timebuffer);
	ret = snprintf(timebuffer + len, (maxsize - len), ".%09dZ", nanos);
	if (ret < 0 || ret >= (maxsize - len)) {
		return false;
	}

	return true;
}

/* get now time buffer */
static bool get_now_time_buffer(char *timebuffer, size_t maxsize)
{
	int err = 0;
	struct timespec ts;

	err = clock_gettime(CLOCK_REALTIME, &ts);
	if (err != 0) {
		ERROR("failed to get time");
		return false;
	}

	return get_time_buffer(&ts, timebuffer, maxsize);
}

static int isulad_lxc_terminal_rotate_write_data(struct lxc_terminal *terminal, const char *buf,
                int bytes_read)
{
	int ret;
	struct stat st;
	int64_t space_left = -1;

	if (terminal->log_fd < 0)
		return 0;

	/* A log size <= 0 means that there's no limit on the size of the log
	 * file at which point we simply ignore whether the log is supposed to
	 * be rotated or not.
	 */
	if (terminal->log_size <= 0)
		return lxc_write_nointr(terminal->log_fd, buf, bytes_read);

	/* Get current size of the log file. */
	ret = fstat(terminal->log_fd, &st);
	if (ret < 0) {
		SYSERROR("Failed to stat the terminal log file descriptor");
		return -1;
	}

	/* handle non-regular files */
	if ((st.st_mode & S_IFMT) != S_IFREG) {
		/* This isn't a regular file. so rotating the file seems a
		 * dangerous thing to do, size limits are also very
		 * questionable. Let's not risk anything and tell the user that
		 * he's requesting us to do weird stuff.
		 */
		if (terminal->log_rotate > 0 || terminal->log_size > 0)
			return -EINVAL;

		/* I mean, sure log wherever you want to. */
		return lxc_write_nointr(terminal->log_fd, buf, bytes_read);
	}

	space_left = terminal->log_size - st.st_size;

	/* User doesn't want to rotate the log file and there's no more space
	 * left so simply truncate it.
	 */
	if (space_left <= 0 && terminal->log_rotate <= 0) {
		ret = lxc_terminal_truncate_log_file(terminal);
		if (ret < 0)
			return ret;

		if (bytes_read <= terminal->log_size)
			return lxc_write_nointr(terminal->log_fd, buf, bytes_read);

		/* Write as much as we can into the buffer and loose the rest. */
		return lxc_write_nointr(terminal->log_fd, buf, terminal->log_size);
	}

	/* There's enough space left. */
	if (bytes_read <= space_left)
		return lxc_write_nointr(terminal->log_fd, buf, bytes_read);

	/* There'd be more to write but we aren't instructed to rotate the log
	 * file so simply return. There's no error on our side here.
	 */
	if (terminal->log_rotate > 0)
		ret = lxc_terminal_rotate_log_file(terminal);
	else
		ret = lxc_terminal_truncate_log_file(terminal);
	if (ret < 0)
		return ret;

	if (terminal->log_size < bytes_read) {
		/* Well, this is unfortunate because it means that there is more
		 * to write than the user has granted us space. There are
		 * multiple ways to handle this but let's use the simplest one:
		 * write as much as we can, tell the user that there was more
		 * stuff to write and move on.
		 * Note that this scenario shouldn't actually happen with the
		 * standard pty-based terminal that LXC allocates since it will
		 * be switched into raw mode. In raw mode only 1 byte at a time
		 * should be read and written.
		 */
		WARN("Size of terminal log file is smaller than the bytes to write");
		ret = lxc_write_nointr(terminal->log_fd, buf, terminal->log_size);
		if (ret < 0)
			return -1;
		bytes_read -= ret;
		return bytes_read;
	}

	/* Yay, we made it. */
	ret = lxc_write_nointr(terminal->log_fd, buf, bytes_read);
	if (ret < 0)
		return -1;
	bytes_read -= ret;
	return bytes_read;
}

static ssize_t isulad_logger_json_write(struct lxc_terminal *terminal, const char *type, const char *buf,
                                int bytes_read)
{
	logger_json_file *msg = NULL;
	ssize_t ret = -1;
	size_t len;
	char *json = NULL;
	char timebuffer[64] = { 0 };
	parser_error err = NULL;
	struct parser_context ctx = { GEN_OPTIONS_SIMPLIFY | GEN_OPTIONS_NOT_VALIDATE_UTF8, stderr };

	if (bytes_read < 0 || bytes_read >= INT_MAX) {
		return -1;
	}
	msg = calloc(sizeof(logger_json_file), 1);
	if (msg == NULL) {
		return -errno;
	}
	msg->log = calloc(bytes_read, 1);
	if (!msg->log) {
		goto cleanup;
	}
	memcpy(msg->log, buf, bytes_read);
	msg->log_len = bytes_read;
	msg->stream = type ? safe_strdup(type) : safe_strdup("stdout");

	get_now_time_buffer(timebuffer, sizeof(timebuffer));
	msg->time = safe_strdup(timebuffer);

	json = logger_json_file_generate_json(msg, &ctx, &err);
	if (!json) {
		ERROR("Failed to generate json: %s", err);
		goto cleanup;
	}
	len = strlen(json);
	json[len] = '\n';
	ret = isulad_lxc_terminal_rotate_write_data(terminal, json, len + 1);
cleanup:
	free(json);
	free_logger_json_file(msg);
	free(err);
	return ret;
}

static ssize_t isulad_logger_syslog_write(struct lxc_terminal *terminal, const char *buf)
{
        syslog(LOG_INFO, "%s", buf);
        return 0;
}

static inline bool is_syslog(const char *driver)
{
        if (driver == NULL) {
                return false;
        }

        return (strcmp("syslog", driver) == 0);
}

static inline ssize_t isulad_logger_write(struct lxc_terminal *terminal, const char *type, const char *buf,
                                int bytes_read)
{
        if (is_syslog(terminal->log_driver)) {
                return isulad_logger_syslog_write(terminal, buf);
        }

        return isulad_logger_json_write(terminal, type, buf, bytes_read);
}

static int isulad_lxc_terminal_write_log_file(struct lxc_terminal *terminal, const char *type, char *buf,
                                       int bytes_read)
{
#define __BUF_CACHE_SIZE (16 * LXC_TERMINAL_BUFFER_SIZE)
	static char cache[__BUF_CACHE_SIZE];
	static int size = 0;
	int upto, index;
	int begin = 0, buf_readed = 0,  buf_left = 0;
	int ret;

	if (buf != NULL && bytes_read > 0) {
		/* Work out how much more data we are okay with reading this time. */
		upto = size + bytes_read;
		if (upto > __BUF_CACHE_SIZE) {
			upto = __BUF_CACHE_SIZE;
		}

		if (upto > size) {
			buf_readed = upto - size;
			memcpy(cache + size, buf, buf_readed);
			buf_left = bytes_read - buf_readed;
			size += buf_readed;
		}
	}

	// If we have no data to log, and there's no more coming, we're done.
	if (size == 0)
		return 0;

	// Break up the data that we've buffered up into lines, and log each in turn.
	for (index = 0; index < size; index++) {
		if (cache[index] == '\n') {
			ret = isulad_logger_write(terminal, type, cache + begin, index - begin + 1);
			if (ret < 0) {
				WARN("Failed to log msg");
			}
			begin = index + 1;
		}
	}
	/* If there's no more coming, or the buffer is full but
	 * has no newlines, log whatever we haven't logged yet,
	 * noting that it's a partial log line. */
	if (buf == NULL || (begin == 0 && size == __BUF_CACHE_SIZE)) {
		if (begin < size) {
			ret = isulad_logger_write(terminal, type, cache + begin, size - begin);
			if (ret < 0) {
				WARN("Failed to log msg");
			}
			begin = 0;
			size = 0;
		}
		if (buf == NULL) {
			return 0;
		}
	}
	/* Move any unlogged data to the front of the buffer in preparation for another read. */
	if (begin > 0) {
		memcpy(cache, cache + begin, size - begin);
		size -= begin;
	}
	/* Move left data to cache buffer */
	if (buf_left > 0) {
		memcpy(cache + size, buf + buf_readed, buf_left);
		size += buf_left;
	}
	return 0;
}

/* isulad: forward data to all fifos */
static void lxc_forward_data_to_fifo(struct lxc_list *list, bool is_err, const char *buf, int r)
{
	struct lxc_list *it  = NULL;
	struct lxc_list *next = NULL;
	struct lxc_fifos_fd *elem = NULL;
	ssize_t w = 0;

	lxc_list_for_each_safe(it, list, next) {
		elem = it->elem;
		if (is_err) {
			if (elem->err_fd >= 0) {
				w = lxc_write_nointr_for_fifo(elem->err_fd, buf, r);
				if (w != r) {
					WARN("Failed to write to fifo fd %d with error: %s", elem->err_fd, strerror(errno));
				}
			}
		} else {
			if (elem->out_fd >= 0) {
				w = lxc_write_nointr_for_fifo(elem->out_fd, buf, r);
				if (w != r) {
					WARN("Failed to write to fifo fd %d with error: %s", elem->out_fd, strerror(errno));
				}
			}
		}
	}

	return;
}

/* isulad: judge the fd whether is fifo */
static bool lxc_terminal_is_fifo(int fd, struct lxc_list *list)
{
	struct lxc_list *it = NULL;
	struct lxc_list *next = NULL;
	struct lxc_fifos_fd *elem = NULL;

	lxc_list_for_each_safe(it, list, next) {
		elem = it->elem;
		if (elem->in_fd == fd)
			return true;
	}

	return false;
}

/* isulad: if fd == -1, means delete all the fifos*/
int lxc_terminal_delete_fifo(int fd, struct lxc_list *list)
{
	struct lxc_list *it = NULL;
	struct lxc_list *next = NULL;
	struct lxc_fifos_fd *elem = NULL;

	lxc_list_for_each_safe(it, list, next) {
		elem = it->elem;
		if (elem->in_fd == fd || -1 == fd) {
			INFO("Delete fifo fd %d", fd);
			lxc_list_del(it);
			if (elem->in_fifo)
				free(elem->in_fifo);
			if (elem->out_fifo)
				free(elem->out_fifo);
			if (elem->err_fifo)
				free(elem->err_fifo);
			if (elem->in_fd >= 0)
				close(elem->in_fd);
			if (elem->out_fd >= 0)
				close(elem->out_fd);
			if (elem->err_fd >= 0)
				close(elem->err_fd);
			free(elem);
		}
	}

	return 0;
}

int lxc_terminal_io_cb(int fd, uint32_t events, void *data,
                       struct lxc_epoll_descr *descr)
{
	struct lxc_terminal *terminal = data;
	char buf[2 * LXC_TERMINAL_BUFFER_SIZE];
	int r, w, w_log, w_rbuf;

	w = r = lxc_read_nointr(fd, buf, sizeof(buf));
	if (r <= 0) {
		INFO("Terminal client on fd %d has exited", fd);
		lxc_mainloop_del_handler(descr, fd);

		if (fd == terminal->master) {
			terminal->master = -EBADF;
			/* write remained buffer to terminal log */
			if (terminal->log_fd >= 0) {
				w_log = isulad_lxc_terminal_write_log_file(terminal, "stdout", NULL, 0);
				if (w_log < 0)
					TRACE("Failed to write %d bytes to terminal log", r);
			}
			/* notes: do not close the master fd due to if we close the fd, the process may
			 * recive SIGHUP and the exit code will be 129 (128 + 1)
			 */
			return LXC_MAINLOOP_CLOSE;
		} else if (fd == terminal->peer) {
			lxc_terminal_signal_fini(terminal);
			terminal->peer = -EBADF;
			close(fd);
			return LXC_MAINLOOP_CONTINUE; /* isulad: do not close mainloop when peer close*/
		} else if (lxc_terminal_is_fifo(fd, &terminal->fifos)) {
			/* isulad: delete fifos when the client close */
			lxc_terminal_delete_fifo(fd, &terminal->fifos);
			return LXC_MAINLOOP_CONTINUE;
		} else if (fd == terminal->pipes[1][0] || fd == terminal->pipes[2][0]) {
			if (fd == terminal->pipes[1][0]) {
				if (terminal->log_fd >= 0) {
					w_log = isulad_lxc_terminal_write_log_file(terminal, "stdout", NULL, 0);
				}
				terminal->pipes[1][0] = -EBADF;
			} else if (fd == terminal->pipes[2][0]) {
				if (terminal->log_fd >= 0) {
					w_log = isulad_lxc_terminal_write_log_file(terminal, "stderr", NULL, 0);
				}
				terminal->pipes[2][0] = -EBADF;
			}
			/* notes: do not close the master fd due to if we close the fd, the process may
			 * recive SIGHUP and the exit code will be 141 (128 + 13)
			 */
			return LXC_MAINLOOP_CONTINUE;
		} else if (fd == terminal->pipes[0][1]) {
			TRACE("closed stdin pipe of container stdin");
			terminal->pipes[0][1] = -EBADF;
			return LXC_MAINLOOP_CONTINUE;
		} else {
			WARN("Handler received unexpected file descriptor");
		}
		close(fd);
		return LXC_MAINLOOP_CLOSE;
	}

	if (fd == terminal->peer || lxc_terminal_is_fifo(fd, &terminal->fifos)) {
		if (terminal->master > 0)
			w = lxc_write_nointr(terminal->master, buf, r);
		if (terminal->pipes[0][1] > 0)
			w = lxc_write_nointr(terminal->pipes[0][1], buf, r);
	}

	w_rbuf = w_log = 0;
	if (fd == terminal->master || fd == terminal->pipes[1][0] || fd == terminal->pipes[2][0]) {
		/* write to peer first */
		if (terminal->peer >= 0)
			w = lxc_write_nointr(terminal->peer, buf, r);

		/* isulad: forward data to fifos */
		lxc_forward_data_to_fifo(&terminal->fifos, fd == terminal->pipes[2][0], buf, r);

		/* write to terminal ringbuffer */
		if (terminal->buffer_size > 0)
			w_rbuf = lxc_ringbuf_write(&terminal->ringbuf, buf, r);

		/* write to terminal log */
		if (terminal->log_fd >= 0) {
			if (fd == terminal->master || fd == terminal->pipes[1][0])
				w_log = isulad_lxc_terminal_write_log_file(terminal, "stdout", buf, r);
			else if (fd == terminal->pipes[2][0])
				w_log = isulad_lxc_terminal_write_log_file(terminal, "stderr", buf, r);
		}
	}

	if (w != r)
		WARN("Short write on terminal r:%d != w:%d", r, w);

	if (w_rbuf < 0) {
		errno = -w_rbuf;
		SYSTRACE("Failed to write %d bytes to terminal ringbuffer", r);
	}

	if (w_log < 0)
		TRACE("Failed to write %d bytes to terminal log", r);

	return LXC_MAINLOOP_CONTINUE;
}
#else
int lxc_terminal_io_cb(int fd, uint32_t events, void *data,
		       struct lxc_epoll_descr *descr)
{
	struct lxc_terminal *terminal = data;
	char buf[LXC_TERMINAL_BUFFER_SIZE];
	int r, w, w_log, w_rbuf;

	w = r = lxc_read_nointr(fd, buf, sizeof(buf));
	if (r <= 0) {
		INFO("Terminal client on fd %d has exited", fd);
		lxc_mainloop_del_handler(descr, fd);

		if (fd == terminal->master) {
			terminal->master = -EBADF;
		} else if (fd == terminal->peer) {
			lxc_terminal_signal_fini(terminal);
			terminal->peer = -EBADF;
		} else {
			ERROR("Handler received unexpected file descriptor");
		}
		close(fd);

		return LXC_MAINLOOP_CLOSE;
	}

	if (fd == terminal->peer)
		w = lxc_write_nointr(terminal->master, buf, r);

	w_rbuf = w_log = 0;
	if (fd == terminal->master) {
		/* write to peer first */
		if (terminal->peer >= 0)
			w = lxc_write_nointr(terminal->peer, buf, r);

		/* write to terminal ringbuffer */
		if (terminal->buffer_size > 0)
			w_rbuf = lxc_ringbuf_write(&terminal->ringbuf, buf, r);

		/* write to terminal log */
		if (terminal->log_fd >= 0)
			w_log = lxc_terminal_write_log_file(terminal, buf, r);
	}

	if (w != r)
		WARN("Short write on terminal r:%d != w:%d", r, w);

	if (w_rbuf < 0) {
		errno = -w_rbuf;
		SYSTRACE("Failed to write %d bytes to terminal ringbuffer", r);
	}

	if (w_log < 0)
		TRACE("Failed to write %d bytes to terminal log", r);

	return LXC_MAINLOOP_CONTINUE;
}
#endif

static int lxc_terminal_mainloop_add_peer(struct lxc_terminal *terminal)
{
	int ret;

	if (terminal->peer >= 0) {
		ret = lxc_mainloop_add_handler(terminal->descr, terminal->peer,
					       lxc_terminal_io_cb, terminal);
		if (ret < 0) {
			WARN("Failed to add terminal peer handler to mainloop");
			return -1;
		}
	}

	if (!terminal->tty_state || terminal->tty_state->sigfd < 0)
		return 0;

	ret = lxc_mainloop_add_handler(terminal->descr, terminal->tty_state->sigfd,
				       lxc_terminal_signalfd_cb, terminal->tty_state);
	if (ret < 0) {
		WARN("Failed to add signal handler to mainloop");
		return -1;
	}

	return 0;
}

#ifdef HAVE_ISULAD
/* isulad add pipes to mainloop */
static int lxc_terminal_mainloop_add_pipes(struct lxc_terminal *terminal)
{
	int ret = 0;

	// parent read data from fifo, and send to stdin of container
	if (terminal->pipes[0][1] > 0) {
		ret = lxc_mainloop_add_handler(terminal->descr, terminal->pipes[0][1],
		                               lxc_terminal_io_cb, terminal);
		if (ret) {
			ERROR("pipe fd %d not added to mainloop", terminal->pipes[0][1]);
			return -1;
		}
	}
	// parent read data from stdout of container, and send to fifo
	if (terminal->pipes[1][0] > 0) {
		ret = lxc_mainloop_add_handler(terminal->descr, terminal->pipes[1][0],
		                               lxc_terminal_io_cb, terminal);
		if (ret) {
			ERROR("pipe fd %d not added to mainloop", terminal->pipes[1][0]);
			return -1;
		}
	}
	// parent read data from stderr of container, and send to fifo
	if (terminal->pipes[2][0] > 0) {
		ret = lxc_mainloop_add_handler(terminal->descr, terminal->pipes[2][0],
		                               lxc_terminal_io_cb, terminal);
		if (ret) {
			ERROR("pipe fd %d not added to mainloop", terminal->pipes[2][0]);
			return -1;
		}
	}
	return ret;
}

/* isulad add fifo to mainloop */
static int lxc_terminal_mainloop_add_fifo(struct lxc_terminal *terminal)
{
	int ret = 0;
	struct lxc_list *it = NULL;
	struct lxc_list *next = NULL;
	struct lxc_fifos_fd *elem = NULL;

	lxc_list_for_each_safe(it, &terminal->fifos, next) {
		elem = it->elem;
		if (elem->in_fd >= 0) {
			ret = lxc_mainloop_add_handler(terminal->descr, elem->in_fd,
			                               lxc_terminal_io_cb, terminal);
			if (ret) {
				ERROR("console fifo %s not added to mainloop", elem->in_fifo);
				return -1;
			}
		}
	}
	return ret;
}

int lxc_terminal_mainloop_add(struct lxc_epoll_descr *descr,
                              struct lxc_terminal *terminal)
{
	int ret;

	/* We cache the descr so that we can add an fd to it when someone
	 * does attach to it in lxc_terminal_allocate().
	 */
	terminal->descr = descr;

	ret = lxc_terminal_mainloop_add_peer(terminal);
	if (ret < 0) {
		ERROR("Failed to add handler for terminal peer to mainloop");
		return -1;
	}

	/* isulad add pipes to mainloop */
	ret = lxc_terminal_mainloop_add_pipes(terminal);
	if (ret < 0) {
		ERROR("Failed to add handler for terminal fifos to mainloop");
		return -1;
	}

	/* isulad add fifo to mainloop */
	ret = lxc_terminal_mainloop_add_fifo(terminal);
	if (ret < 0) {
		ERROR("Failed to add handler for terminal fifos to mainloop");
		return -1;
	}

	if (terminal->master < 0) {
		INFO("Terminal is not initialized");
		return 0;
	}

	ret = lxc_mainloop_add_handler(descr, terminal->master,
	                               lxc_terminal_io_cb, terminal);
	if (ret < 0) {
		ERROR("Failed to add handler for terminal master fd %d to "
		      "mainloop", terminal->master);
		return -1;
	}

	return 0;
}
#else
int lxc_terminal_mainloop_add(struct lxc_epoll_descr *descr,
			      struct lxc_terminal *terminal)
{
	int ret;

	if (terminal->master < 0) {
		INFO("Terminal is not initialized");
		return 0;
	}

	ret = lxc_mainloop_add_handler(descr, terminal->master,
				       lxc_terminal_io_cb, terminal);
	if (ret < 0) {
		ERROR("Failed to add handler for terminal master fd %d to "
		      "mainloop", terminal->master);
		return -1;
	}

	/* We cache the descr so that we can add an fd to it when someone
	 * does attach to it in lxc_terminal_allocate().
	 */
	terminal->descr = descr;

	return lxc_terminal_mainloop_add_peer(terminal);
}
#endif

int lxc_setup_tios(int fd, struct termios *oldtios)
{
	int ret;
	struct termios newtios;

	if (!isatty(fd)) {
		ERROR("File descriptor %d does not refer to a terminal", fd);
		return -1;
	}

	/* Get current termios. */
	ret = tcgetattr(fd, oldtios);
	if (ret < 0) {
		SYSERROR("Failed to get current terminal settings");
		return -1;
	}

	/* ensure we don't end up in an endless loop:
	 * The kernel might fire SIGTTOU while an
	 * ioctl() in tcsetattr() is executed. When the ioctl()
	 * is resumed and retries, the signal handler interrupts it again.
	 */
	signal (SIGTTIN, SIG_IGN);
	signal (SIGTTOU, SIG_IGN);

	newtios = *oldtios;

	/* We use the same settings that ssh does. */
	newtios.c_iflag |= IGNPAR;
	newtios.c_iflag &= ~(ISTRIP | INLCR | IGNCR | ICRNL | IXON | IXANY | IXOFF);
#ifdef IUCLC
	newtios.c_iflag &= ~IUCLC;
#endif
	newtios.c_lflag &= ~(TOSTOP | ISIG | ICANON | ECHO | ECHOE | ECHOK | ECHONL);
#ifdef IEXTEN
	newtios.c_lflag &= ~IEXTEN;
#endif
	newtios.c_oflag &= ~ONLCR;
	newtios.c_oflag |= OPOST;
	newtios.c_cc[VMIN] = 1;
	newtios.c_cc[VTIME] = 0;

	/* Set new attributes. */
	ret = tcsetattr(fd, TCSAFLUSH, &newtios);
	if (ret < 0) {
		ERROR("Failed to set new terminal settings");
		return -1;
	}

	return 0;
}

static void lxc_terminal_peer_proxy_free(struct lxc_terminal *terminal)
{
	lxc_terminal_signal_fini(terminal);

	close(terminal->proxy.master);
	terminal->proxy.master = -1;

	close(terminal->proxy.slave);
	terminal->proxy.slave = -1;

	terminal->proxy.busy = -1;

	terminal->proxy.name[0] = '\0';

	terminal->peer = -1;
}

static int lxc_terminal_peer_proxy_alloc(struct lxc_terminal *terminal,
					 int sockfd)
{
	int ret;
	struct termios oldtermio;
	struct lxc_terminal_state *ts;

	if (terminal->master < 0) {
		ERROR("Terminal not set up");
		return -1;
	}

	if (terminal->proxy.busy != -1 || terminal->peer != -1) {
		NOTICE("Terminal already in use");
		return -1;
	}

	if (terminal->tty_state) {
		ERROR("Terminal has already been initialized");
		return -1;
	}

	/* This is the proxy terminal that will be given to the client, and
	 * that the real terminal master will send to / recv from.
	 */
	ret = openpty(&terminal->proxy.master, &terminal->proxy.slave, NULL,
		      NULL, NULL);
	if (ret < 0) {
		SYSERROR("Failed to open proxy terminal");
		return -1;
	}

	ret = ttyname_r(terminal->proxy.slave, terminal->proxy.name,
			sizeof(terminal->proxy.name));
	if (ret < 0) {
		SYSERROR("Failed to retrieve name of proxy terminal slave");
		goto on_error;
	}

	ret = fd_cloexec(terminal->proxy.master, true);
	if (ret < 0) {
		SYSERROR("Failed to set FD_CLOEXEC flag on proxy terminal master");
		goto on_error;
	}

	ret = fd_cloexec(terminal->proxy.slave, true);
	if (ret < 0) {
		SYSERROR("Failed to set FD_CLOEXEC flag on proxy terminal slave");
		goto on_error;
	}

	ret = lxc_setup_tios(terminal->proxy.slave, &oldtermio);
	if (ret < 0)
		goto on_error;

	ts = lxc_terminal_signal_init(terminal->proxy.master, terminal->master);
	if (!ts)
		goto on_error;

	terminal->tty_state = ts;
	terminal->peer = terminal->proxy.slave;
	terminal->proxy.busy = sockfd;
	ret = lxc_terminal_mainloop_add_peer(terminal);
	if (ret < 0)
		goto on_error;

	NOTICE("Opened proxy terminal with master fd %d and slave fd %d",
	       terminal->proxy.master, terminal->proxy.slave);
	return 0;

on_error:
	lxc_terminal_peer_proxy_free(terminal);
	return -1;
}

int lxc_terminal_allocate(struct lxc_conf *conf, int sockfd, int *ttyreq)
{
	int ttynum;
	int masterfd = -1;
	struct lxc_tty_info *ttys = &conf->ttys;
	struct lxc_terminal *terminal = &conf->console;

	if (*ttyreq == 0) {
		int ret;

		ret = lxc_terminal_peer_proxy_alloc(terminal, sockfd);
		if (ret < 0)
			goto out;

		masterfd = terminal->proxy.master;
		goto out;
	}

	if (*ttyreq > 0) {
		if (*ttyreq > ttys->max)
			goto out;

		if (ttys->tty[*ttyreq - 1].busy >= 0)
			goto out;

		/* The requested tty is available. */
		ttynum = *ttyreq;
		goto out_tty;
	}

	/* Search for next available tty, fixup index tty1 => [0]. */
	for (ttynum = 1; ttynum <= ttys->max && ttys->tty[ttynum - 1].busy >= 0; ttynum++) {
		;
	}

	/* We didn't find any available slot for tty. */
	if (ttynum > ttys->max)
		goto out;

	*ttyreq = ttynum;

out_tty:
	ttys->tty[ttynum - 1].busy = sockfd;
	masterfd = ttys->tty[ttynum - 1].master;

out:
	return masterfd;
}

void lxc_terminal_free(struct lxc_conf *conf, int fd)
{
	int i;
	struct lxc_tty_info *ttys = &conf->ttys;
	struct lxc_terminal *terminal = &conf->console;

	for (i = 0; i < ttys->max; i++)
		if (ttys->tty[i].busy == fd)
			ttys->tty[i].busy = -1;

	if (terminal->proxy.busy != fd)
		return;

	lxc_mainloop_del_handler(terminal->descr, terminal->proxy.slave);
	lxc_terminal_peer_proxy_free(terminal);
}

static int lxc_terminal_peer_default(struct lxc_terminal *terminal)
{
	struct lxc_terminal_state *ts = NULL;
	const char *path = NULL;
	int ret = 0;

	if (terminal->path)
		path = terminal->path;

#ifdef HAVE_ISULAD
	/* isulad: if no console was given, try current controlling terminal, there
	 * won't be one if we were started as a daemon (-d)
	 */
	if (!path && !access("/dev/tty", F_OK)) {
		int fd;
		fd = open("/dev/tty", O_RDWR);
		if (fd >= 0) {
			close(fd);
			path = "/dev/tty";
		}
	}

	if (!path) {
		DEBUG("Not have a controlling terminal");
		return 0;
	}
#endif

	terminal->peer = lxc_unpriv(open(path, O_RDWR | O_CLOEXEC));
	if (terminal->peer < 0) {
		if (!terminal->path) {
			errno = ENODEV;
			SYSDEBUG("The process does not have a controlling terminal");
			goto on_succes;
		}

		SYSERROR("Failed to open proxy terminal \"%s\"", path);
		return -ENOTTY;
	}
	DEBUG("Using terminal \"%s\" as proxy", path);

	if (!isatty(terminal->peer)) {
		ERROR("File descriptor for \"%s\" does not refer to a terminal", path);
		goto on_error_free_tios;
	}

	ts = lxc_terminal_signal_init(terminal->peer, terminal->master);
	terminal->tty_state = ts;
	if (!ts) {
		WARN("Failed to install signal handler");
		goto on_error_free_tios;
	}

	lxc_terminal_winsz(terminal->peer, terminal->master);

	terminal->tios = malloc(sizeof(*terminal->tios));
	if (!terminal->tios)
		goto on_error_free_tios;

	ret = lxc_setup_tios(terminal->peer, terminal->tios);
	if (ret < 0)
		goto on_error_close_peer;
	else
		goto on_succes;

on_error_free_tios:
	free(terminal->tios);
	terminal->tios = NULL;

on_error_close_peer:
	close(terminal->peer);
	terminal->peer = -1;
	ret = -ENOTTY;

on_succes:
	return ret;
}

int lxc_terminal_write_ringbuffer(struct lxc_terminal *terminal)
{
	char *r_addr;
	ssize_t ret;
	uint64_t used;
	struct lxc_ringbuf *buf = &terminal->ringbuf;

	/* There's not log file where we can dump the ringbuffer to. */
	if (terminal->log_fd < 0)
		return 0;

	used = lxc_ringbuf_used(buf);
	if (used == 0)
		return 0;

	ret = lxc_terminal_truncate_log_file(terminal);
	if (ret < 0)
		return ret;

	/* Write as much as we can without exceeding the limit. */
	if (terminal->log_size < used)
		used = terminal->log_size;

	r_addr = lxc_ringbuf_get_read_addr(buf);
	ret = lxc_write_nointr(terminal->log_fd, r_addr, used);
	if (ret < 0)
		return -EIO;

	return 0;
}

void lxc_terminal_delete(struct lxc_terminal *terminal)
{
	int ret;

	ret = lxc_terminal_write_ringbuffer(terminal);
	if (ret < 0)
		WARN("Failed to write terminal log to disk");

	if (terminal->tios && terminal->peer >= 0) {
		ret = tcsetattr(terminal->peer, TCSAFLUSH, terminal->tios);
		if (ret < 0)
			SYSWARN("Failed to set old terminal settings");
	}
	free(terminal->tios);
	terminal->tios = NULL;

	if (terminal->peer >= 0)
		close(terminal->peer);
	terminal->peer = -1;

	if (terminal->master >= 0)
		close(terminal->master);
	terminal->master = -1;

	if (terminal->slave >= 0)
		close(terminal->slave);
	terminal->slave = -1;

	if (terminal->log_fd >= 0)
		close(terminal->log_fd);
	terminal->log_fd = -1;

#ifdef HAVE_ISULAD
        if (is_syslog(terminal->log_driver)) {
                closelog();
                free(terminal->log_driver);
	}
	/* isulad: close all pipes */
	if (terminal->pipes[0][0] >= 0)
		close(terminal->pipes[0][0]);
	terminal->pipes[0][0] = -1;
	if (terminal->pipes[0][1] >= 0)
		close(terminal->pipes[0][1]);
	terminal->pipes[0][1] = -1;
	if (terminal->pipes[1][0] >= 0)
		close(terminal->pipes[1][0]);
	terminal->pipes[1][0] = -1;
	if (terminal->pipes[1][1] >= 0)
		close(terminal->pipes[1][1]);
	terminal->pipes[1][1] = -1;
	if (terminal->pipes[2][0] >= 0)
		close(terminal->pipes[2][0]);
	terminal->pipes[2][0] = -1;
	if (terminal->pipes[2][1] >= 0)
		close(terminal->pipes[2][1]);
	terminal->pipes[2][1] = -1;

	/* isulad: delete all fifos */
	lxc_terminal_delete_fifo(-1, &terminal->fifos);
#endif
}

/**
 * Note that this function needs to run before the mainloop starts. Since we
 * register a handler for the terminal's masterfd when we create the mainloop
 * the terminal handler needs to see an allocated ringbuffer.
 */
static int lxc_terminal_create_ringbuf(struct lxc_terminal *terminal)
{
	int ret;
	struct lxc_ringbuf *buf = &terminal->ringbuf;
	uint64_t size = terminal->buffer_size;

	/* no ringbuffer previously allocated and no ringbuffer requested */
	if (!buf->addr && size <= 0)
		return 0;

	/* ringbuffer allocated but no new ringbuffer requested */
	if (buf->addr && size <= 0) {
		lxc_ringbuf_release(buf);
		buf->addr = NULL;
		buf->r_off = 0;
		buf->w_off = 0;
		buf->size = 0;
		TRACE("Deallocated terminal ringbuffer");
		return 0;
	}

	if (size <= 0)
		return 0;

	/* check wether the requested size for the ringbuffer has changed */
	if (buf->addr && buf->size != size) {
		TRACE("Terminal ringbuffer size changed from %" PRIu64
		      " to %" PRIu64 " bytes. Deallocating terminal ringbuffer",
		      buf->size, size);
		lxc_ringbuf_release(buf);
	}

	ret = lxc_ringbuf_create(buf, size);
	if (ret < 0) {
		ERROR("Failed to setup %" PRIu64 " byte terminal ringbuffer", size);
		return -1;
	}

	TRACE("Allocated %" PRIu64 " byte terminal ringbuffer", size);
	return 0;
}

/**
 * This is the terminal log file. Please note that the terminal log file is
 * (implementation wise not content wise) independent of the terminal ringbuffer.
 */
int lxc_terminal_create_log_file(struct lxc_terminal *terminal)
{
	if (!terminal->log_path)
		return 0;

	terminal->log_fd = lxc_unpriv(open(terminal->log_path, O_CLOEXEC | O_RDWR | O_CREAT | O_APPEND, 0600));
	if (terminal->log_fd < 0) {
		SYSERROR("Failed to open terminal log file \"%s\"", terminal->log_path);
		return -1;
	}

	DEBUG("Using \"%s\" as terminal log file", terminal->log_path);
	return 0;
}

#ifdef HAVE_ISULAD
/* isulad: fd_nonblock */
static int fd_nonblock(int fd)
{
	int flags;

	flags = fcntl(fd, F_GETFL);

	return fcntl(fd, F_SETFL, (int)((unsigned int)flags | O_NONBLOCK));
}

static int terminal_fifo_open(const char *fifo_path, int flags)
{
	int fd = -1;

	fd = lxc_open(fifo_path, flags, 0);
	if (fd < 0) {
		WARN("Failed to open fifo %s to send message: %s.", fifo_path,
		     strerror(errno));
		return -1;
	}

	return fd;
}

bool fifo_exists(const char *path)
{
	struct stat sb;
	int ret;

	ret = stat(path, &sb);
	if (ret < 0)
		// could be something other than eexist, just say no
		return false;
	return S_ISFIFO(sb.st_mode);
}

/* isulad: set terminal fifos */
static int lxc_terminal_set_fifo(struct lxc_terminal *console, const char *in, const char *out, const char *err, int *input_fd)
{
	int fifofd_in = -1, fifofd_out = -1, fifofd_err = -1;
	struct lxc_fifos_fd *fifo_elem = NULL;

	if ((in && !fifo_exists(in)) || (out && !fifo_exists(out)) || (err && !fifo_exists(err))) {
		ERROR("File %s or %s or %s does not refer to a FIFO", in, out, err);
		return -1;
	}

	if (in) {
		fifofd_in = terminal_fifo_open(in, O_RDONLY | O_NONBLOCK | O_CLOEXEC);
		if (fifofd_in < 0) {
			SYSERROR("Failed to open FIFO: %s", in);
			return -1;
		}
	}

	if (out) {
		fifofd_out = terminal_fifo_open(out, O_WRONLY | O_NONBLOCK | O_CLOEXEC);
		if (fifofd_out < 0) {
			SYSERROR("Failed to open FIFO: %s", out);
			if (fifofd_in >= 0)
				close(fifofd_in);
			return -1;
		}
	}

	if (err) {
		fifofd_err = terminal_fifo_open(err, O_WRONLY | O_NONBLOCK | O_CLOEXEC);
		if (fifofd_err < 0) {
			SYSERROR("Failed to open FIFO: %s", err);
			if (fifofd_in >= 0)
				close(fifofd_in);
			if (fifofd_out >= 0)
				close(fifofd_out);
			return -1;
		}
	}

	fifo_elem = malloc(sizeof(*fifo_elem));
	if (fifo_elem == NULL) {
		if (fifofd_in >= 0)
			close(fifofd_in);
		if (fifofd_out >= 0)
			close(fifofd_out);
		if (fifofd_err >= 0)
			close(fifofd_err);
		return -1;
	}
	memset(fifo_elem, 0, sizeof(*fifo_elem));

	fifo_elem->in_fifo = safe_strdup(in ? in : "");
	fifo_elem->out_fifo = safe_strdup(out ? out : "");
	fifo_elem->err_fifo = safe_strdup(err ? err : "");
	fifo_elem->in_fd = fifofd_in;
	fifo_elem->out_fd = fifofd_out;
	fifo_elem->err_fd = fifofd_err;
	lxc_list_add_elem(&fifo_elem->node, fifo_elem);
	lxc_list_add_tail(&console->fifos, &fifo_elem->node);

	if (input_fd)
		*input_fd = fifofd_in;

	return 0;
}

/* isulad: add default fifos */
static int lxc_terminal_fifo_default(struct lxc_terminal *terminal)
{
	if (terminal->init_fifo[0] || terminal->init_fifo[1] || terminal->init_fifo[2])
		return lxc_terminal_set_fifo(terminal, terminal->init_fifo[0], terminal->init_fifo[1], terminal->init_fifo[2], NULL);
	return 0;
}

int lxc_terminal_create(struct lxc_terminal *terminal)
{
	int ret;

	if (!terminal->disable_pty) {
		ret = openpty(&terminal->master, &terminal->slave, NULL, NULL, NULL);
		if (ret < 0) {
			SYSERROR("Failed to open terminal");
			return -1;
		}

		ret = ttyname_r(terminal->slave, terminal->name, sizeof(terminal->name));
		if (ret < 0) {
			SYSERROR("Failed to retrieve name of terminal slave");
			goto err;
		}

		ret = fd_cloexec(terminal->master, true);
		if (ret < 0) {
			SYSERROR("Failed to set FD_CLOEXEC flag on terminal master");
			goto err;
		}

		/* isulad: make master NONBLOCK */
		ret = fd_nonblock(terminal->master);
		if (ret < 0) {
			SYSERROR("Failed to set O_NONBLOCK flag on terminal master");
			goto err;
		}

		ret = fd_cloexec(terminal->slave, true);
		if (ret < 0) {
			SYSERROR("Failed to set FD_CLOEXEC flag on terminal slave");
			goto err;
		}

		ret = lxc_terminal_peer_default(terminal);
		if (ret < 0) {
			ERROR("Failed to allocate proxy terminal");
			goto err;
		}
	} else {
		/* isulad: create 3 pipes */
		/* for stdin */
		if (pipe2(terminal->pipes[0], O_CLOEXEC)) {
			ERROR("Failed to create stdin pipe");
			goto err;
		}

		/* for stdout */
		if (pipe2(terminal->pipes[1], O_CLOEXEC)) {
			ERROR("Failed to create stdout pipe");
			goto err;
		}
		/* for stderr */
		if (pipe2(terminal->pipes[2], O_CLOEXEC)) {
			ERROR("Failed to create stderr pipe");
			goto err;
		}
	}

	/* isulad: open fifos */
	ret = lxc_terminal_fifo_default(terminal);
	if (ret < 0) {
		ERROR("Failed to allocate fifo terminal");
		goto err;
	}

	return 0;

err:
	lxc_terminal_delete(terminal);
	return -ENODEV;
}

/* isulad: add fifos dynamic*/
int lxc_terminal_add_fifos(struct lxc_conf *conf, const char *fifonames)
{
	int ret = 0;
	struct lxc_terminal *terminal = &conf->console;
	int fifofd_in = -1;
	char *tmp = NULL, *saveptr = NULL, *in = NULL, *out = NULL, *err = NULL;
	const char *none_fifo_name = "none";

	tmp = safe_strdup(fifonames);

	in = strtok_r(tmp, "&&&&", &saveptr);
	if (!in) {
		ret = -1;
		goto free_out;
	}
	if (strcmp(in, none_fifo_name) == 0)
		in = NULL;

	out = strtok_r(NULL, "&&&&",  &saveptr);
	if (!out) {
		ret = -1;
		goto free_out;
	}
	if (strcmp(out, none_fifo_name) == 0)
		out = NULL;

	err = strtok_r(NULL, "&&&&",  &saveptr);
	if (!err) {
		ret = -1;
		goto free_out;
	}
	if (strcmp(err, none_fifo_name) == 0)
		err = NULL;

	ret = lxc_terminal_set_fifo(terminal, in, out, err, &fifofd_in);
	if (ret < 0) {
		ERROR("Faild to set fifos to console config");
		ret = -1;
		goto free_out;
	}

	if (lxc_mainloop_add_handler(terminal->descr, fifofd_in,
	                             lxc_terminal_io_cb, terminal)) {
		ERROR("console fifo not added to mainloop");
		lxc_terminal_delete_fifo(fifofd_in, &terminal->fifos);
		ret = -1;
		goto free_out;
	}

free_out:
	if (tmp)
		free(tmp);
	return ret;
}

#else
int lxc_terminal_create(struct lxc_terminal *terminal)
{
	int ret;

	ret = openpty(&terminal->master, &terminal->slave, NULL, NULL, NULL);
	if (ret < 0) {
		SYSERROR("Failed to open terminal");
		return -1;
	}

	ret = ttyname_r(terminal->slave, terminal->name, sizeof(terminal->name));
	if (ret < 0) {
		SYSERROR("Failed to retrieve name of terminal slave");
		goto err;
	}

	ret = fd_cloexec(terminal->master, true);
	if (ret < 0) {
		SYSERROR("Failed to set FD_CLOEXEC flag on terminal master");
		goto err;
	}

	ret = fd_cloexec(terminal->slave, true);
	if (ret < 0) {
		SYSERROR("Failed to set FD_CLOEXEC flag on terminal slave");
		goto err;
	}

	ret = lxc_terminal_peer_default(terminal);
	if (ret < 0) {
		ERROR("Failed to allocate proxy terminal");
		goto err;
	}

	return 0;

err:
	lxc_terminal_delete(terminal);
	return -ENODEV;
}
#endif

int lxc_terminal_setup(struct lxc_conf *conf)
{
	int ret;
	struct lxc_terminal *terminal = &conf->console;

	if (terminal->path && strcmp(terminal->path, "none") == 0) {
		INFO("No terminal requested");
		return 0;
	}

	ret = lxc_terminal_create(terminal);
	if (ret < 0)
		return -1;

#ifdef HAVE_ISULAD
        if (is_syslog(terminal->log_driver)) {
                if (terminal->log_syslog_tag == NULL) {
                        terminal->log_syslog_tag = malloc(16 * sizeof(char));
                        (void)strlcpy(terminal->log_syslog_tag, conf->name, 16);
                }
                if (terminal->log_syslog_facility <= 0) {
                        terminal->log_syslog_facility = LOG_DAEMON;
                }
                openlog(terminal->log_syslog_tag, LOG_PID, terminal->log_syslog_facility);
        }
#endif
	ret = lxc_terminal_create_log_file(terminal);
	if (ret < 0)
		goto err;

	ret = lxc_terminal_create_ringbuf(terminal);
	if (ret < 0)
		goto err;

	return 0;

err:
	lxc_terminal_delete(terminal);
	return -ENODEV;
}

static bool __terminal_dup2(int duplicate, int original)
{
	int ret;

	if (!isatty(original))
		return true;

	ret = dup2(duplicate, original);
	if (ret < 0) {
		SYSERROR("Failed to dup2(%d, %d)", duplicate, original);
		return false;
	}

	return true;
}

int lxc_terminal_set_stdfds(int fd)
{
	int i;

	if (fd < 0)
		return 0;

	for (i = 0; i < 3; i++)
		if (!__terminal_dup2(fd, (int[]){STDIN_FILENO, STDOUT_FILENO,
						 STDERR_FILENO}[i]))
			return -1;

	return 0;
}

int lxc_terminal_stdin_cb(int fd, uint32_t events, void *cbdata,
			  struct lxc_epoll_descr *descr)
{
	int ret;
	char c;
	struct lxc_terminal_state *ts = cbdata;

	if (fd != ts->stdinfd)
		return LXC_MAINLOOP_CLOSE;

	ret = lxc_read_nointr(ts->stdinfd, &c, 1);
	if (ret <= 0)
		return LXC_MAINLOOP_CLOSE;

	if (ts->escape >= 1) {
		/* we want to exit the terminal with Ctrl+a q */
		if (c == ts->escape && !ts->saw_escape) {
			ts->saw_escape = 1;
			return LXC_MAINLOOP_CONTINUE;
		}

		if (c == 'q' && ts->saw_escape)
			return LXC_MAINLOOP_CLOSE;

		ts->saw_escape = 0;
	}

	ret = lxc_write_nointr(ts->masterfd, &c, 1);
	if (ret <= 0)
		return LXC_MAINLOOP_CLOSE;

	return LXC_MAINLOOP_CONTINUE;
}

int lxc_terminal_master_cb(int fd, uint32_t events, void *cbdata,
			   struct lxc_epoll_descr *descr)
{
	int r, w;
	char buf[LXC_TERMINAL_BUFFER_SIZE];
	struct lxc_terminal_state *ts = cbdata;

	if (fd != ts->masterfd)
		return LXC_MAINLOOP_CLOSE;

	r = lxc_read_nointr(fd, buf, sizeof(buf));
	if (r <= 0)
		return LXC_MAINLOOP_CLOSE;

	w = lxc_write_nointr(ts->stdoutfd, buf, r);
	if (w <= 0 || w != r)
		return LXC_MAINLOOP_CLOSE;

	return LXC_MAINLOOP_CONTINUE;
}

int lxc_terminal_getfd(struct lxc_container *c, int *ttynum, int *masterfd)
{
	return lxc_cmd_console(c->name, ttynum, masterfd, c->config_path);
}

int lxc_console(struct lxc_container *c, int ttynum,
		int stdinfd, int stdoutfd, int stderrfd,
		int escape)
{
	int masterfd, ret, ttyfd;
	struct lxc_epoll_descr descr;
	struct termios oldtios;
	struct lxc_terminal_state *ts;
	struct lxc_terminal terminal = {
		.tty_state = NULL,
	};
	int istty = 0;

	ttyfd = lxc_cmd_console(c->name, &ttynum, &masterfd, c->config_path);
	if (ttyfd < 0)
		return -1;

	ret = setsid();
	if (ret < 0)
		TRACE("Process is already group leader");

	ts = lxc_terminal_signal_init(stdinfd, masterfd);
	if (!ts) {
		ret = -1;
		goto close_fds;
	}
	terminal.tty_state = ts;
	ts->escape = escape;
	ts->stdoutfd = stdoutfd;

	istty = isatty(stdinfd);
	if (istty) {
		lxc_terminal_winsz(stdinfd, masterfd);
		lxc_terminal_winsz(ts->stdinfd, ts->masterfd);
	} else {
		INFO("File descriptor %d does not refer to a terminal", stdinfd);
	}

	ret = lxc_mainloop_open(&descr);
	if (ret) {
		ERROR("Failed to create mainloop");
		goto sigwinch_fini;
	}

	if (ts->sigfd != -1) {
		ret = lxc_mainloop_add_handler(&descr, ts->sigfd,
					       lxc_terminal_signalfd_cb, ts);
		if (ret < 0) {
			ERROR("Failed to add signal handler to mainloop");
			goto close_mainloop;
		}
	}

	ret = lxc_mainloop_add_handler(&descr, ts->stdinfd,
				       lxc_terminal_stdin_cb, ts);
	if (ret < 0) {
		ERROR("Failed to add stdin handler");
		goto close_mainloop;
	}

	ret = lxc_mainloop_add_handler(&descr, ts->masterfd,
				       lxc_terminal_master_cb, ts);
	if (ret < 0) {
		ERROR("Failed to add master handler");
		goto close_mainloop;
	}

	if (ts->escape >= 1) {
		fprintf(stderr,
			"\n"
			"Connected to tty %1$d\n"
			"Type <Ctrl+%2$c q> to exit the console, "
			"<Ctrl+%2$c Ctrl+%2$c> to enter Ctrl+%2$c itself\n",
			ttynum, 'a' + escape - 1);
	}

	if (istty) {
		ret = lxc_setup_tios(stdinfd, &oldtios);
		if (ret < 0)
			goto close_mainloop;
	}

	ret = lxc_mainloop(&descr, -1);
	if (ret < 0) {
		ERROR("The mainloop returned an error");
		goto restore_tios;
	}

	ret = 0;

restore_tios:
	if (istty) {
		istty = tcsetattr(stdinfd, TCSAFLUSH, &oldtios);
		if (istty < 0)
			SYSWARN("Failed to restore terminal properties");
	}

close_mainloop:
	lxc_mainloop_close(&descr);

sigwinch_fini:
	lxc_terminal_signal_fini(&terminal);

close_fds:
	close(masterfd);
	close(ttyfd);

	return ret;
}

int lxc_make_controlling_terminal(int fd)
{
	int ret;

	setsid();

	ret = ioctl(fd, TIOCSCTTY, (char *)NULL);
	if (ret < 0)
		return -1;

	return 0;
}

int lxc_terminal_prepare_login(int fd)
{
	int ret;

	ret = lxc_make_controlling_terminal(fd);
	if (ret < 0)
		return -1;

#ifdef HAVE_ISULAD
	ret = set_stdfds(fd);
	if (ret < 0)
		return -1;
#else
	ret = lxc_terminal_set_stdfds(fd);
	if (ret < 0)
		return -1;
#endif

	if (fd > STDERR_FILENO)
		close(fd);

	return 0;
}

void lxc_terminal_info_init(struct lxc_terminal_info *terminal)
{
	terminal->name[0] = '\0';
	terminal->master = -EBADF;
	terminal->slave = -EBADF;
	terminal->busy = -1;
}

void lxc_terminal_init(struct lxc_terminal *terminal)
{
	memset(terminal, 0, sizeof(*terminal));
	terminal->slave = -EBADF;
	terminal->master = -EBADF;
	terminal->peer = -EBADF;
	terminal->log_fd = -EBADF;
	lxc_terminal_info_init(&terminal->proxy);
#ifdef HAVE_ISULAD
	terminal->init_fifo[0] = NULL;
	terminal->init_fifo[1] = NULL;
	terminal->init_fifo[2] = NULL;
	terminal->pipes[0][0] = -1;
	terminal->pipes[0][1] = -1;
	terminal->pipes[1][0] = -1;
	terminal->pipes[1][1] = -1;
	terminal->pipes[2][0] = -1;
	terminal->pipes[2][1] = -1;
	lxc_list_init(&terminal->fifos);
#endif
}

void lxc_terminal_conf_free(struct lxc_terminal *terminal)
{
	free(terminal->log_path);
	free(terminal->path);
	if (terminal->buffer_size > 0 && terminal->ringbuf.addr)
		lxc_ringbuf_release(&terminal->ringbuf);
	lxc_terminal_signal_fini(terminal);
#ifdef HAVE_ISULAD
	/*isulad: free console fifos */
	free(terminal->init_fifo[0]);
	free(terminal->init_fifo[1]);
	free(terminal->init_fifo[2]);
	lxc_terminal_delete_fifo(-1, &terminal->fifos);
	free(terminal->log_driver);
	free(terminal->log_syslog_tag);
#endif
}

int lxc_terminal_map_ids(struct lxc_conf *c, struct lxc_terminal *terminal)
{
	int ret;

	if (lxc_list_empty(&c->id_map))
		return 0;

	if (strcmp(terminal->name, "") == 0)
		return 0;

	ret = chown_mapped_root(terminal->name, c);
	if (ret < 0) {
		ERROR("Failed to chown terminal \"%s\"", terminal->name);
		return -1;
	}

	TRACE("Chowned terminal \"%s\"", terminal->name);

	return 0;
}

