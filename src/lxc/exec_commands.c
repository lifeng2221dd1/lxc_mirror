/******************************************************************************
 * Copyright (c) Huawei Technologies Co., Ltd. 2019. All rights reserved.
 * Author: lifeng
 * Create: 2019-12-08
 * Description: provide container definition
 * lxc: linux Container library
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
  ******************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1
#endif
#include <caps.h>
#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <poll.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "af_unix.h"
#include "cgroup.h"
#include "exec_commands.h"
#include "commands_utils.h"
#include "conf.h"
#include "config.h"
#include "confile.h"
#include "log.h"
#include "lxc.h"
#include "lxclock.h"
#include "mainloop.h"
#include "monitor.h"
#include "terminal.h"
#include "utils.h"

lxc_log_define(commands_exec, lxc);

static const char *lxc_exec_cmd_str(lxc_exec_cmd_t cmd)
{
	static const char *const cmdname[LXC_EXEC_CMD_MAX] = {
		[LXC_EXEC_CMD_SET_TERMINAL_WINCH]  = "set_exec_terminal_winch",
	};

	if (cmd >= LXC_EXEC_CMD_MAX)
		return "Invalid request";

	return cmdname[cmd];
}

static int lxc_exec_cmd_rsp_recv(int sock, struct lxc_exec_cmd_rr *cmd)
{
	int ret, rspfd;
	struct lxc_exec_cmd_rsp *rsp = &cmd->rsp;

	ret = lxc_abstract_unix_recv_fds_timeout(sock, &rspfd, 1, rsp, sizeof(*rsp), 1000 * 1000);
	if (ret < 0) {
		SYSERROR("Failed to receive response for command \"%s\"",
		        lxc_exec_cmd_str(cmd->req.cmd));

		if (errno == ECONNRESET || errno == EAGAIN || errno == EWOULDBLOCK) {
			errno = ECONNRESET; /*isulad set errno ECONNRESET when timeout */
			return -1;
		}

		return -1;
	}
	TRACE("Command \"%s\" received response", lxc_exec_cmd_str(cmd->req.cmd));

	if (rsp->datalen == 0) {
		DEBUG("Response data length for command \"%s\" is 0",
		      lxc_exec_cmd_str(cmd->req.cmd));
		return ret;
	}

	if (rsp->datalen > LXC_CMD_DATA_MAX) {
		ERROR("Response data for command \"%s\" is too long: %d bytes > %d",
		      lxc_exec_cmd_str(cmd->req.cmd), rsp->datalen, LXC_CMD_DATA_MAX);
		return -1;
	}

	rsp->data = malloc(rsp->datalen);
	if (!rsp->data) {
		errno = ENOMEM;
		ERROR("Failed to allocate response buffer for command \"%s\"",
		      lxc_exec_cmd_str(cmd->req.cmd));
		return -1;
	}

	ret = lxc_recv_nointr(sock, rsp->data, rsp->datalen, 0);
	if (ret != rsp->datalen) {
		SYSERROR("Failed to receive response data for command \"%s\"",
		         lxc_exec_cmd_str(cmd->req.cmd));
		return -1;
	}

	return ret;
}

static int lxc_exec_cmd_rsp_send(int fd, struct lxc_exec_cmd_rsp *rsp)
{
	ssize_t ret;

	errno = EMSGSIZE;
	ret = lxc_send_nointr(fd, rsp, sizeof(*rsp), MSG_NOSIGNAL);
	if (ret < 0 || (size_t)ret != sizeof(*rsp)) {
		SYSERROR("Failed to send command response %zd", ret);
		return -1;
	}

	if (!rsp->data || rsp->datalen <= 0)
		return 0;

	errno = EMSGSIZE;
	ret = lxc_send_nointr(fd, rsp->data, rsp->datalen, MSG_NOSIGNAL);
	if (ret < 0 || ret != (ssize_t)rsp->datalen) {
		SYSWARN("Failed to send command response data %zd", ret);
		return -1;
	}

	return 0;
}

static int lxc_exec_cmd_send(const char *name, struct lxc_exec_cmd_rr *cmd,
			const char *lxcpath, const char *hashed_sock_name, const char *suffix)
{
	int client_fd, saved_errno;
	ssize_t ret = -1;

	client_fd = lxc_cmd_connect(name, lxcpath, hashed_sock_name, suffix);
	if (client_fd < 0)
		return -1;

	ret = lxc_abstract_unix_send_credential(client_fd, &cmd->req,
						sizeof(cmd->req));
	if (ret < 0 || (size_t)ret != sizeof(cmd->req))
		goto on_error;

	if (cmd->req.datalen <= 0)
		return client_fd;

	errno = EMSGSIZE;
	ret = lxc_send_nointr(client_fd, (void *)cmd->req.data,
			      cmd->req.datalen, MSG_NOSIGNAL);
	if (ret < 0 || ret != (ssize_t)cmd->req.datalen)
		goto on_error;

	return client_fd;

on_error:
	saved_errno = errno;
	close(client_fd);
	errno = saved_errno;

	return -1;
}

static int lxc_exec_cmd(const char *name, struct lxc_exec_cmd_rr *cmd, const char *lxcpath, const char *hashed_sock_name, const char *suffix)
{
	int client_fd = -1;
	int saved_errno;
	int ret = -1;

	client_fd = lxc_exec_cmd_send(name, cmd, lxcpath, hashed_sock_name, suffix);
	if (client_fd < 0) {
		SYSTRACE("Command \"%s\" failed to connect command socket",
		         lxc_exec_cmd_str(cmd->req.cmd));
		return -1;
	}

	ret = lxc_exec_cmd_rsp_recv(client_fd, cmd);

	saved_errno = errno;
	close(client_fd);
	errno = saved_errno;
	return ret;
}

int lxc_exec_cmd_set_terminal_winch(const char *name, const char *lxcpath, const char *suffix, unsigned int height, unsigned int width)
{
	int ret = 0;
	struct lxc_exec_cmd_set_terminal_winch_request data = { 0 };

	data.height = height;
	data.width = width;

	struct lxc_exec_cmd_rr cmd = {
		.req = {
			.cmd = LXC_EXEC_CMD_SET_TERMINAL_WINCH,
			.datalen = sizeof(struct lxc_exec_cmd_set_terminal_winch_request),
			.data = &data,
		},
	};

	ret = lxc_exec_cmd(name, &cmd, lxcpath, NULL, suffix);
	if (ret < 0) {
		ERROR("Failed to send command to container");
		return -1;
	}

	if (cmd.rsp.ret != 0) {
		ERROR("Command response error:%d", cmd.rsp.ret);
		return -1;
	}
	return 0;
}

static int lxc_exec_cmd_set_terminal_winch_callback(int fd, struct lxc_exec_cmd_req *req,
					struct lxc_exec_command_handler *handler)
{
	struct lxc_exec_cmd_rsp rsp;
	struct lxc_exec_cmd_set_terminal_winch_request *data = (struct lxc_exec_cmd_set_terminal_winch_request *)(req->data);
	memset(&rsp, 0, sizeof(rsp));

	rsp.ret = lxc_set_terminal_winsz(handler->terminal, data->height, data->width);;

	return lxc_exec_cmd_rsp_send(fd, &rsp);

}

static int lxc_exec_cmd_process(int fd, struct lxc_exec_cmd_req *req,
			   struct lxc_exec_command_handler *handler)
{
	typedef int (*callback)(int, struct lxc_exec_cmd_req *, struct lxc_exec_command_handler *);

	callback cb[LXC_EXEC_CMD_MAX] = {
		[LXC_EXEC_CMD_SET_TERMINAL_WINCH]  = lxc_exec_cmd_set_terminal_winch_callback,
	};

	if (req->cmd >= LXC_EXEC_CMD_MAX) {
		ERROR("Undefined command id %d", req->cmd);
		return -1;
	}
	return cb[req->cmd](fd, req, handler);
}

static void lxc_exec_cmd_fd_cleanup(int fd, struct lxc_epoll_descr *descr)
{
	lxc_mainloop_del_handler(descr, fd);
	close(fd);
	return;
}

static int lxc_exec_cmd_handler(int fd, uint32_t events, void *data,
			   struct lxc_epoll_descr *descr)
{
	int ret;
	struct lxc_exec_cmd_req req;
	void *reqdata = NULL;
	struct lxc_exec_command_handler *handler = data;

	ret = lxc_abstract_unix_rcv_credential(fd, &req, sizeof(req));
	if (ret < 0) {
		SYSERROR("Failed to receive data on command socket for command "
		         "\"%s\"", lxc_exec_cmd_str(req.cmd));

		if (errno == EACCES) {
			/* We don't care for the peer, just send and close. */
			struct lxc_exec_cmd_rsp rsp = {.ret = ret};

			lxc_exec_cmd_rsp_send(fd, &rsp);
		}

		goto out_close;
	}

	if (ret == 0)
		goto out_close;

	if (ret != sizeof(req)) {
		WARN("Failed to receive full command request. Ignoring request "
		     "for \"%s\"", lxc_exec_cmd_str(req.cmd));
		ret = -1;
		goto out_close;
	}

	if (req.datalen > LXC_CMD_DATA_MAX) {
		ERROR("Received command data length %d is too large for "
		      "command \"%s\"", req.datalen, lxc_exec_cmd_str(req.cmd));
		errno = EFBIG;
		ret = -EFBIG;
		goto out_close;
	}

	if (req.datalen > 0) {
		reqdata = alloca(req.datalen);
		if (!reqdata) {
			ERROR("Failed to allocate memory for \"%s\" command",
			      lxc_exec_cmd_str(req.cmd));
			errno = ENOMEM;
			ret = -ENOMEM;
			goto out_close;
		}

		ret = lxc_recv_nointr(fd, reqdata, req.datalen, 0);
		if (ret != req.datalen) {
			WARN("Failed to receive full command request. Ignoring "
			     "request for \"%s\"", lxc_exec_cmd_str(req.cmd));
			ret = LXC_MAINLOOP_ERROR;
			goto out_close;
		}

		req.data = reqdata;
	}

	ret = lxc_exec_cmd_process(fd, &req, handler);
	if (ret) {
		/* This is not an error, but only a request to close fd. */
		ret = LXC_MAINLOOP_CONTINUE;
		goto out_close;
	}

out:
	return ret;

out_close:
	lxc_exec_cmd_fd_cleanup(fd, descr);
	goto out;
}

static int lxc_exec_cmd_accept(int fd, uint32_t events, void *data,
			  struct lxc_epoll_descr *descr)
{
	int connection = -1;
	int opt = 1, ret = -1;

	connection = accept(fd, NULL, 0);
	if (connection < 0) {
		SYSERROR("Failed to accept connection to run command");
		return LXC_MAINLOOP_ERROR;
	}

	ret = fcntl(connection, F_SETFD, FD_CLOEXEC);
	if (ret < 0) {
		SYSERROR("Failed to set close-on-exec on incoming command connection");
		goto out_close;
	}

	ret = setsockopt(connection, SOL_SOCKET, SO_PASSCRED, &opt, sizeof(opt));
	if (ret < 0) {
		SYSERROR("Failed to enable necessary credentials on command socket");
		goto out_close;
	}

	ret = lxc_mainloop_add_handler(descr, connection, lxc_exec_cmd_handler, data);
	if (ret) {
		ERROR("Failed to add command handler");
		goto out_close;
	}

out:
	return ret;

out_close:
	close(connection);
	goto out;
}
#ifdef HAVE_ISULAD
int lxc_exec_unix_sock_delete(const char *name, const char *suffix)
{
	char path[LXC_AUDS_ADDR_LEN] = {0};

	if (name == NULL || suffix == NULL)
		return -1;

	if (generate_named_unix_sock_path(name, suffix, path, sizeof(path)) != 0)
		return -1;

	(void)unlink(path);

	return 0;
}

int lxc_exec_cmd_init(const char *name, const char *lxcpath, const char *suffix)
{
	__do_close int fd = -EBADF;
	int ret;
	char path[LXC_AUDS_ADDR_LEN] = {0};
	__do_free char *exec_sock_dir = NULL;

	exec_sock_dir = generate_named_unix_sock_dir(name);
	if (exec_sock_dir == NULL)
		return -1;

	if (mkdir_p(exec_sock_dir, 0600) < 0)
		return log_error_errno(-1, errno, "Failed to create exec sock directory %s", path);

	if (generate_named_unix_sock_path(name, suffix, path, sizeof(path)) != 0)
		return -1;

	TRACE("Creating unix socket \"%s\"", path);

	fd = lxc_named_unix_open(path, SOCK_STREAM, 0);
	if (fd < 0) {
		if (errno == EADDRINUSE) {
			WARN("Container \"%s\" exec unix sock is occupied", name);
			(void)unlink(path);
			fd = lxc_named_unix_open(path, SOCK_STREAM, 0);
			if (fd < 0)
				return log_error_errno(-1, errno, "Failed to create command socket %s", path);
		} else {
			return log_error_errno(-1, errno, "Failed to create command socket %s", path);
		}
	}

	ret = fcntl(fd, F_SETFD, FD_CLOEXEC);
	if (ret < 0)
		return log_error_errno(-1, errno, "Failed to set FD_CLOEXEC on command socket file descriptor");

	return log_trace(move_fd(fd), "Created unix socket \"%s\"", path);
}
#else
int lxc_exec_cmd_init(const char *name, const char *lxcpath, const char *suffix)
{
	int fd, ret;
	char path[LXC_AUDS_ADDR_LEN] = {0};

	ret = lxc_make_abstract_socket_name(path, sizeof(path), name, lxcpath, NULL, suffix);
	if (ret < 0)
		return -1;
	TRACE("Creating abstract unix socket \"%s\"", &path[1]);

	fd = lxc_abstract_unix_open(path, SOCK_STREAM, 0);
	if (fd < 0) {
		SYSERROR("Failed to create command socket %s", &path[1]);
		if (errno == EADDRINUSE)
			ERROR("Container \"%s\" appears to be already running", name);

		return -1;
	}

	ret = fcntl(fd, F_SETFD, FD_CLOEXEC);
	if (ret < 0) {
		SYSERROR("Failed to set FD_CLOEXEC on command socket file descriptor");
		close(fd);
		return -1;
	}

	return fd;
}
#endif

int lxc_exec_cmd_mainloop_add(struct lxc_epoll_descr *descr, struct lxc_exec_command_handler *handler)
{
	int ret;
	int fd = handler->maincmd_fd;

	ret = lxc_mainloop_add_handler(descr, fd, lxc_exec_cmd_accept, handler);
	if (ret < 0) {
		ERROR("Failed to add handler for command socket");
		close(fd);
	}

	return ret;
}
