/* SPDX-License-Identifier: LGPL-2.1+ */
/******************************************************************************
 * Copyright (c) Huawei Technologies Co., Ltd. 2020. Allrights reserved
 * Description: isulad utils
 * Author: lifeng
 * Create: 2020-04-11
******************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "isulad_utils.h"
#include "log.h"
#include "path.h"
#include "file_utils.h"

lxc_log_define(isulad_utils, lxc);

void *lxc_common_calloc_s(size_t size)
{
	if (size == 0 || size > SIZE_MAX) {
		return NULL;
	}

	return calloc((size_t)1, size);
}

int lxc_mem_realloc(void **newptr, size_t newsize, void *oldptr, size_t oldsize)
{
	void *tmp = NULL;

	if (newsize == 0) {
		goto err_out;
	}

	tmp = lxc_common_calloc_s(newsize);
	if (tmp == NULL) {
		ERROR("Failed to malloc memory");
		goto err_out;
	}

	if (oldptr != NULL) {
		memcpy(tmp, oldptr, (newsize < oldsize) ? newsize : oldsize);

		memset(oldptr, 0, oldsize);

		free(oldptr);
	}

	*newptr = tmp;
	return 0;

err_out:
	return -1;
}

char *safe_strdup(const char *src)
{
	char *dst = NULL;

	if (src == NULL) {
		return NULL;
	}

	dst = strdup(src);
	if (dst == NULL) {
		abort();
	}

	return dst;
}

int lxc_open(const char *filename, int flags, mode_t mode)
{
	char rpath[PATH_MAX] = {0x00};

	if (cleanpath(filename, rpath, sizeof(rpath)) == NULL) {
		return -1;
	}
	if (mode) {
		return open(rpath, (int)((unsigned int)flags | O_CLOEXEC), mode);
	} else {
		return open(rpath, (int)((unsigned int)flags | O_CLOEXEC));
	}
}

FILE *lxc_fopen(const char *filename, const char *mode)
{
	char rpath[PATH_MAX] = {0x00};

	if (cleanpath(filename, rpath, sizeof(rpath)) == NULL) {
		return NULL;
	}

	return fopen_cloexec(rpath, mode);
}
