/* SPDX-License-Identifier: LGPL-2.1+ */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1
#endif
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <termios.h>
#include <unistd.h>

#include <lxc/lxccontainer.h>

#include "arguments.h"
#include "attach.h"
#include "caps.h"
#include "config.h"
#include "confile.h"
#include "log.h"
#include "rexec.h"
#include "utils.h"

lxc_log_define(lxc_attach, lxc);

/**
 * This function will copy any binary that calls liblxc into a memory file and
 * will use the memfd to rexecute the binary. This is done to prevent attacks
 * through the /proc/self/exe symlink to corrupt the host binary when host and
 * container are in the same user namespace or have set up an identity id
 * mapping: CVE-2019-5736.
 */
#ifdef ENFORCE_MEMFD_REXEC
__attribute__((constructor)) static void lxc_attach_rexec(void)
{
	if (!getenv("LXC_MEMFD_REXEC") && lxc_rexec("lxc-attach")) {
		fprintf(stderr, "Failed to re-execute lxc-attach via memory file descriptor\n");
		_exit(EXIT_FAILURE);
	}
}
#endif

static int my_parser(struct lxc_arguments *args, int c, char *arg);
static int add_to_simple_array(char ***array, ssize_t *capacity, char *value);
static bool stdfd_is_pty(void);
static int lxc_attach_create_log_file(const char *log_file);

static int elevated_privileges;
static signed long new_personality = -1;
static int namespace_flags = -1;
static int remount_sys_proc;
static lxc_attach_env_policy_t env_policy = LXC_ATTACH_KEEP_ENV;
static char **extra_env;
static ssize_t extra_env_size;
static char **extra_keep;
static ssize_t extra_keep_size;

static const struct option my_longopts[] = {
	{"elevated-privileges", optional_argument, 0, 'e'},
	{"arch", required_argument, 0, 'a'},
	{"namespaces", required_argument, 0, 's'},
	{"remount-sys-proc", no_argument, 0, 'R'},
	/* TODO: decide upon short option names */
	{"clear-env", no_argument, 0, 500},
	{"keep-env", no_argument, 0, 501},
	{"keep-var", required_argument, 0, 502},
	{"set-var", required_argument, 0, 'v'},
	{"pty-log", required_argument, 0, 'L'},
	{"rcfile", required_argument, 0, 'f'},
#ifndef HAVE_ISULAD
	{"uid", required_argument, 0, 'u'},
	{"gid", required_argument, 0, 'g'},
#else
	{"workdir", required_argument, 0, 'w'},
	{"user", required_argument, 0, 'u'},
	{"in-fifo", required_argument, 0, OPT_INPUT_FIFO}, /* isulad add terminal fifos*/
	{"out-fifo", required_argument, 0, OPT_OUTPUT_FIFO},
	{"err-fifo", required_argument, 0, OPT_STDERR_FIFO},
	{"suffix", required_argument, 0, OPT_ATTACH_SUFFIX},
	{"timeout", required_argument, 0, OPT_ATTACH_TIMEOUT},
	{"disable-pty", no_argument, 0, OPT_DISABLE_PTY},
	{"open-stdin", no_argument, 0, OPT_OPEN_STDIN},
#endif
	LXC_COMMON_OPTIONS
};

static struct lxc_arguments my_args = {
	.progname     = "lxc-attach",
	.help         = "\
--name=NAME [-- COMMAND]\n\
\n\
Execute the specified COMMAND - enter the container NAME\n\
\n\
Options :\n\
  -n, --name=NAME   NAME of the container\n\
  -e, --elevated-privileges=PRIVILEGES\n\
                    Use elevated privileges instead of those of the\n\
                    container. If you don't specify privileges to be\n\
                    elevated as OR'd list: CAP, CGROUP and LSM (capabilities,\n\
                    cgroup and restrictions, respectively) then all of them\n\
                    will be elevated.\n\
                    WARNING: This may leak privileges into the container.\n\
                    Use with care.\n\
  -a, --arch=ARCH   Use ARCH for program instead of container's own\n\
                    architecture.\n\
  -s, --namespaces=FLAGS\n\
                    Don't attach to all the namespaces of the container\n\
                    but just to the following OR'd list of flags:\n\
                    MOUNT, PID, UTSNAME, IPC, USER or NETWORK.\n\
                    WARNING: Using -s implies -e with all privileges\n\
                    elevated, it may therefore leak privileges into the\n\
                    container. Use with care.\n\
  -R, --remount-sys-proc\n\
                    Remount /sys and /proc if not attaching to the\n\
                    mount namespace when using -s in order to properly\n\
                    reflect the correct namespace context. See the\n\
                    lxc-attach(1) manual page for details.\n\
      --clear-env   Clear all environment variables before attaching.\n\
                    The attached shell/program will start with only\n\
                    container=lxc set.\n\
      --keep-env    Keep all current environment variables. This\n\
                    is the current default behaviour, but is likely to\n\
                    change in the future.\n\
  -L, --pty-log=FILE\n\
                    Log pty output to FILE\n\
  -v, --set-var     Set an additional variable that is seen by the\n\
                    attached program in the container. May be specified\n\
                    multiple times.\n\
      --keep-var    Keep an additional environment variable. Only\n\
                    applicable if --clear-env is specified. May be used\n\
                    multiple times.\n\
  -f, --rcfile=FILE\n\
                    Load configuration file FILE\n\
"
#ifndef HAVE_ISULAD
"\
  -u, --uid=UID     Execute COMMAND with UID inside the container\n\
  -g, --gid=GID     Execute COMMAND with GID inside the container\n\
"
#else
"\
  --user            User ID (format: UID[:GID])\n\
  -w, --workdir     Working directory inside the container.\n\
  --timeout         Timeout in seconds (default: 0)\n\
"
#endif
,
	.options      = my_longopts,
	.parser       = my_parser,
	.checker      = NULL,
	.log_priority = "ERROR",
	.log_file     = "none",
	.uid          = LXC_INVALID_UID,
	.gid          = LXC_INVALID_GID,
};

#ifdef HAVE_ISULAD
static int parse_user_id(const char *username, char **uid, char **gid, char **tmp_dup)
{
	char *tmp = NULL;
	char *pdot = NULL;

	if (uid == NULL || gid == NULL || tmp_dup == NULL) {
		return -1;
	}

	if (username != NULL) {
		tmp = strdup(username);
		if (tmp == NULL) {
			ERROR("Failed to duplicate user name");
			return -1;
		}

		// for free tmp in caller
		*tmp_dup = tmp;
		pdot = strstr(tmp, ":");
		if (pdot != NULL) {
			*pdot = '\0';
			if (pdot != tmp) {
				// uid found
				*uid = tmp;
			}

			if (*(pdot + 1) != '\0') {
				// gid found
				*gid = pdot + 1;
			}
		} else {
			// No : found
			if (*tmp != '\0') {
				*uid = tmp;
			}
		}
	}

	return 0;
}

static int get_attach_uid_gid(const char *username, uid_t *user_id, gid_t *group_id)
{
	char *tmp = NULL;
	char *uid = NULL;
	char *gid = NULL;

	// parse uid and gid by username
	if (parse_user_id(username, &uid, &gid, &tmp) != 0) {
		return -1;
	}

	if (uid != NULL) {
		*user_id = (unsigned int)atoll(uid);
	}
	if (gid != NULL) {
		*group_id = (unsigned int)atoll(gid);
	}

	free(tmp);
	return 0;
}
#endif

static int my_parser(struct lxc_arguments *args, int c, char *arg)
{
	int ret;

	switch (c) {
	case 'e':
		ret = lxc_fill_elevated_privileges(arg, &elevated_privileges);
		if (ret)
			return -1;
		break;
	case 'R': remount_sys_proc = 1; break;
	case 'a':
		new_personality = lxc_config_parse_arch(arg);
		if (new_personality < 0) {
			ERROR("Invalid architecture specified: %s", arg);
			return -1;
		}
		break;
	case 's':
		namespace_flags = 0;

		if (lxc_namespace_2_std_identifiers(arg) < 0)
			return -1;

		ret = lxc_fill_namespace_flags(arg, &namespace_flags);
		if (ret)
			return -1;

		/* -s implies -e */
		lxc_fill_elevated_privileges(NULL, &elevated_privileges);
		break;
	case 500: /* clear-env */
		env_policy = LXC_ATTACH_CLEAR_ENV;
		break;
	case 501: /* keep-env */
		env_policy = LXC_ATTACH_KEEP_ENV;
		break;
	case 502: /* keep-var */
		ret = add_to_simple_array(&extra_keep, &extra_keep_size, arg);
		if (ret < 0) {
			ERROR("Failed to alloc memory");
			return -1;
		}
		break;
	case 'v':
		ret = add_to_simple_array(&extra_env, &extra_env_size, arg);
		if (ret < 0) {
			ERROR("Failed to alloc memory");
			return -1;
		}
		break;
	case 'L':
		args->console_log = arg;
		break;
	case 'f':
		args->rcfile = arg;
		break;
#ifndef HAVE_ISULAD
	case 'u':
		if (lxc_safe_uint(arg, &args->uid) < 0)
			return -1;
		break;
	case 'g':
		if (lxc_safe_uint(arg, &args->gid) < 0)
			return -1;
		break;
#else
	case 'u':
		if (get_attach_uid_gid(arg, &args->uid, &args->gid) != 0) {
			ERROR("Failed to get attach user U/GID");
			return -1;
		}
		break;
	case 'w':
		args->workdir=arg;
		break;
	case OPT_INPUT_FIFO:
		args->terminal_fifos[0] = arg;
		break;
	case OPT_OUTPUT_FIFO:
		args->terminal_fifos[1] = arg;
		break;
	case OPT_STDERR_FIFO:
		args->terminal_fifos[2] = arg;
		break;
	case OPT_ATTACH_SUFFIX:
		args->suffix = arg;
		break;
	case OPT_ATTACH_TIMEOUT:
		if(!is_non_negative_num(arg)) {
			ERROR("Error attach timeout parameter:%s.\n", arg);
			return -1;
		}
		args->attach_timeout = (unsigned int)atoll(arg);
		break;
	case OPT_DISABLE_PTY:
		args->disable_pty = 1;
		break;
	case OPT_OPEN_STDIN:
		args->open_stdin = 1;
		break;
#endif
	}
	return 0;
}

static int add_to_simple_array(char ***array, ssize_t *capacity, char *value)
{
	ssize_t count = 0;

	if (!array)
		return -1;

	if (*array)
		for (; (*array)[count]; count++);

	/* we have to reallocate */
	if (count >= *capacity - 1) {
		ssize_t new_capacity = ((count + 1) / 32 + 1) * 32;

		char **new_array = realloc((void*)*array, sizeof(char *) * new_capacity);
		if (!new_array)
			return -1;

		memset(&new_array[count], 0, sizeof(char*)*(new_capacity - count));

		*array = new_array;
		*capacity = new_capacity;
	}

	if (!(*array))
		return -1;

	(*array)[count] = value;
	return 0;
}

static bool stdfd_is_pty(void)
{
	if (isatty(STDIN_FILENO))
		return true;

	if (isatty(STDOUT_FILENO))
		return true;

	if (isatty(STDERR_FILENO))
		return true;

	return false;
}

static int lxc_attach_create_log_file(const char *log_file)
{
	int fd;

	fd = open(log_file, O_CLOEXEC | O_RDWR | O_CREAT | O_APPEND, 0600);
	if (fd < 0) {
		ERROR("Failed to open log file \"%s\"", log_file);
		return -1;
	}

	return fd;
}

#ifdef HAVE_ISULAD
// isulad: send '128 + signal' if container is killed by signal.
#define EXIT_SIGNAL_OFFSET 128

/*isulad: attach with terminal*/
static int do_attach_foreground(struct lxc_container *c, lxc_attach_command_t *command,
				lxc_attach_options_t *attach_options,
				char **errmsg)
{
	int ret = 0;
	pid_t pid;
	int wexit = -1;
	int signal;

	if (command->program)
		ret = c->attach(c, lxc_attach_run_command, command, attach_options, &pid);
	else
		ret = c->attach(c, lxc_attach_run_shell, NULL, attach_options, &pid);
	if (ret < 0) {
		*errmsg = safe_strdup("Internal error, failed to call attach");
		goto out;
	}

	ret = lxc_wait_for_pid_status(pid);
	if (ret < 0) {
		free(*errmsg);
		*errmsg = safe_strdup("Internal error, failed to wait attached process");
		goto out;
	}

	if (WIFEXITED(ret))
		wexit = WEXITSTATUS(ret);
	else
		wexit = -1;

	if (WIFSIGNALED(ret)) {
		signal = WTERMSIG(ret);
		wexit = EXIT_SIGNAL_OFFSET + signal;
	}

	WARN("Execd pid %d exit with %d", pid, wexit);

out:
	if (c->lxc_conf->errmsg) {
		free(*errmsg);
		*errmsg = safe_strdup(c->lxc_conf->errmsg);
	}
	return wexit;
}

static void close_msg_pipe(int *errpipe)
{
	if (errpipe[0] >= 0) {
		close(errpipe[0]);
		errpipe[0] = -1;
	}
	if (errpipe[1] >= 0) {
		close(errpipe[1]);
		errpipe[1] = -1;
	}
}

/*isulad: attach without terminal in background */
static int do_attach_background(struct lxc_container *c, lxc_attach_command_t *command,
				lxc_attach_options_t *attach_options,
				char **errmsg)
{
	int ret = 0;
	int msgpipe[2];
	pid_t pid = 0;
	ssize_t size_read;
	char msgbuf[BUFSIZ + 1] = {0};

	//pipdfd for get error message of child or grandchild process.
	if (pipe2(msgpipe, O_CLOEXEC) != 0) {
		SYSERROR("Failed to init msgpipe");
		return -1;
	}

	pid = fork();
	if (pid < 0) {
		close_msg_pipe(msgpipe);
		return -1;
	}

	if (pid != 0) {
		close(msgpipe[1]);
		msgpipe[1] = -1;
		size_read = read(msgpipe[0], msgbuf, BUFSIZ);
		if (size_read > 0) {
			*errmsg = safe_strdup(msgbuf);
			ret = -1;
		}

		close(msgpipe[0]);
		msgpipe[0] = -1;

		return ret;
	}

	/* second fork to be reparented by init */
	pid = fork();
	if (pid < 0) {
		SYSERROR("Error doing dual-fork");
		close_msg_pipe(msgpipe);
		exit(1);
	}
	if (pid != 0) {
		close_msg_pipe(msgpipe);
		exit(0);
	}

	close(msgpipe[0]);
	msgpipe[0] = -1;

	if (null_stdfds() < 0) {
		ERROR("failed to close fds");
		exit(1);
	}
	setsid();

	if (command->program)
		ret = c->attach(c, lxc_attach_run_command, command, attach_options, &pid);
	else
		ret = c->attach(c, lxc_attach_run_shell, NULL, attach_options, &pid);
	if (ret < 0) {
		if (c->lxc_conf->errmsg)
			lxc_write_error_message(msgpipe[1], "%s", c->lxc_conf->errmsg);
		else
			lxc_write_error_message(msgpipe[1], "Failed to attach container");
		close(msgpipe[1]);
		msgpipe[1] = -1;
		ret = -1;
		goto out;
	}

	close(msgpipe[1]);
	msgpipe[1] = -1;

	ret = wait_for_pid(pid);
out:
	lxc_container_put(c);
	if (ret)
		exit(EXIT_FAILURE);
	else
		exit(0);
}

int main(int argc, char *argv[])
{
	int wexit = 0;
	struct lxc_log log;
	char *errmsg = NULL;
	lxc_attach_options_t attach_options = LXC_ATTACH_OPTIONS_DEFAULT;
	lxc_attach_command_t command = (lxc_attach_command_t){.program = NULL};

	if (lxc_caps_init())
		exit(EXIT_FAILURE);

	if (lxc_arguments_parse(&my_args, argc, argv))
		exit(EXIT_FAILURE);

	log.name = my_args.name;
	log.file = my_args.log_file;
	log.level = my_args.log_priority;
	log.prefix = my_args.progname;
	log.quiet = my_args.quiet;
	log.lxcpath = my_args.lxcpath[0];

	if (lxc_log_init(&log))
		exit(EXIT_FAILURE);

	if (geteuid())
		if (access(my_args.lxcpath[0], O_RDONLY) < 0) {
			ERROR("You lack access to %s", my_args.lxcpath[0]);
			exit(EXIT_FAILURE);
		}

	struct lxc_container *c = lxc_container_new(my_args.name, my_args.lxcpath[0]);
	if (!c)
		exit(EXIT_FAILURE);

	if (my_args.rcfile) {
		c->clear_config(c);
		if (!c->load_config(c, my_args.rcfile)) {
			ERROR("Failed to load rcfile");
			lxc_container_put(c);
			exit(EXIT_FAILURE);
		}

		c->configfile = strdup(my_args.rcfile);
		if (!c->configfile) {
			ERROR("Out of memory setting new config filename");
			lxc_container_put(c);
			exit(EXIT_FAILURE);
		}
	}

	if (!c->may_control(c)) {
		ERROR("Insufficent privileges to control %s", c->name);
		lxc_container_put(c);
		exit(EXIT_FAILURE);
	}

	if (remount_sys_proc)
		attach_options.attach_flags |= LXC_ATTACH_REMOUNT_PROC_SYS;

	if (elevated_privileges)
		attach_options.attach_flags &= ~(elevated_privileges);

	if (my_args.terminal_fifos[0] || my_args.terminal_fifos[1] || my_args.terminal_fifos[2]) {
		attach_options.init_fifo[0] = my_args.terminal_fifos[0];
		attach_options.init_fifo[1] = my_args.terminal_fifos[1];
		attach_options.init_fifo[2] = my_args.terminal_fifos[2];
		attach_options.attach_flags |= LXC_ATTACH_TERMINAL;
	} else if (stdfd_is_pty()) {
		attach_options.attach_flags |= LXC_ATTACH_TERMINAL;
	}

	attach_options.namespaces = namespace_flags;
	attach_options.personality = new_personality;
	attach_options.env_policy = env_policy;
	attach_options.extra_env_vars = extra_env;
	attach_options.extra_keep_env = extra_keep;
	attach_options.timeout = my_args.attach_timeout;

	if (my_args.argc > 0) {
		command.program = my_args.argv[0];
		command.argv = (char**)my_args.argv;
	}

	if (my_args.console_log) {
		attach_options.log_fd = lxc_attach_create_log_file(my_args.console_log);
		if (attach_options.log_fd < 0) {
			ERROR("Failed to create log file for %s", c->name);
			lxc_container_put(c);
			exit(EXIT_FAILURE);
		}
	}

	if (my_args.uid != LXC_INVALID_UID)
		attach_options.uid = my_args.uid;

	if (my_args.gid != LXC_INVALID_GID)
		attach_options.gid = my_args.gid;

	attach_options.suffix = my_args.suffix;

	if (my_args.disable_pty) {
		attach_options.disable_pty = true;
	}

	if (my_args.open_stdin) {
		attach_options.open_stdin = true;
	}

#ifdef HAVE_ISULAD
	if (my_args.workdir) {
		attach_options.initial_cwd = my_args.workdir;
	}
#endif

	/* isulad: add do attach background */
	if (attach_options.attach_flags & LXC_ATTACH_TERMINAL)
		wexit = do_attach_foreground(c, &command, &attach_options, &errmsg);
	else
		wexit = do_attach_background(c, &command, &attach_options, &errmsg);

	if (errmsg) {
		fprintf(stderr, "%s:%s:%s:%d starting container process caused \"%s\"", c->name,
		__FILE__, __func__, __LINE__, errmsg);
		free(errmsg);
	}

	lxc_container_put(c);
	if (wexit >= 0)
		exit(wexit);

	exit(EXIT_FAILURE);
}
#else
int main(int argc, char *argv[])
{
	int ret = -1;
	int wexit = 0;
	struct lxc_log log;
	pid_t pid;
	lxc_attach_options_t attach_options = LXC_ATTACH_OPTIONS_DEFAULT;
	lxc_attach_command_t command = (lxc_attach_command_t){.program = NULL};

	if (lxc_caps_init())
		exit(EXIT_FAILURE);

	if (lxc_arguments_parse(&my_args, argc, argv))
		exit(EXIT_FAILURE);

	log.name = my_args.name;
	log.file = my_args.log_file;
	log.level = my_args.log_priority;
	log.prefix = my_args.progname;
	log.quiet = my_args.quiet;
	log.lxcpath = my_args.lxcpath[0];

	if (lxc_log_init(&log))
		exit(EXIT_FAILURE);

	if (geteuid())
		if (access(my_args.lxcpath[0], O_RDONLY) < 0) {
			ERROR("You lack access to %s", my_args.lxcpath[0]);
			exit(EXIT_FAILURE);
		}

	struct lxc_container *c = lxc_container_new(my_args.name, my_args.lxcpath[0]);
	if (!c)
		exit(EXIT_FAILURE);

	if (my_args.rcfile) {
		c->clear_config(c);
		if (!c->load_config(c, my_args.rcfile)) {
			ERROR("Failed to load rcfile");
			lxc_container_put(c);
			exit(EXIT_FAILURE);
		}

		c->configfile = strdup(my_args.rcfile);
		if (!c->configfile) {
			ERROR("Out of memory setting new config filename");
			lxc_container_put(c);
			exit(EXIT_FAILURE);
		}
	}

	if (!c->may_control(c)) {
		ERROR("Insufficent privileges to control %s", c->name);
		lxc_container_put(c);
		exit(EXIT_FAILURE);
	}

	if (remount_sys_proc)
		attach_options.attach_flags |= LXC_ATTACH_REMOUNT_PROC_SYS;

	if (elevated_privileges)
		attach_options.attach_flags &= ~(elevated_privileges);

	if (stdfd_is_pty())
		attach_options.attach_flags |= LXC_ATTACH_TERMINAL;

	attach_options.namespaces = namespace_flags;
	attach_options.personality = new_personality;
	attach_options.env_policy = env_policy;
	attach_options.extra_env_vars = extra_env;
	attach_options.extra_keep_env = extra_keep;

	if (my_args.argc > 0) {
		command.program = my_args.argv[0];
		command.argv = (char**)my_args.argv;
	}

	if (my_args.console_log) {
		attach_options.log_fd = lxc_attach_create_log_file(my_args.console_log);
		if (attach_options.log_fd < 0)
			goto out;
	}

	if (my_args.uid != LXC_INVALID_UID)
		attach_options.uid = my_args.uid;

	if (my_args.gid != LXC_INVALID_GID)
		attach_options.gid = my_args.gid;

	if (command.program) {
		ret = c->attach_run_wait(c, &attach_options, command.program,
					 (const char **)command.argv);
		if (ret < 0)
			goto out;
	} else {
		ret = c->attach(c, lxc_attach_run_shell, NULL, &attach_options, &pid);
		if (ret < 0)
			goto out;

		ret = lxc_wait_for_pid_status(pid);
		if (ret < 0)
			goto out;
	}
	if (WIFEXITED(ret))
		wexit = WEXITSTATUS(ret);

out:
	lxc_container_put(c);
	if (ret >= 0)
		exit(wexit);

	exit(EXIT_FAILURE);
}
#endif
