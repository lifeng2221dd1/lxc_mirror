/* SPDX-License-Identifier: LGPL-2.1+ */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1
#endif
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <libgen.h>
#include <net/if.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/utsname.h>
#include <unistd.h>

#include <lxc/lxccontainer.h>

#include "arguments.h"
#include "caps.h"
#include "config.h"
#include "confile.h"
#include "log.h"

#ifdef HAVE_ISULAD
#include <ctype.h>
#include "isulad_utils.h"
#endif

lxc_log_define(lxc_start, lxc);

static int my_parser(struct lxc_arguments *args, int c, char *arg);
static int ensure_path(char **confpath, const char *path);

static struct lxc_list defines;

static const struct option my_longopts[] = {
	{"daemon", no_argument, 0, 'd'},
	{"foreground", no_argument, 0, 'F'},
	{"rcfile", required_argument, 0, 'f'},
	{"define", required_argument, 0, 's'},
	{"console", required_argument, 0, 'c'},
	{"console-log", required_argument, 0, 'L'},
	{"close-all-fds", no_argument, 0, 'C'},
	{"pidfile", required_argument, 0, 'p'},
	{"share-net", required_argument, 0, OPT_SHARE_NET},
	{"share-ipc", required_argument, 0, OPT_SHARE_IPC},
	{"share-uts", required_argument, 0, OPT_SHARE_UTS},
	{"share-pid", required_argument, 0, OPT_SHARE_PID},
#ifdef HAVE_ISULAD
	{"in-fifo", required_argument, 0, OPT_INPUT_FIFO},
	{"out-fifo", required_argument, 0, OPT_OUTPUT_FIFO},
	{"err-fifo", required_argument, 0, OPT_STDERR_FIFO},
	{"container-pidfile", required_argument, 0, OPT_CONTAINER_INFO},
	{"exit-fifo", required_argument, 0, OPT_EXIT_FIFO},
	{"start-timeout", required_argument, 0, OPT_START_TIMEOUT},
	{"disable-pty", no_argument, 0, OPT_DISABLE_PTY},
	{"open-stdin", no_argument, 0, OPT_OPEN_STDIN},
	{"start-timeout", required_argument, 0, OPT_START_TIMEOUT},
#endif
	LXC_COMMON_OPTIONS
};

static struct lxc_arguments my_args = {
	.progname     = "lxc-start",
	.help         = "\
--name=NAME -- COMMAND\n\
\n\
lxc-start start COMMAND in specified container NAME\n\
\n\
Options :\n\
  -n, --name=NAME        NAME of the container\n\
  -d, --daemon           Daemonize the container (default)\n\
  -F, --foreground       Start with the current tty attached to /dev/console\n\
  -p, --pidfile=FILE     Create a file with the process id\n\
  -f, --rcfile=FILE      Load configuration file FILE\n\
  -c, --console=FILE     Use specified FILE for the container console\n\
  -L, --console-log=FILE Log container console output to FILE\n\
  -C, --close-all-fds    If any fds are inherited, close them\n\
                         Note: --daemon implies --close-all-fds\n\
  -s, --define KEY=VAL   Assign VAL to configuration variable KEY\n\
      --share-[net|ipc|uts|pid]=NAME Share a namespace with another container or pid\n\
",
	.options      = my_longopts,
	.parser       = my_parser,
	.checker      = NULL,
	.log_priority = "ERROR",
	.log_file     = "none",
	.daemonize    = 1,
	.pidfile      = NULL,
};

static int my_parser(struct lxc_arguments *args, int c, char *arg)
{
	switch (c) {
	case 'c':
		args->console = arg;
		break;
	case 'L':
		args->console_log = arg;
		break;
	case 'd':
		args->daemonize = 1;
		break;
	case 'F':
		args->daemonize = 0;
		break;
	case 'f':
		args->rcfile = arg;
		break;
	case 'C':
		args->close_all_fds = true;
		break;
	case 's':
		return lxc_config_define_add(&defines, arg);
	case 'p':
		args->pidfile = arg;
		break;
	case OPT_SHARE_NET:
		args->share_ns[LXC_NS_NET] = arg;
		break;
	case OPT_SHARE_IPC:
		args->share_ns[LXC_NS_IPC] = arg;
		break;
	case OPT_SHARE_UTS:
		args->share_ns[LXC_NS_UTS] = arg;
		break;
	case OPT_SHARE_PID:
		args->share_ns[LXC_NS_PID] = arg;
		break;

#ifdef HAVE_ISULAD
	case OPT_CONTAINER_INFO:
		args->container_info = arg;
		break;
	case OPT_INPUT_FIFO:
		args->terminal_fifos[0] = arg;
		break;
	case OPT_OUTPUT_FIFO:
		args->terminal_fifos[1] = arg;
		break;
	case OPT_STDERR_FIFO:
		args->terminal_fifos[2] = arg;
		break;
	case OPT_EXIT_FIFO:
		args->exit_monitor_fifo = arg;
		break;
	case OPT_DISABLE_PTY:
		args->disable_pty = 1;
		break;
	case OPT_OPEN_STDIN:
		args->open_stdin = 1;
		break;
	case OPT_START_TIMEOUT:
		if(!is_non_negative_num(arg)) {
			fprintf(stderr, "Error start timeout parameter:%s.\n", arg);
			return -1;
		}
		args->start_timeout = (unsigned int)atoi(arg);
		break;
#endif

	}
	return 0;
}

static int ensure_path(char **confpath, const char *path)
{
	int fd;
	char *fullpath = NULL;

	if (path) {
		if (access(path, W_OK)) {
			fd = creat(path, 0600);
			if (fd < 0 && errno != EEXIST) {
				ERROR("Failed to create '%s'", path);
				return -1;
			}

			if (fd >= 0)
				close(fd);
		}

		fullpath = realpath(path, NULL);
		if (!fullpath) {
			ERROR("Failed to get the real path of '%s'", path);
			return -1;
		}

		*confpath = fullpath;
	}

	return 0;
}

int main(int argc, char *argv[])
{
	const char *lxcpath;
	char *const *args;
	struct lxc_container *c;
	struct lxc_log log;
	int err = EXIT_FAILURE;
	char *rcfile = NULL;
	char *const default_args[] = {
		"/sbin/init",
		NULL,
	};
#ifdef HAVE_ISULAD
	char *container_info_file = NULL;
#endif

	lxc_list_init(&defines);

	if (lxc_caps_init())
		exit(err);

	if (lxc_arguments_parse(&my_args, argc, argv))
		exit(err);

	if (!my_args.argc)
		args = default_args;
	else
		args = my_args.argv;

	log.name = my_args.name;
	log.file = my_args.log_file;
	log.level = my_args.log_priority;
	log.prefix = my_args.progname;
	log.quiet = my_args.quiet;
	log.lxcpath = my_args.lxcpath[0];

	if (lxc_log_init(&log))
		exit(err);

	lxcpath = my_args.lxcpath[0];
	if (access(lxcpath, O_RDONLY) < 0) {
		ERROR("You lack access to %s", lxcpath);
		exit(err);
	}

	/*
	 * rcfile possibilities:
	 * 1. rcfile from random path specified in cli option
	 * 2. rcfile not specified, use $lxcpath/$lxcname/config
	 * 3. rcfile not specified and does not exist.
	 */
	/* rcfile is specified in the cli option */
	if (my_args.rcfile) {
		rcfile = (char *)my_args.rcfile;

		c = lxc_container_new(my_args.name, lxcpath);
		if (!c) {
			ERROR("Failed to create lxc_container");
			exit(err);
		}

		c->clear_config(c);

		if (!c->load_config(c, rcfile)) {
			ERROR("Failed to load rcfile");
			lxc_container_put(c);
			exit(err);
		}

		c->configfile = strdup(my_args.rcfile);
		if (!c->configfile) {
			ERROR("Out of memory setting new config filename");
			goto out;
		}
	} else {
		int rc;

		rc = asprintf(&rcfile, "%s/%s/config", lxcpath, my_args.name);
		if (rc == -1) {
			ERROR("Failed to allocate memory");
			exit(err);
		}

		/* container configuration does not exist */
		if (access(rcfile, F_OK)) {
			free(rcfile);
			rcfile = NULL;
		}

		c = lxc_container_new(my_args.name, lxcpath);
		if (!c) {
			ERROR("Failed to create lxc_container");
			exit(err);
		}
	}

	/* We do not check here whether the container is defined, because we
	 * support volatile containers. Which means the container does not need
	 * to be created for it to be started. You can just pass a configuration
	 * file as argument and start the container right away.
	 */
	if (!c->may_control(c)) {
		ERROR("Insufficent privileges to control %s", c->name);
		goto out;
	}

	if (c->is_running(c)) {
		ERROR("Container is already running");
		err = EXIT_SUCCESS;
		goto out;
	}

	/*
	 * We should use set_config_item() over &defines, which would handle
	 * unset c->lxc_conf for us and let us not use lxc_config_define_load()
	 */
	if (!c->lxc_conf) {
		ERROR("No container config specified");
		goto out;
	}

	if (!lxc_config_define_load(&defines, c))
		goto out;

	if (!rcfile && !strcmp("/sbin/init", args[0])) {
		ERROR("Executing '/sbin/init' with no configuration file may crash the host");
		goto out;
	}

	if (my_args.pidfile)
		if (ensure_path(&c->pidfile, my_args.pidfile) < 0) {
			ERROR("Failed to ensure pidfile '%s'", my_args.pidfile);
			goto out;
		}

#ifdef HAVE_ISULAD
	/* isulad: container info file used to store pid and ppid info of container*/
	if (my_args.container_info != NULL) {
		if (ensure_path(&container_info_file, my_args.container_info) < 0) {
			ERROR("Failed to ensure container's piddile '%s'", my_args.container_info);
			goto out;
		}
		if (!c->set_container_info_file(c, container_info_file)) {
			ERROR("Failed to set container's piddile '%s'", container_info_file);
			goto out;
		}
	}

	if (my_args.terminal_fifos[0] || my_args.terminal_fifos[1] || my_args.terminal_fifos[2]) {
		c->set_terminal_init_fifos(c, my_args.terminal_fifos[0], my_args.terminal_fifos[1], my_args.terminal_fifos[2]);
	}

	/* isulad: fifo used to monitor state of monitor process */
	if (my_args.exit_monitor_fifo != NULL) {
		c->exit_fifo = safe_strdup(my_args.exit_monitor_fifo);
	}

	if (my_args.disable_pty) {
		c->want_disable_pty(c, true);
	}

	if (my_args.open_stdin) {
		c->want_open_stdin(c, true);
	}

	/* isulad: add start timeout */
	if(my_args.start_timeout) {
		c->set_start_timeout(c, my_args.start_timeout);
	}
#endif

	if (my_args.console)
		if (!c->set_config_item(c, "lxc.console.path", my_args.console))
			goto out;

	if (my_args.console_log)
		if (!c->set_config_item(c, "lxc.console.logfile", my_args.console_log))
			goto out;

	if (!lxc_setup_shared_ns(&my_args, c))
		goto out;

	if (!my_args.daemonize)
		c->want_daemonize(c, false);

	if (my_args.close_all_fds)
		c->want_close_all_fds(c, true);

	if (args == default_args)
		err = c->start(c, 0, NULL) ? EXIT_SUCCESS : EXIT_FAILURE;
	else
		err = c->start(c, 0, args) ? EXIT_SUCCESS : EXIT_FAILURE;
	if (err) {
#ifdef HAVE_ISULAD
		if (c->lxc_conf->errmsg)
			fprintf(stderr, "%s:%s:%s:%d starting container process caused \"%s\"", c->name,
					__FILE__, __func__, __LINE__, c->lxc_conf->errmsg);
#endif
		ERROR("The container failed to start");

		if (my_args.daemonize)
			ERROR("To get more details, run the container in foreground mode");

		ERROR("Additional information can be obtained by setting the "
		      "--logfile and --logpriority options");

		err = c->error_num;
		lxc_container_put(c);
		exit(err);
	}

out:
	lxc_container_put(c);
#ifdef HAVE_ISULAD
	free(container_info_file);
#endif
	exit(err);
}
