/* SPDX-License-Identifier: LGPL-2.1+ */
/******************************************************************************
 * Copyright (c) Huawei Technologies Co., Ltd. 2020. Allrights reserved
 * Description: isulad utils
 * Author: lifeng
 * Create: 2020-04-11
******************************************************************************/
#ifndef __ISULAD_PATH_H_
#define __ISULAD_PATH_H_

#include <stdbool.h>

bool specify_current_dir(const char *path);

bool has_traling_path_separator(const char *path);

// PreserveTrailingDotOrSeparator returns the given cleaned path
// and appends a trailing `/.` or `/` if its corresponding  original
// path ends with a trailing `/.` or `/`. If the cleaned
// path already ends in a `.` path segment, then another is not added. If the
// clean path already ends in a path separator, then another is not added.
char *preserve_trailing_dot_or_separator(const char *cleanedpath,
                const char *originalpath);


// Split splits path immediately following the final Separator,
// separating it into a directory and file name component.
// If there is no Separator in path, Split returns an empty dir
// and file set to path.
// The returned values have the property that path = dir+file.
bool filepath_split(const char *path, char **dir, char **base);

/*
 * cleanpath is similar to realpath of glibc, but not expands symbolic links,
 * and not check the existence of components of the path.
 */
char *cleanpath(const char *path, char *realpath, size_t realpath_len);


// FollowSymlinkInScope is a wrapper around evalSymlinksInScope that returns an
// absolute path. This function handles paths in a platform-agnostic manner.
char *follow_symlink_in_scope(const char *fullpath, const char *rootpath);

// GetResourcePath evaluates `path` in the scope of the container's rootpath, with proper path
// sanitisation. Symlinks are all scoped to the rootpath of the container, as
// though the container's rootpath was `/`.
//
// The BaseFS of a container is the host-facing path which is bind-mounted as
// `/` inside the container. This method is essentially used to access a
// particular path inside the container as though you were a process in that
// container.
int get_resource_path(const char *rootpath, const char *path,
                      char **scopepath);

// Rel returns a relative path that is lexically equivalent to targpath when
// joined to basepath with an intervening separator. That is,
// Join(basepath, Rel(basepath, targpath)) is equivalent to targpath itself.
// On success, the returned path will always be relative to basepath,
// even if basepath and targpath share no elements.
// An error is returned if targpath can't be made relative to basepath or if
// knowing the current working directory would be necessary to compute it.
// Rel calls Clean on the result.
char *path_relative(const char *basepath, const char *targpath);

#endif
