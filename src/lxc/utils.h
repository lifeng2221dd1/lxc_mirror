/* SPDX-License-Identifier: LGPL-2.1+ */

#ifndef __LXC_UTILS_H
#define __LXC_UTILS_H

/* Properly support loop devices on 32bit systems. */
#define _FILE_OFFSET_BITS 64

#ifndef MAX_GRBUF_SIZE
#define MAX_GRBUF_SIZE 65536
#endif

#include <errno.h>
#include <linux/loop.h>
#include <linux/types.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/vfs.h>
#include <unistd.h>

#include "file_utils.h"
#include "initutils.h"
#include "macro.h"
#include "memory_utils.h"
#include "raw_syscalls.h"
#include "string_utils.h"

#ifdef HAVE_ISULAD
#include "isulad_utils.h"

/* isulad: replace space with SPACE_MAGIC_STR */
#define SPACE_MAGIC_STR "[#)"
#endif

/* returns 1 on success, 0 if there were any failures */
extern int lxc_rmdir_onedev(const char *path, const char *exclude);
extern int get_u16(unsigned short *val, const char *arg, int base);
extern int mkdir_p(const char *dir, mode_t mode);
extern char *get_rundir(void);

/* Define getline() if missing from the C library */
#ifndef HAVE_GETLINE
#ifdef HAVE_FGETLN
#include <../include/getline.h>
#endif
#endif

#ifdef HAVE_ISULAD
/* isulad:
 ld cutime, cstime, priority, nice, timeout, it_real_value, rss,
 c  state,
 d  ppid, pgrp, session, tty, tpgid,
 s  signal, blocked, sigignore, sigcatch,
 lu flags, min_flt, cmin_flt, maj_flt, cmaj_flt, utime, stime,
 lu rss_rlim, start_code, end_code, start_stack, kstk_esp, kstk_eip,
 lu start_time, vsize, wchan, nswap, cnswap,
*/

/* Basic data structure which holds all information we can get about a process.
 * (unless otherwise specified, fields are read from /proc/#/stat)
 *
 * Most of it comes from task_struct in linux/sched.h
 */
typedef struct proc_t {
    // 1st 16 bytes
    int pid;        /* process id */
    int ppid;       /* pid of parent process */

    char state;     /* single-char code for process state (S=sleeping) */

    unsigned long long
    utime,      /* user-mode CPU time accumulated by process */
    stime,      /* kernel-mode CPU time accumulated by process */
    // and so on...
    cutime,     /* cumulative utime of process and reaped children */
    cstime,     /* cumulative stime of process and reaped children */
    start_time; /* start time of process -- seconds since 1-1-70 */

    long
    priority,   /* kernel scheduling priority */
    timeout,    /* ? */
    nice,       /* standard unix nice level of process */
    rss,        /* resident set size from /proc/#/stat (pages) */
    it_real_value;  /* ? */
    unsigned long
    rtprio,     /* real-time priority */
    sched,      /* scheduling class */
    vsize,      /* number of pages of virtual memory ... */
    rss_rlim,   /* resident set size limit? */
    flags,      /* kernel flags for the process */
    min_flt,    /* number of minor page faults since process start */
    maj_flt,    /* number of major page faults since process start */
    cmin_flt,   /* cumulative min_flt of process and child processes */
    cmaj_flt,   /* cumulative maj_flt of process and child processes */
    nswap,      /* ? */
    cnswap,     /* cumulative nswap ? */
    start_code, /* address of beginning of code segment */
    end_code,   /* address of end of code segment */
    start_stack,    /* address of the bottom of stack for the process */
    kstk_esp,   /* kernel stack pointer */
    kstk_eip,   /* kernel instruction pointer */
    wchan;      /* address of kernel wait channel proc is sleeping in */

    char cmd[16];   /* basename of executable file in call to exec(2) */
    int
    pgrp,       /* process group id */
    session,    /* session id */
    tty,        /* full device number of controlling terminal */
    tpgid,      /* terminal process group id */
    exit_signal,    /* might not be SIGCHLD */
    processor;      /* current (or most recent?) CPU */
} proc_t;
#endif

static inline int lxc_set_cloexec(int fd)
{
	return fcntl(fd, F_SETFD, FD_CLOEXEC);
}

/*
 * Struct to carry child pid from lxc_popen() to lxc_pclose(). Not an opaque
 * struct to allow direct access to the underlying FILE without additional
 * wrappers.
 */
struct lxc_popen_FILE {
	int pipe;
	FILE *f;
	pid_t child_pid;
};

/* popen(command, "re") replacement that restores default signal mask
 * via sigprocmask(2) (unblocks all signals) after fork(2) but prior to calling exec(3).
 * In short, popen(command, "re") does pipe() + fork()                 + exec()
 * while lxc_popen(command)       does pipe() + fork() + sigprocmask() + exec().
 * Returns pointer to struct lxc_popen_FILE, that should be freed with lxc_pclose().
 * On error returns NULL.
 */
extern struct lxc_popen_FILE *lxc_popen(const char *command);

/* pclose() replacement to be used on struct lxc_popen_FILE *,
 * returned by lxc_popen().
 * Waits for associated process to terminate, returns its exit status and
 * frees resources, pointed to by struct lxc_popen_FILE *.
 */
extern int lxc_pclose(struct lxc_popen_FILE *fp);

static inline void __auto_lxc_pclose__(struct lxc_popen_FILE **f)
{
	if (*f)
		lxc_pclose(*f);
}
#define __do_lxc_pclose __attribute__((__cleanup__(__auto_lxc_pclose__)))

/*
 * wait on a child we forked
 */
extern int wait_for_pid(pid_t pid);
extern int lxc_wait_for_pid_status(pid_t pid);
extern int wait_for_pidfd(int pidfd);

#if HAVE_OPENSSL
extern int sha1sum_file(char *fnam, unsigned char *md_value, unsigned int *md_len);
#endif

/* initialize rand with urandom */
extern int randseed(bool);

/* are we unprivileged with respect to our namespaces */
inline static bool am_guest_unpriv(void) {
	return geteuid() != 0;
}

/* are we unprivileged with respect to init_user_ns */
inline static bool am_host_unpriv(void)
{
	__do_fclose FILE *f = NULL;
	uid_t user, host, count;
	int ret;

	if (geteuid() != 0)
		return true;

	/* Now: are we in a user namespace? Because then we're also
	 * unprivileged.
	 */
	f = fopen("/proc/self/uid_map", "re");
	if (!f)
		return false;

	ret = fscanf(f, "%u %u %u", &user, &host, &count);
	if (ret != 3)
		return false;

	return user != 0 || host != 0 || count != UINT32_MAX;
}

/*
 * parse /proc/self/uid_map to find what @orig maps to
 */
extern uid_t get_ns_uid(uid_t orig);
/*
 * parse /proc/self/gid_map to find what @orig maps to
 */
extern gid_t get_ns_gid(gid_t orig);

extern bool dir_exists(const char *path);

#define FNV1A_64_INIT ((uint64_t)0xcbf29ce484222325ULL)
extern uint64_t fnv_64a_buf(void *buf, size_t len, uint64_t hval);

extern bool is_shared_mountpoint(const char *path);
extern int detect_shared_rootfs(void);
extern bool detect_ramfs_rootfs(void);
extern char *on_path(const char *cmd, const char *rootfs);
extern bool cgns_supported(void);
extern char *choose_init(const char *rootfs);
extern bool switch_to_ns(pid_t pid, const char *ns);
extern char *get_template_path(const char *t);
extern int open_without_symlink(const char *target, const char *prefix_skip);
#ifdef HAVE_ISULAD
extern int safe_mount(const char *src, const char *dest, const char *fstype,
		      unsigned long flags, const void *data,
		      const char *rootfs, const char *mount_label);
#else
extern int safe_mount(const char *src, const char *dest, const char *fstype,
		      unsigned long flags, const void *data,
		      const char *rootfs);
#endif
extern int lxc_mount_proc_if_needed(const char *rootfs);
extern int open_devnull(void);
extern int set_stdfds(int fd);
extern int null_stdfds(void);
extern int lxc_preserve_ns(const int pid, const char *ns);

/* Check whether a signal is blocked by a process. */
extern bool task_blocks_signal(pid_t pid, int signal);

/* Switch to a new uid and gid.
 * If LXC_INVALID_{G,U}ID is passed then the set{g,u}id() will not be called.
 */
extern bool lxc_switch_uid_gid(uid_t uid, gid_t gid);
extern bool lxc_setgroups(int size, gid_t list[]);

/* Find an unused loop device and associate it with source. */
extern int lxc_prepare_loop_dev(const char *source, char *loop_dev, int flags);

/* Clear all mounts on a given node.
 * >= 0 successfully cleared. The number returned is the number of umounts
 *      performed.
 * < 0  error umounting. Return -errno.
 */
extern int lxc_unstack_mountpoint(const char *path, bool lazy);

/*
 * run_command runs a command and collect it's std{err,out} output in buf.
 *
 * @param[out] buf     The buffer where the commands std{err,out] output will be
 *                     read into. If no output was produced, buf will be memset
 *                     to 0.
 * @param[in] buf_size The size of buf. This function will reserve one byte for
 *                     \0-termination.
 * @param[in] child_fn The function to be run in the child process. This
 *                     function must exec.
 * @param[in] args     Arguments to be passed to child_fn.
 */
extern int run_command(char *buf, size_t buf_size, int (*child_fn)(void *),
		       void *args);

/*
 * run_command runs a command and collect it's std{err,out} output in buf, returns exit status.
 *
 * @param[out] buf     The buffer where the commands std{err,out] output will be
 *                     read into. If no output was produced, buf will be memset
 *                     to 0.
 * @param[in] buf_size The size of buf. This function will reserve one byte for
 *                     \0-termination.
 * @param[in] child_fn The function to be run in the child process. This
 *                     function must exec.
 * @param[in] args     Arguments to be passed to child_fn.
 */
extern int run_command_status(char *buf, size_t buf_size, int (*child_fn)(void *),
		       void *args);

/* return copy of string @entry;  do not fail. */
extern char *must_copy_string(const char *entry);

/* Re-allocate a pointer, do not fail */
extern void *must_realloc(void *orig, size_t sz);

extern bool lxc_nic_exists(char *nic);

static inline uint64_t lxc_getpagesize(void)
{
	int64_t pgsz;

	pgsz = sysconf(_SC_PAGESIZE);
	if (pgsz <= 0)
		pgsz = 1 << 12;

	return pgsz;
}

/* If n is not a power of 2 this function will return the next power of 2
 * greater than that number. Note that this function always returns the *next*
 * power of 2 *greater* that number not the *nearest*. For example, passing 1025
 * as argument this function will return 2048 although the closest power of 2
 * would be 1024.
 * If the caller passes in 0 they will receive 0 in return since this is invalid
 * input and 0 is not a power of 2.
 */
extern uint64_t lxc_find_next_power2(uint64_t n);

/* Set a signal the child process will receive after the parent has died. */
extern int lxc_set_death_signal(int signal, pid_t parent, int parent_status_fd);
extern int fd_cloexec(int fd, bool cloexec);
extern int lxc_rm_rf(const char *dirname);
extern int lxc_setup_keyring(char *keyring_label);
extern bool lxc_can_use_pidfd(int pidfd);

extern int fix_stdio_permissions(uid_t uid);

#ifdef HAVE_ISULAD
extern void lxc_write_error_message(int errfd, const char *format, ...);
extern int lxc_file2str(const char *filename, char ret[], int cap);
extern int unsigned long long lxc_get_process_startat(pid_t pid);
// set env home in container
extern int lxc_setup_env_home(uid_t uid);

extern bool lxc_process_alive(pid_t pid, unsigned long long start_time);

extern bool is_non_negative_num(const char *s);
#endif

#endif /* __LXC_UTILS_H */
