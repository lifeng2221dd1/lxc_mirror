// Generated from spec.json. Do not edit!
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <string.h>
#include <read-file.h>
#include "oci_runtime_spec.h"

oci_runtime_spec_hooks *make_oci_runtime_spec_hooks(yajl_val tree, struct parser_context *ctx, parser_error *err) {
    oci_runtime_spec_hooks *ret = NULL;
    *err = 0;
    if (tree == NULL)
        return ret;
    ret = safe_malloc(sizeof(*ret));
    {
        yajl_val tmp = get_val(tree, "prestart", yajl_t_array);
        if (tmp != NULL && YAJL_GET_ARRAY(tmp) != NULL && YAJL_GET_ARRAY(tmp)->len > 0) {
            size_t i;
            ret->prestart_len = YAJL_GET_ARRAY(tmp)->len;
            ret->prestart = safe_malloc((YAJL_GET_ARRAY(tmp)->len + 1) * sizeof(*ret->prestart));
            for (i = 0; i < YAJL_GET_ARRAY(tmp)->len; i++) {
                yajl_val val = YAJL_GET_ARRAY(tmp)->values[i];
                ret->prestart[i] = make_defs_hook(val, ctx, err);
                if (ret->prestart[i] == NULL) {
                    free_oci_runtime_spec_hooks(ret);
                    return NULL;
                }
            }
        }
    }
    {
        yajl_val tmp = get_val(tree, "poststart", yajl_t_array);
        if (tmp != NULL && YAJL_GET_ARRAY(tmp) != NULL && YAJL_GET_ARRAY(tmp)->len > 0) {
            size_t i;
            ret->poststart_len = YAJL_GET_ARRAY(tmp)->len;
            ret->poststart = safe_malloc((YAJL_GET_ARRAY(tmp)->len + 1) * sizeof(*ret->poststart));
            for (i = 0; i < YAJL_GET_ARRAY(tmp)->len; i++) {
                yajl_val val = YAJL_GET_ARRAY(tmp)->values[i];
                ret->poststart[i] = make_defs_hook(val, ctx, err);
                if (ret->poststart[i] == NULL) {
                    free_oci_runtime_spec_hooks(ret);
                    return NULL;
                }
            }
        }
    }
    {
        yajl_val tmp = get_val(tree, "poststop", yajl_t_array);
        if (tmp != NULL && YAJL_GET_ARRAY(tmp) != NULL && YAJL_GET_ARRAY(tmp)->len > 0) {
            size_t i;
            ret->poststop_len = YAJL_GET_ARRAY(tmp)->len;
            ret->poststop = safe_malloc((YAJL_GET_ARRAY(tmp)->len + 1) * sizeof(*ret->poststop));
            for (i = 0; i < YAJL_GET_ARRAY(tmp)->len; i++) {
                yajl_val val = YAJL_GET_ARRAY(tmp)->values[i];
                ret->poststop[i] = make_defs_hook(val, ctx, err);
                if (ret->poststop[i] == NULL) {
                    free_oci_runtime_spec_hooks(ret);
                    return NULL;
                }
            }
        }
    }

    if (tree->type == yajl_t_object && (ctx->options & PARSE_OPTIONS_STRICT)) {
        int i;
        for (i = 0; i < tree->u.object.len; i++)
            if (strcmp(tree->u.object.keys[i], "prestart") &&
                strcmp(tree->u.object.keys[i], "poststart") &&
                strcmp(tree->u.object.keys[i], "poststop")) {
                if (ctx->stderr > 0)
                    fprintf(ctx->stderr, "WARNING: unknown key found: %s\n", tree->u.object.keys[i]);
            }
        }
    return ret;
}

void free_oci_runtime_spec_hooks(oci_runtime_spec_hooks *ptr) {
    if (ptr == NULL)
        return;
    if (ptr->prestart != NULL) {
        size_t i;
        for (i = 0; i < ptr->prestart_len; i++)
            if (ptr->prestart[i] != NULL) {
                free_defs_hook(ptr->prestart[i]);
                ptr->prestart[i] = NULL;
            }
        free(ptr->prestart);
        ptr->prestart = NULL;
    }
    if (ptr->poststart != NULL) {
        size_t i;
        for (i = 0; i < ptr->poststart_len; i++)
            if (ptr->poststart[i] != NULL) {
                free_defs_hook(ptr->poststart[i]);
                ptr->poststart[i] = NULL;
            }
        free(ptr->poststart);
        ptr->poststart = NULL;
    }
    if (ptr->poststop != NULL) {
        size_t i;
        for (i = 0; i < ptr->poststop_len; i++)
            if (ptr->poststop[i] != NULL) {
                free_defs_hook(ptr->poststop[i]);
                ptr->poststop[i] = NULL;
            }
        free(ptr->poststop);
        ptr->poststop = NULL;
    }
    free(ptr);
}

yajl_gen_status gen_oci_runtime_spec_hooks(yajl_gen g, oci_runtime_spec_hooks *ptr, struct parser_context *ctx, parser_error *err) {
    yajl_gen_status stat = yajl_gen_status_ok;
    *err = 0;
    stat = reformat_start_map(g);
    if (yajl_gen_status_ok != stat)
        GEN_SET_ERROR_AND_RETURN(stat, err);
    if ((ctx->options & GEN_OPTIONS_ALLKEYVALUE) ||(ptr != NULL && ptr->prestart != NULL)) {
        size_t len = 0, i;
        stat = reformat_map_key(g, "prestart", strlen("prestart"));
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
        if (ptr != NULL && ptr->prestart != NULL) {
            len = ptr->prestart_len;
        }
        if (!len && !(ctx->options & GEN_OPTIONS_SIMPLIFY))
            yajl_gen_config(g, yajl_gen_beautify, 0);
        stat = reformat_start_array(g);
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
        for (i = 0; i < len; i++) {
            stat = gen_defs_hook(g, ptr->prestart[i], ctx, err);
            if (yajl_gen_status_ok != stat)
                GEN_SET_ERROR_AND_RETURN(stat, err);
        }
        stat = reformat_end_array(g);
        if (!len && !(ctx->options & GEN_OPTIONS_SIMPLIFY))
            yajl_gen_config(g, yajl_gen_beautify, 1);
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
    }
    if ((ctx->options & GEN_OPTIONS_ALLKEYVALUE) ||(ptr != NULL && ptr->poststart != NULL)) {
        size_t len = 0, i;
        stat = reformat_map_key(g, "poststart", strlen("poststart"));
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
        if (ptr != NULL && ptr->poststart != NULL) {
            len = ptr->poststart_len;
        }
        if (!len && !(ctx->options & GEN_OPTIONS_SIMPLIFY))
            yajl_gen_config(g, yajl_gen_beautify, 0);
        stat = reformat_start_array(g);
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
        for (i = 0; i < len; i++) {
            stat = gen_defs_hook(g, ptr->poststart[i], ctx, err);
            if (yajl_gen_status_ok != stat)
                GEN_SET_ERROR_AND_RETURN(stat, err);
        }
        stat = reformat_end_array(g);
        if (!len && !(ctx->options & GEN_OPTIONS_SIMPLIFY))
            yajl_gen_config(g, yajl_gen_beautify, 1);
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
    }
    if ((ctx->options & GEN_OPTIONS_ALLKEYVALUE) ||(ptr != NULL && ptr->poststop != NULL)) {
        size_t len = 0, i;
        stat = reformat_map_key(g, "poststop", strlen("poststop"));
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
        if (ptr != NULL && ptr->poststop != NULL) {
            len = ptr->poststop_len;
        }
        if (!len && !(ctx->options & GEN_OPTIONS_SIMPLIFY))
            yajl_gen_config(g, yajl_gen_beautify, 0);
        stat = reformat_start_array(g);
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
        for (i = 0; i < len; i++) {
            stat = gen_defs_hook(g, ptr->poststop[i], ctx, err);
            if (yajl_gen_status_ok != stat)
                GEN_SET_ERROR_AND_RETURN(stat, err);
        }
        stat = reformat_end_array(g);
        if (!len && !(ctx->options & GEN_OPTIONS_SIMPLIFY))
            yajl_gen_config(g, yajl_gen_beautify, 1);
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
    }
    stat = reformat_end_map(g);
    if (yajl_gen_status_ok != stat)
        GEN_SET_ERROR_AND_RETURN(stat, err);
    return yajl_gen_status_ok;
}
