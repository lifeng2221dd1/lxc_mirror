// Generated from json-file.json. Do not edit!
#ifndef LOGGER_JSON_FILE_SCHEMA_H
#define LOGGER_JSON_FILE_SCHEMA_H

#include <sys/types.h>
#include <stdint.h>
#include "json_common.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    uint8_t *log;
    size_t log_len;

    char *stream;

    char *time;

    uint8_t *attrs;
    size_t attrs_len;

}
logger_json_file;

void free_logger_json_file(logger_json_file *ptr);

logger_json_file *make_logger_json_file(yajl_val tree, struct parser_context *ctx, parser_error *err);

yajl_gen_status gen_logger_json_file(yajl_gen g, logger_json_file *ptr, struct parser_context *ctx, parser_error *err);

logger_json_file *logger_json_file_parse_file(const char *filename, struct parser_context *ctx, parser_error *err);

logger_json_file *logger_json_file_parse_file_stream(FILE *stream, struct parser_context *ctx, parser_error *err);

logger_json_file *logger_json_file_parse_data(const char *jsondata, struct parser_context *ctx, parser_error *err);

char *logger_json_file_generate_json(logger_json_file *ptr, struct parser_context *ctx, parser_error *err);

#ifdef __cplusplus
}
#endif

#endif
