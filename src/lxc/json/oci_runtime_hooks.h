/******************************************************************************
 * Copyright (C), 1988-1999, Huawei Tech. Co., Ltd.
 * FileName: oci_runtime_hooks.h
 * Author: tanyifeng  Version: 0.1   Date: 2018-11-08
 * Explanation: provide container oci runtime hooks function definition
 ******************************************************************************/
#ifndef _CONTAINER_HOOKS_H
# define _CONTAINER_HOOKS_H

# include "oci_runtime_spec.h"

oci_runtime_spec_hooks *oci_runtime_spec_hooks_parse_file(const char *filename,
                                                          struct parser_context *ctx, parser_error *err);

#endif
