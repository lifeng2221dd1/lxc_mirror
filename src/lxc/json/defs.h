// Generated from defs.json. Do not edit!
#ifndef DEFS_SCHEMA_H
#define DEFS_SCHEMA_H

#include <sys/types.h>
#include <stdint.h>
#include "json_common.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    char *path;

    char **args;
    size_t args_len;

    char **env;
    size_t env_len;

    int timeout;

}
defs_hook;

void free_defs_hook(defs_hook *ptr);

defs_hook *make_defs_hook(yajl_val tree, struct parser_context *ctx, parser_error *err);

yajl_gen_status gen_defs_hook(yajl_gen g, defs_hook *ptr, struct parser_context *ctx, parser_error *err);

#ifdef __cplusplus
}
#endif

#endif
