// Generated from defs.json. Do not edit!
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <string.h>
#include <read-file.h>
#include "defs.h"

defs_hook *make_defs_hook(yajl_val tree, struct parser_context *ctx, parser_error *err) {
    defs_hook *ret = NULL;
    *err = 0;
    if (tree == NULL)
        return ret;
    ret = safe_malloc(sizeof(*ret));
    {
        yajl_val val = get_val(tree, "path", yajl_t_string);
        if (val != NULL) {
            char *str = YAJL_GET_STRING(val);
            ret->path = safe_strdup(str ? str : "");
        }
    }
    {
        yajl_val tmp = get_val(tree, "args", yajl_t_array);
        if (tmp != NULL && YAJL_GET_ARRAY(tmp) != NULL && YAJL_GET_ARRAY(tmp)->len > 0) {
            size_t i;
            ret->args_len = YAJL_GET_ARRAY(tmp)->len;
            if (YAJL_GET_ARRAY(tmp)->len > SIZE_MAX / sizeof(*ret->args) - 1) {
                free_defs_hook(ret);
                return NULL;
            }
            ret->args = safe_malloc((YAJL_GET_ARRAY(tmp)->len + 1) * sizeof(*ret->args));
            for (i = 0; i < YAJL_GET_ARRAY(tmp)->len; i++) {
                yajl_val val = YAJL_GET_ARRAY(tmp)->values[i];
                if (val != NULL) {
                    char *str = YAJL_GET_STRING(val);
                    ret->args[i] = safe_strdup(str ? str : "");
                }
            }
        }
    }
    {
        yajl_val tmp = get_val(tree, "env", yajl_t_array);
        if (tmp != NULL && YAJL_GET_ARRAY(tmp) != NULL && YAJL_GET_ARRAY(tmp)->len > 0) {
            size_t i;
            ret->env_len = YAJL_GET_ARRAY(tmp)->len;
            if (YAJL_GET_ARRAY(tmp)->len > SIZE_MAX / sizeof(*ret->env) - 1) {
                free_defs_hook(ret);
                return NULL;
            }
            ret->env = safe_malloc((YAJL_GET_ARRAY(tmp)->len + 1) * sizeof(*ret->env));
            for (i = 0; i < YAJL_GET_ARRAY(tmp)->len; i++) {
                yajl_val val = YAJL_GET_ARRAY(tmp)->values[i];
                if (val != NULL) {
                    char *str = YAJL_GET_STRING(val);
                    ret->env[i] = safe_strdup(str ? str : "");
                }
            }
        }
    }
    {
        yajl_val val = get_val(tree, "timeout", yajl_t_number);
        if (val != NULL) {
            int invalid = common_safe_int(YAJL_GET_NUMBER(val), (int *)&ret->timeout);
            if (invalid) {
                if (asprintf(err, "Invalid value '%s' with type 'integer' for key 'timeout': %s", YAJL_GET_NUMBER(val), strerror(-invalid)) < 0)
                    *err = safe_strdup("error allocating memory");
                free_defs_hook(ret);
                return NULL;
            }
        }
    }
    if (ret->path == NULL) {
        if (asprintf(err, "Required field '%s' not present", "path") < 0)
            *err = safe_strdup("error allocating memory");
        free_defs_hook(ret);
        return NULL;
    }

    if (tree->type == yajl_t_object && (ctx->options & PARSE_OPTIONS_STRICT)) {
        int i;
        for (i = 0; i < tree->u.object.len; i++)
            if (strcmp(tree->u.object.keys[i], "path") &&
                strcmp(tree->u.object.keys[i], "args") &&
                strcmp(tree->u.object.keys[i], "env") &&
                strcmp(tree->u.object.keys[i], "timeout")) {
                if (ctx->stderr > 0)
                    fprintf(ctx->stderr, "WARNING: unknown key found: %s\n", tree->u.object.keys[i]);
            }
        }
    return ret;
}

void free_defs_hook(defs_hook *ptr) {
    if (ptr == NULL)
        return;
    free(ptr->path);
    ptr->path = NULL;
    if (ptr->args != NULL) {
        size_t i;
        for (i = 0; i < ptr->args_len; i++) {
            if (ptr->args[i] != NULL) {
                free(ptr->args[i]);
                ptr->args[i] = NULL;
            }
        }
        free(ptr->args);
        ptr->args = NULL;
    }
    if (ptr->env != NULL) {
        size_t i;
        for (i = 0; i < ptr->env_len; i++) {
            if (ptr->env[i] != NULL) {
                free(ptr->env[i]);
                ptr->env[i] = NULL;
            }
        }
        free(ptr->env);
        ptr->env = NULL;
    }
    free(ptr);
}

yajl_gen_status gen_defs_hook(yajl_gen g, defs_hook *ptr, struct parser_context *ctx, parser_error *err) {
    yajl_gen_status stat = yajl_gen_status_ok;
    *err = 0;
    stat = reformat_start_map(g);
    if (yajl_gen_status_ok != stat)
        GEN_SET_ERROR_AND_RETURN(stat, err);
    if ((ctx->options & GEN_OPTIONS_ALLKEYVALUE) ||(ptr != NULL && ptr->path != NULL)) {
        char *str = "";
        stat = reformat_map_key(g, "path", strlen("path"));
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
        if (ptr != NULL && ptr->path != NULL) {
            str = ptr->path;
        }
        stat = reformat_string(g, str, strlen(str));
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
    }
    if ((ctx->options & GEN_OPTIONS_ALLKEYVALUE) || (ptr != NULL && ptr->args != NULL)) {
        size_t len = 0, i;
        stat = reformat_map_key(g, "args", strlen("args"));
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
        if (ptr != NULL && ptr->args != NULL) {
            len = ptr->args_len;
        }
        if (!len && !(ctx->options & GEN_OPTIONS_SIMPLIFY))
            yajl_gen_config(g, yajl_gen_beautify, 0);
        stat = reformat_start_array(g);
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
        for (i = 0; i < len; i++) {
            stat = reformat_string(g, ptr->args[i], strlen(ptr->args[i]));
            if (yajl_gen_status_ok != stat)
                GEN_SET_ERROR_AND_RETURN(stat, err);
        }
        stat = reformat_end_array(g);
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
        if (!len && !(ctx->options & GEN_OPTIONS_SIMPLIFY))
            yajl_gen_config(g, yajl_gen_beautify, 1);
    }
    if ((ctx->options & GEN_OPTIONS_ALLKEYVALUE) || (ptr != NULL && ptr->env != NULL)) {
        size_t len = 0, i;
        stat = reformat_map_key(g, "env", strlen("env"));
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
        if (ptr != NULL && ptr->env != NULL) {
            len = ptr->env_len;
        }
        if (!len && !(ctx->options & GEN_OPTIONS_SIMPLIFY))
            yajl_gen_config(g, yajl_gen_beautify, 0);
        stat = reformat_start_array(g);
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
        for (i = 0; i < len; i++) {
            stat = reformat_string(g, ptr->env[i], strlen(ptr->env[i]));
            if (yajl_gen_status_ok != stat)
                GEN_SET_ERROR_AND_RETURN(stat, err);
        }
        stat = reformat_end_array(g);
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
        if (!len && !(ctx->options & GEN_OPTIONS_SIMPLIFY))
            yajl_gen_config(g, yajl_gen_beautify, 1);
    }
    if ((ctx->options & GEN_OPTIONS_ALLKEYVALUE) ||(ptr != NULL && ptr->timeout)) {
        long long int num = 0;
        stat = reformat_map_key(g, "timeout", strlen("timeout"));
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
        if (ptr != NULL && ptr->timeout) {
            num = (long long int)ptr->timeout;
        }
        stat = reformat_int(g, num);
        if (yajl_gen_status_ok != stat)
            GEN_SET_ERROR_AND_RETURN(stat, err);
    }
    stat = reformat_end_map(g);
    if (yajl_gen_status_ok != stat)
        GEN_SET_ERROR_AND_RETURN(stat, err);
    return yajl_gen_status_ok;
}
