#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include <config.h>
#include "read-file.h"

#ifndef O_CLOEXEC
#define O_CLOEXEC 02000000
#endif

char *fread_file(FILE *stream, size_t *length)
{
	char *buf = NULL, *tmpbuf = NULL;
	size_t off = 0;

	while (1) {
		size_t ret, newsize;

		newsize = off + BUFSIZ + 1;
		tmpbuf = (char *)calloc(1, newsize);
		if (tmpbuf == NULL) {
			goto out;
		}

		if (buf) {
			memcpy(tmpbuf, buf, off);

			memset(buf, 0, off);

			free(buf);
		}

		buf = tmpbuf;
		ret = fread(buf + off, 1, BUFSIZ, stream);
		if (!ret && ferror(stream)) {
			tmpbuf = NULL;
			goto out;
		}
		if (ret < BUFSIZ || feof(stream)) {
			*length = off + ret + 1;
			buf[*length - 1] = '\0';
			return buf;
		}
		off += BUFSIZ;
	}
out:
	if (buf) {
		free(buf);
	}
	if (tmpbuf) {
		free(tmpbuf);
	}
	return NULL;

}

char *read_file(const char *path, size_t *length)
{
	char *buf = NULL;
	char rpath[PATH_MAX + 1] = {0};
	int fd = -1;
	int tmperrno;
	FILE *fp = NULL;

	if (!path || !length) {
		return NULL;
	}

	if (strlen(path) > PATH_MAX || NULL == realpath(path, rpath)) {
		return NULL;
	}

	fd = open(rpath, O_RDONLY | O_CLOEXEC, 0640);
	if (fd < 0) {
		return NULL;
	}

	fp = fdopen(fd, "r");
	tmperrno = errno;
	if (!fp) {
		close(fd);
		errno = tmperrno;
		return NULL;
	}

	buf = fread_file(fp, length);
	fclose(fp);
	return buf;
}
