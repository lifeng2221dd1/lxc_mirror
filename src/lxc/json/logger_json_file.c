// Generated from json-file.json. Do not edit!
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <string.h>
#include <read-file.h>
#include "logger_json_file.h"

logger_json_file *make_logger_json_file(yajl_val tree, struct parser_context *ctx, parser_error *err) {
	logger_json_file *ret = NULL;
	*err = 0;
	if (tree == NULL)
		return ret;
	ret = safe_malloc(sizeof(*ret));
	{
		yajl_val tmp = get_val(tree, "log", yajl_t_string);
		if (tmp != NULL) {
			char *str = YAJL_GET_STRING(tmp);
			ret->log = (uint8_t *)safe_strdup(str ? str : "");
			ret->log_len = str != NULL ? strlen(str) : 0;
		}
	}
	{
		yajl_val val = get_val(tree, "stream", yajl_t_string);
		if (val != NULL) {
			char *str = YAJL_GET_STRING(val);
			ret->stream = safe_strdup(str ? str : "");
		}
	}
	{
		yajl_val val = get_val(tree, "time", yajl_t_string);
		if (val != NULL) {
			char *str = YAJL_GET_STRING(val);
			ret->time = safe_strdup(str ? str : "");
		}
	}
	{
		yajl_val tmp = get_val(tree, "attrs", yajl_t_string);
		if (tmp != NULL) {
			char *str = YAJL_GET_STRING(tmp);
			ret->attrs = (uint8_t *)safe_strdup(str ? str : "");
			ret->attrs_len = str != NULL ? strlen(str) : 0;
		}
	}

	if (tree->type == yajl_t_object && (ctx->options & PARSE_OPTIONS_STRICT)) {
		int i;
		for (i = 0; i < tree->u.object.len; i++)
			if (strcmp(tree->u.object.keys[i], "log") &&
			                strcmp(tree->u.object.keys[i], "stream") &&
			                strcmp(tree->u.object.keys[i], "time") &&
			                strcmp(tree->u.object.keys[i], "attrs")) {
				if (ctx->stderr > 0)
					fprintf(ctx->stderr, "WARNING: unknown key found: %s\n", tree->u.object.keys[i]);
			}
	}
	return ret;
}

void free_logger_json_file(logger_json_file *ptr) {
	if (ptr == NULL)
		return;
	free(ptr->log);
	ptr->log = NULL;
	free(ptr->stream);
	ptr->stream = NULL;
	free(ptr->time);
	ptr->time = NULL;
	free(ptr->attrs);
	ptr->attrs = NULL;
	free(ptr);
}

yajl_gen_status gen_logger_json_file(yajl_gen g, logger_json_file *ptr, struct parser_context *ctx, parser_error *err) {
	yajl_gen_status stat = yajl_gen_status_ok;
	*err = 0;
	stat = reformat_start_map(g);
	if (yajl_gen_status_ok != stat)
		GEN_SET_ERROR_AND_RETURN(stat, err);
	if ((ctx->options & GEN_OPTIONS_ALLKEYVALUE) || (ptr != NULL && ptr->log != NULL && ptr->log_len)) {
		const char *str = "";
		size_t len = 0;
		stat = reformat_map_key(g, "log", strlen("log"));
		if (yajl_gen_status_ok != stat)
			GEN_SET_ERROR_AND_RETURN(stat, err);
		if (ptr != NULL && ptr->log != NULL) {
			str = (const char *)ptr->log;
			len = ptr->log_len;
		}
		stat = reformat_string(g, str, len);
		if (yajl_gen_status_ok != stat)
			GEN_SET_ERROR_AND_RETURN(stat, err);
	}
	if ((ctx->options & GEN_OPTIONS_ALLKEYVALUE) ||(ptr != NULL && ptr->stream != NULL)) {
		char *str = "";
		stat = reformat_map_key(g, "stream", strlen("stream"));
		if (yajl_gen_status_ok != stat)
			GEN_SET_ERROR_AND_RETURN(stat, err);
		if (ptr != NULL && ptr->stream != NULL) {
			str = ptr->stream;
		}
		stat = reformat_string(g, str, strlen(str));
		if (yajl_gen_status_ok != stat)
			GEN_SET_ERROR_AND_RETURN(stat, err);
	}
	if ((ctx->options & GEN_OPTIONS_ALLKEYVALUE) ||(ptr != NULL && ptr->time != NULL)) {
		char *str = "";
		stat = reformat_map_key(g, "time", strlen("time"));
		if (yajl_gen_status_ok != stat)
			GEN_SET_ERROR_AND_RETURN(stat, err);
		if (ptr != NULL && ptr->time != NULL) {
			str = ptr->time;
		}
		stat = reformat_string(g, str, strlen(str));
		if (yajl_gen_status_ok != stat)
			GEN_SET_ERROR_AND_RETURN(stat, err);
	}
	if ((ctx->options & GEN_OPTIONS_ALLKEYVALUE) || (ptr != NULL && ptr->attrs != NULL && ptr->attrs_len)) {
		const char *str = "";
		size_t len = 0;
		stat = reformat_map_key(g, "attrs", strlen("attrs"));
		if (yajl_gen_status_ok != stat)
			GEN_SET_ERROR_AND_RETURN(stat, err);
		if (ptr != NULL && ptr->attrs != NULL) {
			str = (const char *)ptr->attrs;
			len = ptr->attrs_len;
		}
		stat = reformat_string(g, str, len);
		if (yajl_gen_status_ok != stat)
			GEN_SET_ERROR_AND_RETURN(stat, err);
	}
	stat = reformat_end_map(g);
	if (yajl_gen_status_ok != stat)
		GEN_SET_ERROR_AND_RETURN(stat, err);
	return yajl_gen_status_ok;
}


logger_json_file *logger_json_file_parse_file(const char *filename, struct parser_context *ctx, parser_error *err) {
	logger_json_file *ptr = NULL;
	size_t filesize;
	char *content = NULL;

	if (filename == NULL || err == NULL)
		return NULL;

	*err = NULL;
	content = read_file(filename, &filesize);
	if (content == NULL) {
		if (asprintf(err, "cannot read the file: %s", filename) < 0)
			*err = safe_strdup("error allocating memory");
		return NULL;
	}
	ptr = logger_json_file_parse_data(content, ctx, err);
	free(content);
	return ptr;
}

logger_json_file *logger_json_file_parse_file_stream(FILE *stream, struct parser_context *ctx, parser_error *err) {
	logger_json_file *ptr = NULL;
	size_t filesize;
	char *content = NULL ;

	if (stream == NULL || err == NULL)
		return NULL;

	*err = NULL;
	content = fread_file(stream, &filesize);
	if (content == NULL) {
		*err = safe_strdup("cannot read the file");
		return NULL;
	}
	ptr = logger_json_file_parse_data(content, ctx, err);
	free(content);
	return ptr;
}

logger_json_file *logger_json_file_parse_data(const char *jsondata, struct parser_context *ctx, parser_error *err) {
	logger_json_file *ptr  = NULL;
	yajl_val tree;
	char errbuf[1024];
	struct parser_context tmp_ctx;

	if (jsondata == NULL || err == NULL)
		return NULL;

	*err = NULL;
	if (ctx == NULL) {
		ctx = &tmp_ctx;
		memset(&tmp_ctx, 0, sizeof(tmp_ctx));
	}
	tree = yajl_tree_parse(jsondata, errbuf, sizeof(errbuf));
	if (tree == NULL) {
		if (asprintf(err, "cannot parse the data: %s", errbuf) < 0)
			*err = safe_strdup("error allocating memory");
		return NULL;
	}
	ptr = make_logger_json_file(tree, ctx, err);
	yajl_tree_free(tree);
	return ptr;
}
char *logger_json_file_generate_json(logger_json_file *ptr, struct parser_context *ctx, parser_error *err) {
	yajl_gen g = NULL;
	struct parser_context tmp_ctx;
	const unsigned char *gen_buf = NULL;
	char *json_buf = NULL;
	size_t gen_len = 0;

	if (ptr == NULL || err == NULL)
		return NULL;

	*err = NULL;
	if (ctx == NULL) {
		ctx = &tmp_ctx;
		memset(&tmp_ctx, 0, sizeof(tmp_ctx));
	}

	if (!json_gen_init(&g, ctx)) {
		*err = safe_strdup("Json_gen init failed");
		goto out;
	}
	if (yajl_gen_status_ok != gen_logger_json_file(g, ptr, ctx, err)) {
		if (*err == NULL)
			*err = safe_strdup("Failed to generate json");
		goto free_out;
	}
	yajl_gen_get_buf(g, &gen_buf, &gen_len);
	if (gen_buf == NULL) {
		*err = safe_strdup("Error to get generated json");
		goto free_out;
	}

	if (gen_len == SIZE_MAX) {
		*err = safe_strdup("Invalid buffer length");
		goto free_out;
	}
	json_buf = safe_malloc(gen_len + 1);
	(void)memcpy(json_buf, gen_buf, gen_len);
	json_buf[gen_len] = '\0';

free_out:
	yajl_gen_clear(g);
	yajl_gen_free(g);
out:
	return json_buf;
}
