// Generated from spec.json. Do not edit!
#ifndef OCI_RUNTIME_SPEC_SCHEMA_H
#define OCI_RUNTIME_SPEC_SCHEMA_H

#include <sys/types.h>
#include <stdint.h>
#include "json_common.h"
#include "defs.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    defs_hook **prestart;
    size_t prestart_len;

    defs_hook **poststart;
    size_t poststart_len;

    defs_hook **poststop;
    size_t poststop_len;

}
oci_runtime_spec_hooks;

void free_oci_runtime_spec_hooks(oci_runtime_spec_hooks *ptr);

oci_runtime_spec_hooks *make_oci_runtime_spec_hooks(yajl_val tree, struct parser_context *ctx, parser_error *err);

yajl_gen_status gen_oci_runtime_spec_hooks(yajl_gen g, oci_runtime_spec_hooks *ptr, struct parser_context *ctx, parser_error *err);

#ifdef __cplusplus
}
#endif

#endif
