/******************************************************************************
 * Copyright (C), 1988-1999, Huawei Tech. Co., Ltd.
 * FileName: oci_runtime_hooks.c
 * Author: maoweiyong   Version: 0.1   Date: 2018-11-07
 * Explanation: provide oci runtime hooks functions
 ******************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <read-file.h>
#include "oci_runtime_hooks.h"

#include "log.h"
#include "utils.h"

#define PARSE_ERR_BUFFER_SIZE 1024

oci_runtime_spec_hooks *oci_runtime_spec_hooks_parse_file(const char *filename,
                                                          struct parser_context *ctx, parser_error *err)
{
    yajl_val tree;
    size_t filesize;

    if (!filename || !err) {
        return NULL;
    }
    *err = NULL;
    struct parser_context tmp_ctx;
    if (!ctx) {
        ctx = &tmp_ctx;
        memset(&tmp_ctx, 0, sizeof(tmp_ctx));
    }
    char *content = read_file(filename, &filesize);
    char errbuf[PARSE_ERR_BUFFER_SIZE];
    if (content == NULL) {
        if (asprintf(err, "cannot read the file: %s", filename) < 0) {
            *err = safe_strdup("error allocating memory");
        }
        return NULL;
    }
    tree = yajl_tree_parse(content, errbuf, sizeof(errbuf));
    free(content);
    if (tree == NULL) {
        if (asprintf(err, "cannot parse the file: %s", errbuf) < 0) {
            *err = safe_strdup("error allocating memory");
        }
        return NULL;
    }
    oci_runtime_spec_hooks *ptr = make_oci_runtime_spec_hooks(tree, ctx, err);
    yajl_tree_free(tree);
    return ptr;
}
