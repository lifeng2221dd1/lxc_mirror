// Auto generated file. Do not edit!
#ifndef _JSON_COMMON_H
#define _JSON_COMMON_H

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <yajl/yajl_tree.h>
#include <yajl/yajl_gen.h>
#include "utils.h"

#ifdef __cplusplus
extern "C" {
#endif

# undef linux

//options to report error if there is unknown key found in json
# define PARSE_OPTIONS_STRICT 0x01
//options to generate all key and value
# define GEN_OPTIONS_ALLKEYVALUE 0x02
//options to generate simplify(no indent) json string
# define GEN_OPTIONS_SIMPLIFY 0x04
//options not to validate utf8 data
# define GEN_OPTIONS_NOT_VALIDATE_UTF8 0x08

#define GEN_SET_ERROR_AND_RETURN(stat, err) { \
        if (*(err) == NULL) {\
            if (asprintf(err, "%s: %s: %d: error generating json, errcode: %d", __FILE__, __func__, __LINE__, stat) < 0) { \
                *(err) = safe_strdup("error allocating memory"); \
            } \
        }\
        return stat; \
    }

typedef char *parser_error;

struct parser_context {
    unsigned int options;
    FILE *stderr;
};

yajl_gen_status reformat_number(void *ctx, const char *str, size_t len);

yajl_gen_status reformat_uint(void *ctx, long long unsigned int num);

yajl_gen_status reformat_int(void *ctx, long long int num);

yajl_gen_status reformat_double(void *ctx, double num);

yajl_gen_status reformat_string(void *ctx, const char *str, size_t len);

yajl_gen_status reformat_null(void *ctx);

yajl_gen_status reformat_bool(void *ctx, int boolean);

yajl_gen_status reformat_map_key(void *ctx, const char *str, size_t len);

yajl_gen_status reformat_start_map(void *ctx);

yajl_gen_status reformat_end_map(void *ctx);

yajl_gen_status reformat_start_array(void *ctx);

yajl_gen_status reformat_end_array(void *ctx);

bool json_gen_init(yajl_gen *g, struct parser_context *ctx);

yajl_val get_val(yajl_val tree, const char *name, yajl_type type);

void *safe_malloc(size_t size);

int common_safe_double(const char *numstr, double *converted);

int common_safe_uint8(const char *numstr, uint8_t *converted);

int common_safe_uint16(const char *numstr, uint16_t *converted);

int common_safe_uint32(const char *numstr, uint32_t *converted);

int common_safe_uint64(const char *numstr, uint64_t *converted);

int common_safe_uint(const char *numstr, unsigned int *converted);

int common_safe_int8(const char *numstr, int8_t *converted);

int common_safe_int16(const char *numstr, int16_t *converted);

int common_safe_int32(const char *numstr, int32_t *converted);

int common_safe_int64(const char *numstr, int64_t *converted);

int common_safe_int(const char *numstr, int *converted);

typedef struct {
    int *keys;
    int *values;
    size_t len;
} json_map_int_int;

void free_json_map_int_int(json_map_int_int *map);

json_map_int_int *make_json_map_int_int(yajl_val src, struct parser_context *ctx, parser_error *err);

yajl_gen_status gen_json_map_int_int(void *ctx, json_map_int_int *map, struct parser_context *ptx, parser_error *err);

int append_json_map_int_int(json_map_int_int *map, int key, int val);

typedef struct {
    int *keys;
    bool *values;
    size_t len;
} json_map_int_bool;

void free_json_map_int_bool(json_map_int_bool *map);

json_map_int_bool *make_json_map_int_bool(yajl_val src, struct parser_context *ctx, parser_error *err);

yajl_gen_status gen_json_map_int_bool(void *ctx, json_map_int_bool *map, struct parser_context *ptx, parser_error *err);

int append_json_map_int_bool(json_map_int_bool *map, int key, bool val);

typedef struct {
    int *keys;
    char **values;
    size_t len;
} json_map_int_string;

void free_json_map_int_string(json_map_int_string *map);

json_map_int_string *make_json_map_int_string(yajl_val src, struct parser_context *ctx, parser_error *err);

yajl_gen_status gen_json_map_int_string(void *ctx, json_map_int_string *map, struct parser_context *ptx, parser_error *err);

int append_json_map_int_string(json_map_int_string *map, int key, const char *val);

typedef struct {
    char **keys;
    int *values;
    size_t len;
} json_map_string_int;

void free_json_map_string_int(json_map_string_int *map);

json_map_string_int *make_json_map_string_int(yajl_val src, struct parser_context *ctx, parser_error *err);

yajl_gen_status gen_json_map_string_int(void *ctx, json_map_string_int *map, struct parser_context *ptx, parser_error *err);

int append_json_map_string_int(json_map_string_int *map, const char *key, int val);

typedef struct {
    char **keys;
    bool *values;
    size_t len;
} json_map_string_bool;

void free_json_map_string_bool(json_map_string_bool *map);

json_map_string_bool *make_json_map_string_bool(yajl_val src, struct parser_context *ctx, parser_error *err);

yajl_gen_status gen_json_map_string_bool(void *ctx, json_map_string_bool *map, struct parser_context *ptx, parser_error *err);

int append_json_map_string_bool(json_map_string_bool *map, const char *key, bool val);

typedef struct {
    char **keys;
    char **values;
    size_t len;
} json_map_string_string;

void free_json_map_string_string(json_map_string_string *map);

json_map_string_string *make_json_map_string_string(yajl_val src, struct parser_context *ctx, parser_error *err);

yajl_gen_status gen_json_map_string_string(void *ctx, json_map_string_string *map, struct parser_context *ptx, parser_error *err);

int append_json_map_string_string(json_map_string_string *map, const char *key, const char *val);

#ifdef __cplusplus
}
#endif

#endif