#ifndef READ_FILE_H
#define READ_FILE_H

#include <stddef.h>
#include <stdio.h>

extern char *fread_file(FILE *stream, size_t *length);

extern char *read_file(const char *path, size_t *length);

#endif
