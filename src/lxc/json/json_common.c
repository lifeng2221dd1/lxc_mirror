// Auto generated file. Do not edit!
#define _GNU_SOURCE
#include <stdio.h>
#include <errno.h>
#include <limits.h>
#include "json_common.h"

#define MAX_NUM_STR_LEN 21

yajl_gen_status reformat_number(void *ctx, const char *str, size_t len) {
    yajl_gen g = (yajl_gen) ctx;
    return yajl_gen_number(g, str, len);
}

yajl_gen_status reformat_uint(void *ctx, long long unsigned int num) {
    char numstr[MAX_NUM_STR_LEN];
    int ret;

    ret = snprintf(numstr, MAX_NUM_STR_LEN, "%llu", num);
    if (ret < 0 || ret >= MAX_NUM_STR_LEN) {
        return yajl_gen_in_error_state;
    }
    return reformat_number(ctx, (const char *)numstr, strlen(numstr));
}

yajl_gen_status reformat_int(void *ctx, long long int num) {
    char numstr[MAX_NUM_STR_LEN];
    int ret;

    ret = snprintf(numstr, MAX_NUM_STR_LEN, "%lld", num);
    if (ret < 0 || ret >= MAX_NUM_STR_LEN) {
        return yajl_gen_in_error_state;
    }
    return reformat_number(ctx, (const char *)numstr, strlen(numstr));
}

yajl_gen_status reformat_double(void *ctx, double num) {
    yajl_gen g = (yajl_gen) ctx;
    return yajl_gen_double(g, num);
}

yajl_gen_status reformat_string(void *ctx, const char *str, size_t len) {
    yajl_gen g = (yajl_gen) ctx;
    return yajl_gen_string(g, (const unsigned char *)str, len);
}

yajl_gen_status reformat_null(void *ctx) {
    yajl_gen g = (yajl_gen) ctx;
    return yajl_gen_null(g);
}

yajl_gen_status reformat_bool(void *ctx, int boolean) {
    yajl_gen g = (yajl_gen) ctx;
    return yajl_gen_bool(g, boolean);
}

yajl_gen_status reformat_map_key(void *ctx, const char *str, size_t len) {
    yajl_gen g = (yajl_gen) ctx;
    return yajl_gen_string(g, (const unsigned char *)str, len);
}

yajl_gen_status reformat_start_map(void *ctx) {
    yajl_gen g = (yajl_gen) ctx;
    return yajl_gen_map_open(g);
}

yajl_gen_status reformat_end_map(void *ctx) {
    yajl_gen g = (yajl_gen) ctx;
    return yajl_gen_map_close(g);
}

yajl_gen_status reformat_start_array(void *ctx) {
    yajl_gen g = (yajl_gen) ctx;
    return yajl_gen_array_open(g);
}

yajl_gen_status reformat_end_array(void *ctx) {
    yajl_gen g = (yajl_gen) ctx;
    return yajl_gen_array_close(g);
}

bool json_gen_init(yajl_gen *g, struct parser_context *ctx) {
    *g = yajl_gen_alloc(NULL);
    if (NULL == *g) {
        return false;

    }
    yajl_gen_config(*g, yajl_gen_beautify, !(ctx->options & GEN_OPTIONS_SIMPLIFY));
    yajl_gen_config(*g, yajl_gen_validate_utf8, !(ctx->options & GEN_OPTIONS_NOT_VALIDATE_UTF8));
    return true;
}

yajl_val get_val(yajl_val tree, const char *name, yajl_type type) {
    const char *path[] = { name, NULL };
    return yajl_tree_get(tree, path, type);
}

void *safe_malloc(size_t size) {
    void *ret = NULL;
    if (size == 0) {
        abort();
    }
    ret = calloc(1, size);
    if (ret == NULL) {
        abort();
    }
    return ret;
}

int common_safe_double(const char *numstr, double *converted) {
    char *err_str = NULL;
    double d;

    if (numstr == NULL) {
        return -EINVAL;
    }

    errno = 0;
    d = strtod(numstr, &err_str);
    if (errno > 0) {
        return -errno;
    }

    if (err_str == NULL || err_str == numstr || *err_str != '\0') {
        return -EINVAL;
    }

    *converted = d;
    return 0;
}

int common_safe_uint8(const char *numstr, uint8_t *converted) {
    char *err = NULL;
    unsigned long int uli;

    if (numstr == NULL) {
        return -EINVAL;
    }

    errno = 0;
    uli = strtoul(numstr, &err, 0);
    if (errno > 0) {
        return -errno;
    }

    if (err == NULL || err == numstr || *err != '\0') {
        return -EINVAL;
    }

    if (uli > UINT8_MAX) {
        return -ERANGE;
    }

    *converted = (uint8_t)uli;
    return 0;
}

int common_safe_uint16(const char *numstr, uint16_t *converted) {
    char *err = NULL;
    unsigned long int uli;

    if (numstr == NULL) {
        return -EINVAL;
    }

    errno = 0;
    uli = strtoul(numstr, &err, 0);
    if (errno > 0) {
        return -errno;
    }

    if (err == NULL || err == numstr || *err != '\0') {
        return -EINVAL;
    }

    if (uli > UINT16_MAX) {
        return -ERANGE;
    }

    *converted = (uint16_t)uli;
    return 0;
}

int common_safe_uint32(const char *numstr, uint32_t *converted) {
    char *err = NULL;
    unsigned long long int ull;

    if (numstr == NULL) {
        return -EINVAL;
    }

    errno = 0;
    ull = strtoull(numstr, &err, 0);
    if (errno > 0) {
        return -errno;
    }

    if (err == NULL || err == numstr || *err != '\0') {
        return -EINVAL;
    }

    if (ull > UINT32_MAX) {
        return -ERANGE;
    }

    *converted = (uint32_t)ull;
    return 0;
}

int common_safe_uint64(const char *numstr, uint64_t *converted) {
    char *err = NULL;
    unsigned long long int ull;

    if (numstr == NULL) {
        return -EINVAL;
    }

    errno = 0;
    ull = strtoull(numstr, &err, 0);
    if (errno > 0) {
        return -errno;
    }

    if (err == NULL || err == numstr || *err != '\0') {
        return -EINVAL;
    }

    *converted = (uint64_t)ull;
    return 0;
}

int common_safe_uint(const char *numstr, unsigned int *converted) {
    char *err = NULL;
    unsigned long long int ull;

    if (numstr == NULL) {
        return -EINVAL;
    }

    errno = 0;
    ull = strtoull(numstr, &err, 0);
    if (errno > 0) {
        return -errno;
    }

    if (err == NULL || err == numstr || *err != '\0') {
        return -EINVAL;
    }

    if (ull > UINT_MAX) {
        return -ERANGE;
    }

    *converted = (unsigned int)ull;
    return 0;
}

int common_safe_int8(const char *numstr, int8_t *converted) {
    char *err = NULL;
    long int li;

    if (numstr == NULL) {
        return -EINVAL;
    }

    errno = 0;
    li = strtol(numstr, &err, 0);
    if (errno > 0) {
        return -errno;
    }

    if (err == NULL || err == numstr || *err != '\0') {
        return -EINVAL;
    }

    if (li > INT8_MAX || li < INT8_MIN) {
        return -ERANGE;
    }

    *converted = (int8_t)li;
    return 0;
}

int common_safe_int16(const char *numstr, int16_t *converted) {
    char *err = NULL;
    long int li;

    if (numstr == NULL) {
        return -EINVAL;
    }

    errno = 0;
    li = strtol(numstr, &err, 0);
    if (errno > 0) {
        return -errno;
    }

    if (err == NULL || err == numstr || *err != '\0') {
        return -EINVAL;
    }

    if (li > INT16_MAX || li < INT16_MIN) {
        return -ERANGE;
    }

    *converted = (int16_t)li;
    return 0;
}

int common_safe_int32(const char *numstr, int32_t *converted) {
    char *err = NULL;
    long long int lli;

    if (numstr == NULL) {
        return -EINVAL;
    }

    errno = 0;
    lli = strtol(numstr, &err, 0);
    if (errno > 0) {
        return -errno;
    }

    if (err == NULL || err == numstr || *err != '\0') {
        return -EINVAL;
    }

    if (lli > INT32_MAX || lli < INT32_MIN) {
        return -ERANGE;
    }

    *converted = (int32_t)lli;
    return 0;
}

int common_safe_int64(const char *numstr, int64_t *converted) {
    char *err = NULL;
    long long int lli;

    if (numstr == NULL) {
        return -EINVAL;
    }

    errno = 0;
    lli = strtoll(numstr, &err, 0);
    if (errno > 0) {
        return -errno;
    }

    if (err == NULL || err == numstr || *err != '\0') {
        return -EINVAL;
    }

    *converted = (int64_t)lli;
    return 0;
}

int common_safe_int(const char *numstr, int *converted) {
    char *err = NULL;
    long long int lli;

    if (numstr == NULL) {
        return -EINVAL;
    }

    errno = 0;
    lli = strtol(numstr, &err, 0);
    if (errno > 0) {
        return -errno;
    }

    if (err == NULL || err == numstr || *err != '\0') {
        return -EINVAL;
    }

    if (lli > INT_MAX || lli < INT_MIN) {
        return -ERANGE;
    }

    *converted = (int)lli;
    return 0;
}

yajl_gen_status gen_json_map_int_int(void *ctx, json_map_int_int *map, struct parser_context *ptx, parser_error *err) {
    yajl_gen_status stat = yajl_gen_status_ok;
    yajl_gen g = (yajl_gen) ctx;
    size_t len = 0, i = 0;
    if (map != NULL) {
        len = map->len;
    }
    if (!len && !(ptx->options & GEN_OPTIONS_SIMPLIFY)) {
        yajl_gen_config(g, yajl_gen_beautify, 0);
    }
    stat = reformat_start_map(g);
    if (yajl_gen_status_ok != stat) {
        GEN_SET_ERROR_AND_RETURN(stat, err);

    }
    for (i = 0; i < len; i++) {
        char numstr[MAX_NUM_STR_LEN];
        int nret;
        nret = snprintf(numstr, MAX_NUM_STR_LEN, "%lld", (long long int)map->keys[i]);
        if (nret < 0 || nret >= MAX_NUM_STR_LEN) {
            if (!*err && asprintf(err, "Error to print string") < 0) {
                *(err) = safe_strdup("error allocating memory");
            }
            return yajl_gen_in_error_state;
        }
        stat = reformat_string(g, numstr, strlen(numstr));
        if (yajl_gen_status_ok != stat) {
            GEN_SET_ERROR_AND_RETURN(stat, err);
        }
        stat = reformat_int(g, map->values[i]);
        if (yajl_gen_status_ok != stat) {
            GEN_SET_ERROR_AND_RETURN(stat, err);
        }
    }

    stat = reformat_end_map(g);
    if (yajl_gen_status_ok != stat) {
        GEN_SET_ERROR_AND_RETURN(stat, err);
    }
    if (!len && !(ptx->options & GEN_OPTIONS_SIMPLIFY)) {
        yajl_gen_config(g, yajl_gen_beautify, 1);
    }
    return yajl_gen_status_ok;
}

void free_json_map_int_int(json_map_int_int *map) {
    if (map != NULL) {
        size_t i;
        for (i = 0; i < map->len; i++) {
            // No need to free key for type int
            // No need to free value for type int
        }
        free(map->keys);
        map->keys = NULL;
        free(map->values);
        map->values = NULL;
        free(map);
    }
}
json_map_int_int *make_json_map_int_int(yajl_val src, struct parser_context *ctx, parser_error *err) {
    json_map_int_int *ret = NULL;
    if (src != NULL && YAJL_GET_OBJECT(src) != NULL) {
        size_t i;
        size_t len = YAJL_GET_OBJECT(src)->len;
        if (len > SIZE_MAX / sizeof(int) - 1) {
            return NULL;
        }
        ret = safe_malloc(sizeof(*ret));
        ret->len = len;
        ret->keys = safe_malloc((len + 1) * sizeof(int));
        ret->values = safe_malloc((len + 1) * sizeof(int));
        for (i = 0; i < len; i++) {
            const char *srckey = YAJL_GET_OBJECT(src)->keys[i];
            yajl_val srcval = YAJL_GET_OBJECT(src)->values[i];

            if (srckey != NULL) {
                int invalid;
                invalid = common_safe_int(srckey, &(ret->keys[i]));
                if (invalid) {
                    if (*err == NULL && asprintf(err, "Invalid key '%s' with type 'int': %s", srckey, strerror(-invalid)) < 0) {
                        *(err) = safe_strdup("error allocating memory");
                    }
                    free_json_map_int_int(ret);
                    return NULL;
                }
            }

            if (srcval != NULL) {
                int invalid;
                if (!YAJL_IS_NUMBER(srcval)) {
                    if (*err == NULL && asprintf(err, "Invalid value with type 'int' for key '%s'", srckey) < 0) {
                        *(err) = safe_strdup("error allocating memory");
                    }
                    free_json_map_int_int(ret);
                    return NULL;
                }
                invalid = common_safe_int(YAJL_GET_NUMBER(srcval), &(ret->values[i]));
                if (invalid) {
                    if (*err == NULL && asprintf(err, "Invalid value with type 'int' for key '%s': %s", srckey, strerror(-invalid)) < 0) {
                        *(err) = safe_strdup("error allocating memory");
                    }
                    free_json_map_int_int(ret);
                    return NULL;
                }
            }
        }
    }
    return ret;
}
int append_json_map_int_int(json_map_int_int *map, int key, int val) {
    size_t len;
    int *keys = NULL;
    int *vals = NULL;

    if (map == NULL) {
        return -1;
    }

    if ((SIZE_MAX / sizeof(int) - 1) < map->len) {
        return -1;
    }

    len = map->len + 1;
    keys = safe_malloc(len * sizeof(int));
    vals = safe_malloc(len * sizeof(int));

    if (map->len) {
        (void)memcpy(keys, map->keys, map->len * sizeof(int));
        (void)memcpy(vals, map->values, map->len * sizeof(int));
    }
    free(map->keys);
    map->keys = keys;
    free(map->values);
    map->values = vals;
    map->keys[map->len] = key;
    map->values[map->len] = val;

    map->len++;
    return 0;
}

yajl_gen_status gen_json_map_int_bool(void *ctx, json_map_int_bool *map, struct parser_context *ptx, parser_error *err) {
    yajl_gen_status stat = yajl_gen_status_ok;
    yajl_gen g = (yajl_gen) ctx;
    size_t len = 0, i = 0;
    if (map != NULL) {
        len = map->len;
    }
    if (!len && !(ptx->options & GEN_OPTIONS_SIMPLIFY)) {
        yajl_gen_config(g, yajl_gen_beautify, 0);
    }
    stat = reformat_start_map(g);
    if (yajl_gen_status_ok != stat) {
        GEN_SET_ERROR_AND_RETURN(stat, err);

    }
    for (i = 0; i < len; i++) {
        char numstr[MAX_NUM_STR_LEN];
        int nret;
        nret = snprintf(numstr, MAX_NUM_STR_LEN, "%lld", (long long int)map->keys[i]);
        if (nret < 0 || nret >= MAX_NUM_STR_LEN) {
            if (!*err && asprintf(err, "Error to print string") < 0) {
                *(err) = safe_strdup("error allocating memory");
            }
            return yajl_gen_in_error_state;
        }
        stat = reformat_string(g, numstr, strlen(numstr));
        if (yajl_gen_status_ok != stat) {
            GEN_SET_ERROR_AND_RETURN(stat, err);
        }
        stat = reformat_bool(g, map->values[i]);
        if (yajl_gen_status_ok != stat) {
            GEN_SET_ERROR_AND_RETURN(stat, err);
        }
    }

    stat = reformat_end_map(g);
    if (yajl_gen_status_ok != stat) {
        GEN_SET_ERROR_AND_RETURN(stat, err);
    }
    if (!len && !(ptx->options & GEN_OPTIONS_SIMPLIFY)) {
        yajl_gen_config(g, yajl_gen_beautify, 1);
    }
    return yajl_gen_status_ok;
}

void free_json_map_int_bool(json_map_int_bool *map) {
    if (map != NULL) {
        free(map->keys);
        map->keys = NULL;
        free(map->values);
        map->values = NULL;
        free(map);
    }
}
json_map_int_bool *make_json_map_int_bool(yajl_val src, struct parser_context *ctx, parser_error *err) {
    json_map_int_bool *ret = NULL;
    if (src != NULL && YAJL_GET_OBJECT(src) != NULL) {
        size_t i;
        size_t len = YAJL_GET_OBJECT(src)->len;
        if (len > SIZE_MAX / sizeof(int) - 1) {
            return NULL;
        }
        ret = safe_malloc(sizeof(*ret));
        ret->len = len;
        ret->keys = safe_malloc((len + 1) * sizeof(int));
        ret->values = safe_malloc((len + 1) * sizeof(bool));
        for (i = 0; i < len; i++) {
            const char *srckey = YAJL_GET_OBJECT(src)->keys[i];
            yajl_val srcval = YAJL_GET_OBJECT(src)->values[i];

            if (srckey != NULL) {
                int invalid;
                invalid = common_safe_int(srckey, &(ret->keys[i]));
                if (invalid) {
                    if (*err == NULL && asprintf(err, "Invalid key '%s' with type 'int': %s", srckey, strerror(-invalid)) < 0) {
                        *(err) = safe_strdup("error allocating memory");
                    }
                    free_json_map_int_bool(ret);
                    return NULL;
                }
            }

            if (srcval != NULL) {
                if (YAJL_IS_TRUE(srcval)) {
                    ret->values[i] = true;
                } else if (YAJL_IS_FALSE(srcval)) {
                    ret->values[i] = false;
                } else {
                    if (*err == NULL && asprintf(err, "Invalid value with type 'bool' for key '%s'", srckey) < 0) {
                        *(err) = safe_strdup("error allocating memory");
                    }
                    free_json_map_int_bool(ret);
                    return NULL;
                }
            }
        }
    }
    return ret;
}
int append_json_map_int_bool(json_map_int_bool *map, int key, bool val) {
    size_t len;
    int *keys = NULL;
    bool *vals = NULL;

    if (map == NULL) {
        return -1;
    }

    if ((SIZE_MAX / sizeof(int) - 1) < map->len || (SIZE_MAX / sizeof(bool) - 1) < map->len) {
        return -1;
    }

    len = map->len + 1;
    keys = safe_malloc(len * sizeof(int));
    vals = safe_malloc(len * sizeof(bool));

    if (map->len) {
        (void)memcpy(keys, map->keys, map->len * sizeof(int));
        (void)memcpy(vals, map->values, map->len * sizeof(bool));
    }
    free(map->keys);
    map->keys = keys;
    free(map->values);
    map->values = vals;
    map->keys[map->len] = key;
    map->values[map->len] = val;

    map->len++;
    return 0;
}

yajl_gen_status gen_json_map_int_string(void *ctx, json_map_int_string *map, struct parser_context *ptx, parser_error *err) {
    yajl_gen_status stat = yajl_gen_status_ok;
    yajl_gen g = (yajl_gen) ctx;
    size_t len = 0, i = 0;
    if (map != NULL) {
        len = map->len;
    }
    if (!len && !(ptx->options & GEN_OPTIONS_SIMPLIFY)) {
        yajl_gen_config(g, yajl_gen_beautify, 0);
    }
    stat = reformat_start_map(g);
    if (yajl_gen_status_ok != stat) {
        GEN_SET_ERROR_AND_RETURN(stat, err);

    }
    for (i = 0; i < len; i++) {
        char numstr[MAX_NUM_STR_LEN];
        int nret;
        nret = snprintf(numstr, MAX_NUM_STR_LEN, "%lld", (long long int)map->keys[i]);
        if (nret < 0 || nret >= MAX_NUM_STR_LEN) {
            if (!*err && asprintf(err, "Error to print string") < 0) {
                *(err) = safe_strdup("error allocating memory");
            }
            return yajl_gen_in_error_state;
        }
        stat = reformat_string(g, numstr, strlen(numstr));
        if (yajl_gen_status_ok != stat) {
            GEN_SET_ERROR_AND_RETURN(stat, err);
        }
        stat = reformat_string(g, map->values[i], strlen(map->values[i]));;
        if (yajl_gen_status_ok != stat) {
            GEN_SET_ERROR_AND_RETURN(stat, err);
        }
    }

    stat = reformat_end_map(g);
    if (yajl_gen_status_ok != stat) {
        GEN_SET_ERROR_AND_RETURN(stat, err);
    }
    if (!len && !(ptx->options & GEN_OPTIONS_SIMPLIFY)) {
        yajl_gen_config(g, yajl_gen_beautify, 1);
    }
    return yajl_gen_status_ok;
}

void free_json_map_int_string(json_map_int_string *map) {
    if (map != NULL) {
        size_t i;
        for (i = 0; i < map->len; i++) {
            // No need to free key for type int
            free(map->values[i]);
            map->values[i] = NULL;
        }
        free(map->keys);
        map->keys = NULL;
        free(map->values);
        map->values = NULL;
        free(map);
    }
}
json_map_int_string *make_json_map_int_string(yajl_val src, struct parser_context *ctx, parser_error *err) {
    json_map_int_string *ret = NULL;
    if (src != NULL && YAJL_GET_OBJECT(src) != NULL) {
        size_t i;
        size_t len = YAJL_GET_OBJECT(src)->len;
        if (len > SIZE_MAX / sizeof(char *) - 1) {
            return NULL;
        }
        ret = safe_malloc(sizeof(*ret));
        ret->len = len;
        ret->keys = safe_malloc((len + 1) * sizeof(int));
        ret->values = safe_malloc((len + 1) * sizeof(char *));
        for (i = 0; i < len; i++) {
            const char *srckey = YAJL_GET_OBJECT(src)->keys[i];
            yajl_val srcval = YAJL_GET_OBJECT(src)->values[i];

            if (srckey != NULL) {
                int invalid;
                invalid = common_safe_int(srckey, &(ret->keys[i]));
                if (invalid) {
                    if (*err == NULL && asprintf(err, "Invalid key '%s' with type 'int': %s", srckey, strerror(-invalid)) < 0) {
                        *(err) = safe_strdup("error allocating memory");
                    }
                    free_json_map_int_string(ret);
                    return NULL;
                }
            }

            if (srcval != NULL) {
                if (!YAJL_IS_STRING(srcval)) {
                    if (*err == NULL && asprintf(err, "Invalid value with type 'string' for key '%s'", srckey) < 0) {
                        *(err) = safe_strdup("error allocating memory");
                    }
                    free_json_map_int_string(ret);
                    return NULL;
                }
                char *str = YAJL_GET_STRING(srcval);
                ret->values[i] = safe_strdup(str ? str : "");
            }
        }
    }
    return ret;
}
int append_json_map_int_string(json_map_int_string *map, int key, const char *val) {
    size_t len;
    int *keys = NULL;
    char **vals = NULL;

    if (map == NULL) {
        return -1;
    }

    if ((SIZE_MAX / sizeof(int) - 1) < map->len || (SIZE_MAX / sizeof(char *) - 1) < map->len) {
        return -1;
    }

    len = map->len + 1;
    keys = safe_malloc(len * sizeof(int));
    vals = safe_malloc(len * sizeof(char *));

    if (map->len) {
        (void)memcpy(keys, map->keys, map->len * sizeof(int));
        (void)memcpy(vals, map->values, map->len * sizeof(char *));
    }
    free(map->keys);
    map->keys = keys;
    free(map->values);
    map->values = vals;
    map->keys[map->len] = key;
    map->values[map->len] = safe_strdup(val ? val : "");

    map->len++;
    return 0;
}

yajl_gen_status gen_json_map_string_int(void *ctx, json_map_string_int *map, struct parser_context *ptx, parser_error *err) {
    yajl_gen_status stat = yajl_gen_status_ok;
    yajl_gen g = (yajl_gen) ctx;
    size_t len = 0, i = 0;
    if (map != NULL) {
        len = map->len;
    }
    if (!len && !(ptx->options & GEN_OPTIONS_SIMPLIFY)) {
        yajl_gen_config(g, yajl_gen_beautify, 0);
    }
    stat = reformat_start_map(g);
    if (yajl_gen_status_ok != stat) {
        GEN_SET_ERROR_AND_RETURN(stat, err);

    }
    for (i = 0; i < len; i++) {
        stat = reformat_string(g, map->keys[i], strlen(map->keys[i]));
        if (yajl_gen_status_ok != stat) {
            GEN_SET_ERROR_AND_RETURN(stat, err);
        }
        stat = reformat_int(g, map->values[i]);
        if (yajl_gen_status_ok != stat) {
            GEN_SET_ERROR_AND_RETURN(stat, err);
        }
    }

    stat = reformat_end_map(g);
    if (yajl_gen_status_ok != stat) {
        GEN_SET_ERROR_AND_RETURN(stat, err);
    }
    if (!len && !(ptx->options & GEN_OPTIONS_SIMPLIFY)) {
        yajl_gen_config(g, yajl_gen_beautify, 1);
    }
    return yajl_gen_status_ok;
}

void free_json_map_string_int(json_map_string_int *map) {
    if (map != NULL) {
        size_t i;
        for (i = 0; i < map->len; i++) {
            free(map->keys[i]);
            map->keys[i] = NULL;
            // No need to free value for type int
        }
        free(map->keys);
        map->keys = NULL;
        free(map->values);
        map->values = NULL;
        free(map);
    }
}
json_map_string_int *make_json_map_string_int(yajl_val src, struct parser_context *ctx, parser_error *err) {
    json_map_string_int *ret = NULL;
    if (src != NULL && YAJL_GET_OBJECT(src) != NULL) {
        size_t i;
        size_t len = YAJL_GET_OBJECT(src)->len;
        if (len > SIZE_MAX / sizeof(char *) - 1) {
            return NULL;
        }
        ret = safe_malloc(sizeof(*ret));
        ret->len = len;
        ret->keys = safe_malloc((len + 1) * sizeof(char *));
        ret->values = safe_malloc((len + 1) * sizeof(int));
        for (i = 0; i < len; i++) {
            const char *srckey = YAJL_GET_OBJECT(src)->keys[i];
            yajl_val srcval = YAJL_GET_OBJECT(src)->values[i];
            ret->keys[i] = safe_strdup(srckey ? srckey : "");

            if (srcval != NULL) {
                int invalid;
                if (!YAJL_IS_NUMBER(srcval)) {
                    if (*err == NULL && asprintf(err, "Invalid value with type 'int' for key '%s'", srckey) < 0) {
                        *(err) = safe_strdup("error allocating memory");
                    }
                    free_json_map_string_int(ret);
                    return NULL;
                }
                invalid = common_safe_int(YAJL_GET_NUMBER(srcval), &(ret->values[i]));
                if (invalid) {
                    if (*err == NULL && asprintf(err, "Invalid value with type 'int' for key '%s': %s", srckey, strerror(-invalid)) < 0) {
                        *(err) = safe_strdup("error allocating memory");
                    }
                    free_json_map_string_int(ret);
                    return NULL;
                }
            }
        }
    }
    return ret;
}
int append_json_map_string_int(json_map_string_int *map, const char *key, int val) {
    size_t len;
    char **keys = NULL;
    int *vals = NULL;

    if (map == NULL) {
        return -1;
    }

    if ((SIZE_MAX / sizeof(char *) - 1) < map->len || (SIZE_MAX / sizeof(int) - 1) < map->len) {
        return -1;
    }

    len = map->len + 1;
    keys = safe_malloc(len * sizeof(char *));
    vals = safe_malloc(len * sizeof(int));

    if (map->len) {
        (void)memcpy(keys, map->keys, map->len * sizeof(char *));
        (void)memcpy(vals, map->values, map->len * sizeof(int));
    }
    free(map->keys);
    map->keys = keys;
    free(map->values);
    map->values = vals;
    map->keys[map->len] = safe_strdup(key ? key : "");
    map->values[map->len] = val;

    map->len++;
    return 0;
}

yajl_gen_status gen_json_map_string_bool(void *ctx, json_map_string_bool *map, struct parser_context *ptx, parser_error *err) {
    yajl_gen_status stat = yajl_gen_status_ok;
    yajl_gen g = (yajl_gen) ctx;
    size_t len = 0, i = 0;
    if (map != NULL) {
        len = map->len;
    }
    if (!len && !(ptx->options & GEN_OPTIONS_SIMPLIFY)) {
        yajl_gen_config(g, yajl_gen_beautify, 0);
    }
    stat = reformat_start_map(g);
    if (yajl_gen_status_ok != stat) {
        GEN_SET_ERROR_AND_RETURN(stat, err);

    }
    for (i = 0; i < len; i++) {
        stat = reformat_string(g, map->keys[i], strlen(map->keys[i]));
        if (yajl_gen_status_ok != stat) {
            GEN_SET_ERROR_AND_RETURN(stat, err);
        }
        stat = reformat_bool(g, map->values[i]);
        if (yajl_gen_status_ok != stat) {
            GEN_SET_ERROR_AND_RETURN(stat, err);
        }
    }

    stat = reformat_end_map(g);
    if (yajl_gen_status_ok != stat) {
        GEN_SET_ERROR_AND_RETURN(stat, err);
    }
    if (!len && !(ptx->options & GEN_OPTIONS_SIMPLIFY)) {
        yajl_gen_config(g, yajl_gen_beautify, 1);
    }
    return yajl_gen_status_ok;
}

void free_json_map_string_bool(json_map_string_bool *map) {
    if (map != NULL) {
        size_t i;
        for (i = 0; i < map->len; i++) {
            free(map->keys[i]);
            map->keys[i] = NULL;
            // No need to free value for type bool
        }
        free(map->keys);
        map->keys = NULL;
        free(map->values);
        map->values = NULL;
        free(map);
    }
}
json_map_string_bool *make_json_map_string_bool(yajl_val src, struct parser_context *ctx, parser_error *err) {
    json_map_string_bool *ret = NULL;
    if (src != NULL && YAJL_GET_OBJECT(src) != NULL) {
        size_t i;
        size_t len = YAJL_GET_OBJECT(src)->len;
        if (len > SIZE_MAX / sizeof(char *) - 1) {
            return NULL;
        }
        ret = safe_malloc(sizeof(*ret));
        ret->len = len;
        ret->keys = safe_malloc((len + 1) * sizeof(char *));
        ret->values = safe_malloc((len + 1) * sizeof(bool));
        for (i = 0; i < len; i++) {
            const char *srckey = YAJL_GET_OBJECT(src)->keys[i];
            yajl_val srcval = YAJL_GET_OBJECT(src)->values[i];
            ret->keys[i] = safe_strdup(srckey ? srckey : "");

            if (srcval != NULL) {
                if (YAJL_IS_TRUE(srcval)) {
                    ret->values[i] = true;
                } else if (YAJL_IS_FALSE(srcval)) {
                    ret->values[i] = false;
                } else {
                    if (*err == NULL && asprintf(err, "Invalid value with type 'bool' for key '%s'", srckey) < 0) {
                        *(err) = safe_strdup("error allocating memory");
                    }
                    free_json_map_string_bool(ret);
                    return NULL;
                }
            }
        }
    }
    return ret;
}

int append_json_map_string_bool(json_map_string_bool *map, const char *key, bool val) {
    size_t len;
    char **keys = NULL;
    bool *vals = NULL;

    if (map == NULL) {
        return -1;
    }

    if ((SIZE_MAX / sizeof(char *) - 1) < map->len || (SIZE_MAX / sizeof(bool) - 1) < map->len) {
        return -1;
    }

    len = map->len + 1;
    keys = safe_malloc(len * sizeof(char *));
    vals = safe_malloc(len * sizeof(bool));

    if (map->len) {
        (void)memcpy(keys, map->keys, map->len * sizeof(char *));
        (void)memcpy(vals, map->values, map->len * sizeof(bool));
    }
    free(map->keys);
    map->keys = keys;
    free(map->values);
    map->values = vals;
    map->keys[map->len] = safe_strdup(key ? key : "");
    map->values[map->len] = val;

    map->len++;
    return 0;
}

yajl_gen_status gen_json_map_string_string(void *ctx, json_map_string_string *map, struct parser_context *ptx, parser_error *err) {
    yajl_gen_status stat = yajl_gen_status_ok;
    yajl_gen g = (yajl_gen) ctx;
    size_t len = 0, i = 0;
    if (map != NULL) {
        len = map->len;
    }
    if (!len && !(ptx->options & GEN_OPTIONS_SIMPLIFY)) {
        yajl_gen_config(g, yajl_gen_beautify, 0);
    }
    stat = reformat_start_map(g);
    if (yajl_gen_status_ok != stat) {
        GEN_SET_ERROR_AND_RETURN(stat, err);

    }
    for (i = 0; i < len; i++) {
        stat = reformat_string(g, map->keys[i], strlen(map->keys[i]));
        if (yajl_gen_status_ok != stat) {
            GEN_SET_ERROR_AND_RETURN(stat, err);
        }
        stat = reformat_string(g, map->values[i], strlen(map->values[i]));;
        if (yajl_gen_status_ok != stat) {
            GEN_SET_ERROR_AND_RETURN(stat, err);
        }
    }

    stat = reformat_end_map(g);
    if (yajl_gen_status_ok != stat) {
        GEN_SET_ERROR_AND_RETURN(stat, err);
    }
    if (!len && !(ptx->options & GEN_OPTIONS_SIMPLIFY)) {
        yajl_gen_config(g, yajl_gen_beautify, 1);
    }
    return yajl_gen_status_ok;
}

void free_json_map_string_string(json_map_string_string *map) {
    if (map != NULL) {
        size_t i;
        for (i = 0; i < map->len; i++) {
            free(map->keys[i]);
            map->keys[i] = NULL;
            free(map->values[i]);
            map->values[i] = NULL;
        }
        free(map->keys);
        map->keys = NULL;
        free(map->values);
        map->values = NULL;
        free(map);
    }
}
json_map_string_string *make_json_map_string_string(yajl_val src, struct parser_context *ctx, parser_error *err) {
    json_map_string_string *ret = NULL;
    if (src != NULL && YAJL_GET_OBJECT(src) != NULL) {
        size_t i;
        size_t len = YAJL_GET_OBJECT(src)->len;
        if (len > SIZE_MAX / sizeof(char *) - 1) {
            return NULL;
        }
        ret = safe_malloc(sizeof(*ret));
        ret->len = len;
        ret->keys = safe_malloc((len + 1) * sizeof(char *));
        ret->values = safe_malloc((len + 1) * sizeof(char *));
        for (i = 0; i < len; i++) {
            const char *srckey = YAJL_GET_OBJECT(src)->keys[i];
            yajl_val srcval = YAJL_GET_OBJECT(src)->values[i];
            ret->keys[i] = safe_strdup(srckey ? srckey : "");

            if (srcval != NULL) {
                if (!YAJL_IS_STRING(srcval)) {
                    if (*err == NULL && asprintf(err, "Invalid value with type 'string' for key '%s'", srckey) < 0) {
                        *(err) = safe_strdup("error allocating memory");
                    }
                    free_json_map_string_string(ret);
                    return NULL;
                }
                char *str = YAJL_GET_STRING(srcval);
                ret->values[i] = safe_strdup(str ? str : "");
            }
        }
    }
    return ret;
}
int append_json_map_string_string(json_map_string_string *map, const char *key, const char *val) {
    size_t len, i;
    char **keys = NULL;
    char **vals = NULL;

    if (map == NULL) {
        return -1;
    }

    for (i = 0; i < map->len; i++) {
        if (strcmp(map->keys[i], key) == 0) {
            free(map->values[i]);
            map->values[i] = safe_strdup(val ? val : "");
            return 0;
        }
    }

    if ((SIZE_MAX / sizeof(char *) - 1) < map->len) {
        return -1;
    }

    len = map->len + 1;
    keys = safe_malloc(len * sizeof(char *));
    vals = safe_malloc(len * sizeof(char *));

    if (map->len) {
        (void)memcpy(keys, map->keys, map->len * sizeof(char *));
        (void)memcpy(vals, map->values, map->len * sizeof(char *));
    }
    free(map->keys);
    map->keys = keys;
    free(map->values);
    map->values = vals;
    map->keys[map->len] = safe_strdup(key ? key : "");
    map->values[map->len] = safe_strdup(val ? val : "");

    map->len++;
    return 0;
}
