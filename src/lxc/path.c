/* SPDX-License-Identifier: LGPL-2.1+ */
/******************************************************************************
 * Copyright (c) Huawei Technologies Co., Ltd. 2020. Allrights reserved
 * Description: isulad utils
 * Author: lifeng
 * Create: 2020-04-11
******************************************************************************/
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <libgen.h>

#include "path.h"
#include "log.h"
#include "isulad_utils.h"

lxc_log_define(lxc_path_ui, lxc);

#define ISSLASH(C) ((C) == '/')
#define IS_ABSOLUTE_FILE_NAME(F) (ISSLASH ((F)[0]))
#define IS_RELATIVE_FILE_NAME(F) (! IS_ABSOLUTE_FILE_NAME (F))

bool specify_current_dir(const char *path)
{
	char *basec = NULL, *bname = NULL;
	bool res = false;

	basec = safe_strdup(path);

	bname = basename(basec);
	if (bname == NULL) {
		free(basec);
		ERROR("Out of memory");
		return false;
	}
	res = !strcmp(bname, ".");
	free(basec);
	return res;
}

bool has_traling_path_separator(const char *path)
{
	return path && strlen(path) && (path[strlen(path) - 1] == '/');
}

// PreserveTrailingDotOrSeparator returns the given cleaned path
// and appends a trailing `/.` or `/` if its corresponding  original
// path ends with a trailing `/.` or `/`. If the cleaned
// path already ends in a `.` path segment, then another is not added. If the
// clean path already ends in a path separator, then another is not added.
char *preserve_trailing_dot_or_separator(const char *cleanedpath,
                const char *originalpath)
{
	char *respath = NULL;
	size_t len;

	if (strlen(cleanedpath) > (SIZE_MAX - 3)) {
		return NULL;
	}

	len = strlen(cleanedpath) + 3;
	respath = malloc(len);
	if (respath == NULL) {
		ERROR("Out of memory");
		return NULL;
	}
	memset(respath, 0x00, len);
	strcat(respath, cleanedpath);

	if (!specify_current_dir(cleanedpath) && specify_current_dir(originalpath)) {
		if (!has_traling_path_separator(respath))
			strcat(respath, "/");
		strcat(respath, ".");
	}

	if (!has_traling_path_separator(respath) &&
	                has_traling_path_separator(originalpath))
		strcat(respath, "/");

	return respath;
}


// Split splits path immediately following the final Separator,
// separating it into a directory and file name component.
// If there is no Separator in path, Split returns an empty dir
// and file set to path.
// The returned values have the property that path = dir+file.
bool filepath_split(const char *path, char **dir, char **base)
{
	ssize_t i;
	size_t len;

	len = strlen(path);
	if (len >= PATH_MAX) {
		ERROR("Invalid path");
		return false;
	}
	i = len - 1;
	while (i >= 0 && path[i] != '/')
		i--;

	*dir = malloc(i + 2);
	if (*dir == NULL) {
		ERROR("Out of memory");
		return false;
	}
	memcpy(*dir, path, i + 1);
	*(*dir + i + 1) = '\0';

	*base = safe_strdup(path + i + 1);

	return true;
}


static bool do_clean_path_continue(const char *endpos, const char *stpos, const char *respath, char **dst)
{
	if (endpos - stpos == 1 && stpos[0] == '.') {
		return true;
	} else if (endpos - stpos == 2 && stpos[0] == '.' && stpos[1] == '.') {
		char *dest = *dst;
		if (dest <= respath + 1) {
			return true;
		}
		for (--dest; dest > respath && !ISSLASH(dest[-1]); --dest) {
			*dst = dest;
			return true;
		}
		*dst = dest;
		return true;
	}
	return false;
}

int do_clean_path(const char *respath, const char *limit_respath,
                  const char *stpos, char **dst)
{
	char *dest = *dst;
	const char *endpos = NULL;

	for (endpos = stpos; *stpos; stpos = endpos) {
		while (ISSLASH(*stpos)) {
			++stpos;
		}

		for (endpos = stpos; *endpos && !ISSLASH(*endpos); ++endpos) {
		}

		if (endpos - stpos == 0) {
			break;
		} else if (do_clean_path_continue(endpos, stpos, respath, &dest)) {
			continue;
		}

		if (!ISSLASH(dest[-1])) {
			*dest++ = '/';
		}

		if (dest + (endpos - stpos) >= limit_respath) {
			ERROR("Path is too long");
			if (dest > respath + 1) {
				dest--;
			}
			*dest = '\0';
			return -1;
		}

		memcpy(dest, stpos, (size_t)(endpos - stpos));
		dest += endpos - stpos;
		*dest = '\0';
	}
	*dst = dest;
	return 0;
}

char *cleanpath(const char *path, char *realpath, size_t realpath_len)
{
	char *respath = NULL;
	char *dest = NULL;
	const char *stpos = NULL;
	const char *limit_respath = NULL;

	if (path == NULL || path[0] == '\0' || \
	                realpath == NULL || (realpath_len < PATH_MAX)) {
		return NULL;
	}

	respath = realpath;

	memset(respath, 0, realpath_len);
	limit_respath = respath + PATH_MAX;

	if (!IS_ABSOLUTE_FILE_NAME(path)) {
		if (!getcwd(respath, PATH_MAX)) {
			ERROR("Failed to getcwd");
			respath[0] = '\0';
			goto error;
		}
		dest = strchr(respath, '\0');
		if (dest == NULL) {
			ERROR("Failed to get the end of respath");
			goto error;
		}
		if (strlen(path) > (PATH_MAX - strlen(respath) - 1)) {
			ERROR("Path is too long");
			goto error;
		}
		strcat(respath, path);
		stpos = path;
	} else {
		dest = respath;
		*dest++ = '/';
		stpos = path;
	}

	if (do_clean_path(respath, limit_respath, stpos, &dest)) {
		goto error;
	}

	if (dest > respath + 1 && ISSLASH(dest[-1])) {
		--dest;
	}
	*dest = '\0';

	return respath;

error:
	return NULL;
}

static int do_path_realloc(const char *start, const char *end,
                           char **rpath, char **dest, const char **rpath_limit)
{
	long long dest_offset = *dest - *rpath;
	char *new_rpath = NULL;
	size_t new_size;
	int nret = 0;
	size_t gap = 0;

	if (*dest + (end - start) < *rpath_limit) {
		return 0;
	}

	gap = (size_t)(end - start) + 1;
	new_size = (size_t)(*rpath_limit - *rpath);
	if (new_size > SIZE_MAX - gap) {
		ERROR("Out of range!");
		return -1;
	}

	if (gap > PATH_MAX) {
		new_size += gap;
	} else {
		new_size += PATH_MAX;
	}
	nret = lxc_mem_realloc((void **)&new_rpath, new_size, *rpath, PATH_MAX);
	if (nret) {
		ERROR("Failed to realloc memory for files limit variables");
		return -1;
	}
	*rpath = new_rpath;
	*rpath_limit = *rpath + new_size;

	*dest = *rpath + dest_offset;

	return 0;
}

static int do_get_symlinks_copy_buf(const char *buf, const char *prefix, size_t prefix_len,
                                    char **rpath, char **dest)
{
	if (IS_ABSOLUTE_FILE_NAME(buf)) {
		if (prefix_len) {
			memcpy(*rpath, prefix, prefix_len);
		}
		*dest = *rpath + prefix_len;
		*(*dest)++ = '/';
	} else {
		if (*dest > *rpath + prefix_len + 1) {
			for (--(*dest); *dest > *rpath && !ISSLASH((*dest)[-1]); --(*dest)) {
				continue;
			}
		}
	}
	return 0;
}

static int do_get_symlinks(const char **fullpath, const char *prefix, size_t prefix_len,
                           char **rpath, char **dest, const char **end,
                           int *num_links, char **extra_buf)
{
	char *buf = NULL;
	size_t len;
	ssize_t n;
	int ret = -1;

	if (++(*num_links) > MAXSYMLINKS) {
		ERROR("Too many links in '%s'", *fullpath);
		goto out;
	}

	buf = lxc_common_calloc_s(PATH_MAX);
	if (buf == NULL) {
		ERROR("Out of memory");
		goto out;
	}

	n = readlink(*rpath, buf, PATH_MAX - 1);
	if (n < 0) {
		goto out;
	}
	buf[n] = '\0';

	if (*extra_buf == NULL) {
		*extra_buf = lxc_common_calloc_s(PATH_MAX);
		if (*extra_buf == NULL) {
			ERROR("Out of memory");
			goto out;
		}
	}

	len = strlen(*end);
	if (len >= PATH_MAX - n) {
		ERROR("Path is too long");
		goto out;
	}

	memmove(&(*extra_buf)[n], *end, len + 1);
	memcpy(*extra_buf, buf, (size_t)n);

	*fullpath = *end = *extra_buf;

	if (do_get_symlinks_copy_buf(buf, prefix, prefix_len, rpath, dest) != 0) {
		goto out;
	}

	ret = 0;
out:
	free(buf);
	return ret;
}

static bool do_eval_symlinks_in_scope_is_symlink(const char *path)
{
	struct stat st;

	if (lstat(path, &st) < 0) {
		return true;
	}

	if (!S_ISLNK(st.st_mode)) {
		return true;
	}
	return false;
}

static void do_eval_symlinks_skip_slash(const char **start, const char **end)
{
	while (ISSLASH(**start)) {
		++(*start);
	}

	for (*end = *start; **end && !ISSLASH(**end); ++(*end)) {
	}
}

static inline void skip_dest_traling_slash(char **dest, char **rpath, size_t prefix_len)
{
	if (*dest > *rpath + prefix_len + 1) {
		for (--(*dest); *dest > *rpath && !ISSLASH((*dest)[-1]); --(*dest)) {
			continue;
		}
	}
}

static inline bool is_current_char(const char c)
{
	return c == '.';
}

static inline bool is_specify_current(const char *end, const char *start)
{
	return (end - start == 1) && is_current_char(start[0]);
}

static inline bool is_specify_parent(const char *end, const char *start)
{
	return (end - start == 2) && is_current_char(start[0]) && is_current_char(start[1]);
}

static int do_eval_symlinks_in_scope(const char *fullpath, const char *prefix,
                                     size_t prefix_len,
                                     char **rpath, char **dest, const char *rpath_limit)
{
	const char *start = NULL;
	const char *end = NULL;
	char *extra_buf = NULL;
	int nret = 0;
	int num_links = 0;

	start = fullpath + prefix_len;
	for (end = start; *start; start = end) {
		do_eval_symlinks_skip_slash(&start, &end);
		if (end - start == 0) {
			break;
		} else if (is_specify_current(end, start)) {
			;
		} else if (is_specify_parent(end, start)) {
			skip_dest_traling_slash(dest, rpath, prefix_len);
		} else {
			if (!ISSLASH((*dest)[-1])) {
				*(*dest)++ = '/';
			}

			nret = do_path_realloc(start, end, rpath, dest, &rpath_limit);
			if (nret != 0) {
				nret = -1;
				goto out;
			}

			memcpy(*dest, start, (size_t)(end - start));
			*dest += end - start;
			**dest = '\0';

			if (do_eval_symlinks_in_scope_is_symlink(*rpath)) {
				continue;
			}

			nret = do_get_symlinks(&fullpath, prefix, prefix_len, rpath, dest, &end, &num_links, &extra_buf);
			if (nret != 0) {
				nret = -1;
				goto out;
			}
		}
	}
out:
	free(extra_buf);
	return nret;
}
static char *eval_symlinks_in_scope(const char *fullpath, const char *rootpath)
{
	char resroot[PATH_MAX] = {0};
	char *root = NULL;
	char *rpath = NULL;
	char *dest = NULL;
	char *prefix = NULL;
	const char *rpath_limit = NULL;
	size_t prefix_len;

	if (fullpath == NULL || rootpath == NULL) {
		return NULL;
	}

	root = cleanpath(rootpath, resroot, sizeof(resroot));
	if (root == NULL) {
		ERROR("Failed to get cleaned path");
		return NULL;
	}

	if (!strcmp(fullpath, root)) {
		return safe_strdup(fullpath);
	}

	if (strstr(fullpath, root) == NULL) {
		ERROR("Path '%s' is not in '%s'", fullpath, root);
		return NULL;
	}

	rpath = lxc_common_calloc_s(PATH_MAX);
	if (rpath == NULL) {
		ERROR("Out of memory");
		goto out;
	}
	rpath_limit = rpath + PATH_MAX;

	prefix = root;
	prefix_len = (size_t)strlen(prefix);
	if (!strcmp(prefix, "/")) {
		prefix_len = 0;
	}

	dest = rpath;
	if (prefix_len) {
		memcpy(rpath, prefix, prefix_len);
		dest += prefix_len;
	}
	*dest++ = '/';

	if (do_eval_symlinks_in_scope(fullpath, prefix, prefix_len, &rpath, &dest,
	                              rpath_limit)) {
		goto out;
	}

	if (dest > rpath + prefix_len + 1 && ISSLASH(dest[-1])) {
		--dest;
	}
	*dest = '\0';
	return rpath;

out:
	free(rpath);
	return NULL;
}

// FollowSymlinkInScope is a wrapper around evalSymlinksInScope that returns an
// absolute path. This function handles paths in a platform-agnostic manner.
char *follow_symlink_in_scope(const char *fullpath, const char *rootpath)
{
	char resfull[PATH_MAX] = {0}, *full = NULL;
	char resroot[PATH_MAX] = {0}, *root = NULL;

	full = cleanpath(fullpath, resfull, PATH_MAX);
	if (!full) {
		ERROR("Failed to get cleaned path");
		return NULL;
	}

	root = cleanpath(rootpath, resroot, PATH_MAX);
	if (!root) {
		ERROR("Failed to get cleaned path");
		return NULL;
	}

	return eval_symlinks_in_scope(full, root);
}

// GetResourcePath evaluates `path` in the scope of the container's rootpath, with proper path
// sanitisation. Symlinks are all scoped to the rootpath of the container, as
// though the container's rootpath was `/`.
//
// The BaseFS of a container is the host-facing path which is bind-mounted as
// `/` inside the container. This method is essentially used to access a
// particular path inside the container as though you were a process in that
// container.
int get_resource_path(const char *rootpath, const char *path,
                      char **scopepath)
{
	char resolved[PATH_MAX] = {0}, *cleanedpath = NULL;
	char *fullpath = NULL;
	size_t len;

	if (!rootpath || !path || !scopepath)
		return -1;

	*scopepath = NULL;

	cleanedpath = cleanpath(path, resolved, PATH_MAX);
	if (!cleanedpath) {
		ERROR("Failed to get cleaned path");
		return -1;
	}

	len = strlen(rootpath) + strlen(cleanedpath) + 1;
	fullpath = malloc(len);
	if (!fullpath) {
		ERROR("Out of memory");
		return -1;
	}
	snprintf(fullpath, len, "%s%s", rootpath, cleanedpath);

	*scopepath = follow_symlink_in_scope(fullpath, rootpath);

	free(fullpath);
	return 0;
}

// Rel returns a relative path that is lexically equivalent to targpath when
// joined to basepath with an intervening separator. That is,
// Join(basepath, Rel(basepath, targpath)) is equivalent to targpath itself.
// On success, the returned path will always be relative to basepath,
// even if basepath and targpath share no elements.
// An error is returned if targpath can't be made relative to basepath or if
// knowing the current working directory would be necessary to compute it.
// Rel calls Clean on the result.
char *path_relative(const char *basepath, const char *targpath)
{
	char resbase[PATH_MAX] = {0}, *base = NULL;
	char restarg[PATH_MAX] = {0}, *targ = NULL;
	size_t bl = 0, tl = 0, b0 = 0, bi = 0, t0 = 0, ti = 0;

	base = cleanpath(basepath, resbase, PATH_MAX);
	if (!base) {
		ERROR("Failed to get cleaned path");
		return NULL;
	}

	targ = cleanpath(targpath, restarg, PATH_MAX);
	if (!targ) {
		ERROR("Failed to get cleaned path");
		return NULL;
	}

	if (strcmp(base, targ) == 0)
		return safe_strdup(".");

	bl = strlen(base);
	tl = strlen(targ);
	while(true) {
		while(bi < bl && !ISSLASH(base[bi]))
			bi++;
		while(ti < tl && !ISSLASH(targ[ti]))
			ti++;
		//not the same string
		if (((bi - b0) != (ti - t0)) || strncmp(base + b0, targ + t0, bi - b0))
			break;
		if (bi < bl)
			bi++;
		if (ti < tl)
			ti++;
		b0 = bi;
		t0 = ti;
	}

	if (b0 != bl) {
		// Base elements left. Must go up before going down.
		int seps = 0, i;
		size_t ncopyed = 0, seps_size;
		char *buf = NULL;

		for (bi = b0; bi < bl; bi++) {
			if (ISSLASH(base[bi]))
				seps++;
		}
		//strlen(..) + strlen(/..) + '\0'
		seps_size = 2 + seps * 3 + 1;
		if (t0 != tl)
			seps_size += 1 + tl - t0;

		buf = calloc(seps_size, 1);
		if (!buf) {
			ERROR("Out of memory");
			return NULL;
		}
		buf[ncopyed++] = '.';
		buf[ncopyed++] = '.';
		for (i = 0; i < seps; i++) {
			buf[ncopyed++] = '/';
			buf[ncopyed++] = '.';
			buf[ncopyed++] = '.';
		}
		if (t0 != tl) {
			buf[ncopyed++] = '/';
			memcpy(buf + ncopyed, targ + t0, tl - t0 + 1);
		}
		return buf;
	}

	return safe_strdup(targ + t0);
}