/******************************************************************************
 * Copyright (c) Huawei Technologies Co., Ltd. 2019. All rights reserved.
 * Author: lifeng
 * Create: 2019-12-08
 * Description: provide container definition
 * lxc: linux Container library
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
  ******************************************************************************/

#ifndef __LXC_EXEC_COMMANDS_H
#define __LXC_EXEC_COMMANDS_H

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "lxccontainer.h"
#include "macro.h"
#include "state.h"
#include "terminal.h"

struct lxc_exec_command_handler {
	int maincmd_fd;
	struct lxc_terminal *terminal;
};

typedef enum {
	LXC_EXEC_CMD_SET_TERMINAL_WINCH,
	LXC_EXEC_CMD_MAX,
} lxc_exec_cmd_t;

struct lxc_exec_cmd_req {
	lxc_exec_cmd_t cmd;
	int datalen;
	const void *data;
};

struct lxc_exec_cmd_rsp {
	int ret; /* 0 on success, -errno on failure */
	int datalen;
	void *data;
};

struct lxc_exec_cmd_rr {
	struct lxc_exec_cmd_req req;
	struct lxc_exec_cmd_rsp rsp;
};

struct lxc_exec_cmd_set_terminal_winch_request {
	unsigned int height;
	unsigned int width;
};

struct lxc_epoll_descr;
struct lxc_handler;

extern int lxc_exec_cmd_init(const char *name, const char *lxcpath, const char *suffix);
extern int lxc_exec_cmd_mainloop_add(struct lxc_epoll_descr *descr, struct lxc_exec_command_handler *handler);
extern int lxc_exec_cmd_set_terminal_winch(const char *name, const char *lxcpath, const char *suffix, unsigned int height, unsigned int width);

#ifdef HAVE_ISULAD
extern int lxc_exec_unix_sock_delete(const char *name, const char *suffix);
#endif

#endif /* __exec_commands_h */
